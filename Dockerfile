FROM node:14.9.0
# Specify where our app will live in the container
WORKDIR /app

# Copy the React App to the container
COPY . .

# Prepare the container for building React
RUN npm install
# We want the production version
# RUN npm run build

EXPOSE 3000

CMD ["npm","run","start"]