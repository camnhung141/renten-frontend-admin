import React, { lazy } from 'react';
import { Redirect } from 'react-router-dom';
import DashboardLayout from '../layouts/DashboardLayout';
import AuthProtect from '../components/Auth/AuthProtect';

// ----------------------------------------------------------------------

const HomeRoutes = {
  path: '*',
  guard: AuthProtect,
  layout: DashboardLayout,
  routes: [
    {
      exact: true,
      path: '/',
      component: lazy(() => import('src/views/general/DashboardAppView'))
    },
    {
      component: () => <Redirect to="/404" />
    }
  ]
};

export default HomeRoutes;
