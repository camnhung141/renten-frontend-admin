import { PATH_APP } from './paths';
import React, { lazy } from 'react';
import { Redirect } from 'react-router-dom';
import AuthProtect from '../components/Auth/AuthProtect';
import DashboardLayout from '../layouts/DashboardLayout';

// ----------------------------------------------------------------------

const AppRoutes = {
  path: PATH_APP.root,
  guard: AuthProtect,
  layout: DashboardLayout,
  routes: [
    // MAIN DASHBOARD
    // ----------------------------------------------------------------------
    {
      exact: true,
      path: PATH_APP.main.dashboard,
      component: lazy(() => import('src/views/general/DashboardAppView'))
    },
    {
      exact: true,
      path: PATH_APP.main.ecommerce,
      component: lazy(() => import('src/views/general/DashboardEcommerceView'))
    },
    {
      exact: true,
      path: PATH_APP.main.analytics,
      component: lazy(() => import('src/views/general/DashboardAnalyticsView'))
    },
    {
      exact: true,
      path: PATH_APP.main.myAccount,
      component: lazy(() => import('src/views/general/AccountShopView'))
    },
    {
      exact: true,
      path: PATH_APP.root,
      component: () => <Redirect to={PATH_APP.main.root} />
    },

    // MANAGEMENT : E-COMMERCE
    // ----------------------------------------------------------------------
    {
      exact: true,
      path: PATH_APP.management.eCommerce.createProduct,
      component: lazy(() => import('../views/e-commerce/NewPostView'))
    },
    {
      exact: true,
      path: PATH_APP.management.eCommerce.products,
      component: lazy(() =>
        import('../views/shop/AccountShopView/ProductsShop')
      )
    },
    {
      exact: true,
      path: PATH_APP.management.eCommerce.product,
      component: lazy(() => import('../views/e-commerce/ProductDetailsView'))
    },
    {
      exact: true,
      path: PATH_APP.management.eCommerce.list,
      component: lazy(() => import('../views/e-commerce/ProductListView'))
    },
    {
      exact: true,
      path: PATH_APP.management.eCommerce.checkout,
      component: lazy(() => import('../views/e-commerce/CheckoutView'))
    },
    {
      exact: true,
      path: PATH_APP.management.eCommerce.invoice,
      component: lazy(() => import('../views/e-commerce/InvoiceView'))
    },
    {
      exact: true,
      path: PATH_APP.management.eCommerce.root,
      component: () => <Redirect to={PATH_APP.management.eCommerce.products} />
    },

    //MANAGEMENT : CATEGORY
    //  --------------------------------------------------------------------

    {
      exact: true,
      path: PATH_APP.management.category.list,
      component: lazy(() => import('../views/category'))
    },
    {
      exact: true,
      path: PATH_APP.management.category.labels,
      component: lazy(() => import('src/views/category'))
    },
    {
      exact: true,
      path: PATH_APP.management.category.root,
      component: () => <Redirect to={PATH_APP.management.category.all} />
    },
    //MANAGEMENT : USER
    //  --------------------------------------------------------------------

    {
      exact: true,
      path: PATH_APP.management.user.list,
      component: lazy(() => import('../views/user/UserListView'))
    },
    {
      exact: true,
      path: PATH_APP.management.user.account,
      component: lazy(() => import('../views/user/AccountView'))
    },
    {
      exact: true,
      path: PATH_APP.management.user.cards,
      component: lazy(() => import('../views/shop/ShopListView'))
    },
    {
      exact: true,
      path: PATH_APP.management.user.root,
      component: () => <Redirect to={PATH_APP.management.user.list} />
    },

    //MANAGEMENT : ORDER
    //  --------------------------------------------------------------------

    {
      exact: true,
      path: PATH_APP.management.order.list,
      component: lazy(() => import('../views/order/OrderListView'))
    },
    {
      exact: true,
      path: PATH_APP.management.order.detail,
      component: lazy(() => import('../views/order/InvoiceView'))
    },
    {
      exact: true,
      path: PATH_APP.management.order.root,
      component: () => <Redirect to={PATH_APP.management.order.list} />
    },
    // MANAGEMENT : USER
    // ----------------------------------------------------------------------
    {
      exact: true,
      path: PATH_APP.management.shop.list,
      component: lazy(() => import('../views/shop/ShopListView'))
    },
    {
      exact: true,
      path: PATH_APP.management.shop.account,
      component: lazy(() => import('../views/shop/AccountShopView'))
    },
    {
      exact: true,
      path: PATH_APP.management.shop.root,
      component: () => <Redirect to={PATH_APP.management.shop.list} />
    },
    {
      exact: true,
      path: PATH_APP.management.root,
      component: () => <Redirect to={PATH_APP.management.shop.list} />
    },

    // ----------------------------------------------------------------------
    {
      component: () => <Redirect to="/404" />
    }
  ]
};

export default AppRoutes;
