import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { PATH_PAGE } from '../../routes/paths';

// ----------------------------------------------------------------------

AuthProtect.propTypes = {
  children: PropTypes.node
};

function AuthProtect({ children }) {
  const auth = useSelector(state => state.auth.isLogin);

  // if (!isLoaded(auth)) {
  //   return <LoadingScreen />;
  // }

  if (!auth) {
    return <Redirect to={PATH_PAGE.auth.login} />;
  }

  return children;
}

export default AuthProtect;
