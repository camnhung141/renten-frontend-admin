import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import createAvatar from '../utils/createAvatar';
import { MAvatar } from '../@material-extend';

// ----------------------------------------------------------------------

MyAvatar.propTypes = {
  className: PropTypes.string
};

function MyAvatar({ className, ...other }) {
  const profile = useSelector(state => state.user.myProfile);

  return (
    <>
      <MAvatar
        src={profile?.avatar}
        alt={'myAvatar'}
        color={createAvatar('myAvatar').color}
        className={className}
        {...other}
      >
        {createAvatar('myAvatar').name}
      </MAvatar>
    </>
  );
}

export default MyAvatar;
