import clsx from 'clsx';
import React from 'react';
import Search from './Search';
import Account from './Account';
import DarkMode from './DarkMode';
import PropTypes from 'prop-types';
import { Icon } from '@iconify/react';
import Notifications from './Notifications';
import menu2Fill from '@iconify-icons/eva/menu-2-fill';
import { alpha, makeStyles } from '@material-ui/core/styles';
import {
  Box,
  AppBar,
  useMediaQuery,
  Toolbar,
  IconButton
} from '@material-ui/core';

// ----------------------------------------------------------------------

const DRAWER_WIDTH = 280;
const APPBAR_MOBILE = 64;
const APPBAR_DESKTOP = 92;

const useStyles = makeStyles(theme => ({
  root: {
    boxShadow: 'none',
    backdropFilter: 'blur(8px)',
    backgroundColor: alpha(theme.palette.background.default, 0.72),
    [theme.breakpoints.up('lg')]: {
      width: `calc(100% - ${DRAWER_WIDTH}px)`
    }
  },
  toolbar: {
    minHeight: APPBAR_MOBILE,
    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(0, 5)
    },
    [theme.breakpoints.up('lg')]: {
      minHeight: APPBAR_DESKTOP
    }
  },
  btnMenu: {
    marginRight: theme.spacing(1),
    color: theme.palette.text.primary
  }
}));

// ----------------------------------------------------------------------

TopBar.propTypes = {
  onOpenNav: PropTypes.func,
  className: PropTypes.string
};

function TopBar({ onOpenNav, className }) {
  const classes = useStyles();
  const hidden = useMediaQuery(theme => theme.breakpoints.up('xl'));

  return (
    <AppBar className={clsx(classes.root, className)}>
      <Toolbar className={classes.toolbar}>
        {!hidden && (
          <IconButton onClick={onOpenNav} className={classes.btnMenu}>
            <Icon icon={menu2Fill} />
          </IconButton>
        )}

        <Search />
        <Box sx={{ flexGrow: 1 }} />

        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
            '& > *:not(:first-of-type)': {
              ml: {
                xs: 0.5,
                sm: 2,
                lg: 3
              }
            }
          }}
        >
          {/* <Languages /> */}
          <Notifications />
          <DarkMode />
          <Account />
        </Box>
      </Toolbar>
    </AppBar>
  );
}

export default TopBar;
