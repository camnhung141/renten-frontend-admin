import React from 'react';
// import { MLabel, MIcon } from '../../../@material-extend';
import { PATH_APP, PATH_PAGE } from '../../../routes/paths';
import { MIcon } from '../../../@material-extend';
// import { PATH_APP } from '../../../routes/paths';

// ----------------------------------------------------------------------

const path = name => `/static/icons/navbar/${name}.svg`;

const ICONS = {
  authenticator: <MIcon src={path('ic_authenticator')} />,
  blog: <MIcon src={path('ic_blog')} />,
  calendar: <MIcon src={path('ic_calendar')} />,
  cart: <MIcon src={path('ic_cart')} />,
  charts: <MIcon src={path('ic_charts')} />,
  chat: <MIcon src={path('ic_chat')} />,
  components: <MIcon src={path('ic_components')} />,
  dashboard: <MIcon src={path('ic_dashboard')} />,
  editor: <MIcon src={path('ic_editor')} />,
  elements: <MIcon src={path('ic_elements')} />,
  error: <MIcon src={path('ic_error')} />,
  mail: <MIcon src={path('ic_mail')} />,
  map: <MIcon src={path('ic_map')} />,
  page: <MIcon src={path('ic_page')} />,
  user: <MIcon src={path('ic_user')} />,
  upload: <MIcon src={path('ic_upload')} />,
  copy: <MIcon src={path('ic_copy')} />,
  carousel: <MIcon src={path('ic_carousel')} />,
  language: <MIcon src={path('ic_language')} />
};

export default [
  // MAIN DASHBOARD
  // ----------------------------------------------------------------------
  {
    subheader: 'CHUNG',
    items: [
      {
        title: 'Bảng Điều Khiển',
        icon: ICONS.dashboard,
        href: PATH_APP.main.root
        // items: [
        //   {
        //     title: 'app',
        //     href: PATH_APP.main.root
        //   },
        //   {
        //     title: 'e-commerce',
        //     href: PATH_APP.main.ecommerce
        //   },
        //   {
        //     title: 'analytics',
        //     href: PATH_APP.main.analytics
        //   }
        // ]
      }
    ]
  },

  // MANAGEMENT
  // ----------------------------------------------------------------------
  {
    subheader: 'QUẢN LÝ',
    items: [
      {
        title: 'Danh mục',
        key: 'admin',
        icon: ICONS.blog,
        href: PATH_APP.management.category.list,
        items: [
          {
            title: 'Danh sách danh mục',
            href: PATH_APP.management.category.list
          }
        ]
      },
      {
        title: 'Người dùng',
        key: 'admin',
        icon: ICONS.user,
        href: PATH_APP.management.user.root,
        items: [
          {
            title: 'Danh sách người dùng',
            href: PATH_APP.management.user.list
          }
        ]
      },

      {
        title: 'Cửa hàng',
        icon: ICONS.carousel,
        key: 'admin',
        href: PATH_APP.management.user.root,
        items: [
          {
            title: 'Danh sách cửa hàng',
            href: PATH_APP.management.shop.list
          }
        ]
      },

      {
        title: 'Sản phẩm',
        icon: ICONS.cart,
        href: PATH_APP.management.user.root,
        items: [
          {
            title: 'Danh sách sản phẩm',
            href: PATH_APP.management.eCommerce.list
          },
          {
            title: 'Tạo mới/ Đăng sản phẩm',
            href: PATH_APP.management.eCommerce.createProduct
          }
        ]
      },
      // MANAGEMENT : E-COMMERCE
      // ----------------------------------------------------------------------
      {
        title: 'Đơn hàng',
        icon: ICONS.elements,
        href: PATH_APP.management.order.root,
        items: [
          {
            title: 'Danh sách đơn hàng',
            href: PATH_APP.management.order.list
          }
        ]
      }

      // MANAGEMENT : E-COMMERCE
      // ----------------------------------------------------------------------
      // {
      //   title: 'e-commerce',
      //   icon: ICONS.cart,
      //   href: PATH_APP.management.eCommerce.root,
      //   items: [
      //     {
      //       title: 'shop',
      //       href: PATH_APP.management.eCommerce.products
      //     },
      //     {
      //       title: 'product',
      //       href: PATH_APP.management.eCommerce.productById
      //     },
      //     {
      //       title: 'list',
      //       href: PATH_APP.management.eCommerce.list
      //     },
      //     {
      //       title: 'checkout',
      //       href: PATH_APP.management.eCommerce.checkout
      //     },
      //     {
      //       title: 'invoice',
      //       href: PATH_APP.management.eCommerce.invoice
      //     }
      //   ]
      // },

      // MANAGEMENT : BLOG
      // ----------------------------------------------------------------------
      // {
      //   title: 'blog',
      //   icon: ICONS.blog,
      //   href: PATH_APP.management.blog.root,
      //   items: [
      //     {
      //       title: 'posts',
      //       href: PATH_APP.management.blog.root
      //     },
      //     {
      //       title: 'post',
      //       href: PATH_APP.management.blog.postById
      //     },
      //     {
      //       title: 'new post',
      //       href: PATH_APP.management.blog.newPost
      //     }
      //   ]
      // }
    ]
  },

  // // APP
  // // ----------------------------------------------------------------------
  // {
  //   subheader: 'app',
  //   items: [
  //     {
  //       title: 'mail',
  //       href: PATH_APP.app.mail.root,
  //       icon: ICONS.mail
  //     },
  //     {
  //       title: 'chat',
  //       href: PATH_APP.app.chat.root,
  //       icon: ICONS.chat
  //     },
  //     {
  //       title: 'calendar',
  //       href: PATH_APP.app.calendar,
  //       icon: ICONS.calendar
  //     }
  //   ]
  // },

  // PAGES
  // ----------------------------------------------------------------------
  {
    subheader: 'Trang',
    items: [
      {
        title: 'Xác thực',
        href: PATH_PAGE.auth.loginUnprotected,
        icon: ICONS.authenticator,
        items: [
          {
            title: 'Đăng nhập',
            href: PATH_PAGE.auth.loginUnprotected
          },
          {
            title: 'Đăng ký',
            href: PATH_PAGE.auth.registerUnprotected
          }
        ]
      },
      {
        title: 'Trang lỗi hoặc trạng thái',
        href: '/404',
        icon: ICONS.error,
        items: [
          {
            title: 'Trang 404',
            href: '/404'
          },
          {
            title: 'Trang 500',
            href: '/500'
          },
          {
            title: 'Đang bảo trì',
            href: PATH_PAGE.maintenance
          },
          {
            title: 'Sớm xuất hiện',
            href: PATH_PAGE.comingSoon
          }
        ]
      }
      // {
      //   title: 'landing page',
      //   href: '/',
      //   icon: ICONS.page
      // },
      // {
      //   title: 'pricing',
      //   href: PATH_PAGE.pricing,
      //   icon: ICONS.page
      // },
      // {
      //   title: 'payment',
      //   href: PATH_PAGE.payment,
      //   icon: ICONS.page
      // }
    ]
  }
];
