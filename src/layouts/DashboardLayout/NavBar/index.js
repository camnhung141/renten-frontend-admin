import NavItem from './NavItem';
import MenuLinks from './config';
import PropTypes from 'prop-types';
import Logo from '../../../components/Logo';
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import MyAvatar from '../../../components/MyAvatar';
import Scrollbars from '../../../components/Scrollbars';
import { PATH_APP, PATH_DOCS } from '../../../routes/paths';
import {
  Link as RouterLink,
  useLocation,
  matchPath,
  useHistory
} from 'react-router-dom';
import { alpha, makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Link,
  List,
  Button,
  Drawer,
  useMediaQuery,
  Typography,
  ListSubheader
} from '@material-ui/core';
import authService from '../../../apis/auth.api';
// ----------------------------------------------------------------------

const DRAWER_WIDTH = 280;

const useStyles = makeStyles(theme => {
  const isLight = theme.palette.mode === 'light';

  return {
    drawer: {
      [theme.breakpoints.up('lg')]: {
        flexShrink: 0,
        width: DRAWER_WIDTH
      }
    },
    drawerPaper: {
      width: DRAWER_WIDTH,
      background: theme.palette.background.default
    },
    subHeader: {
      ...theme.typography.overline,
      marginTop: theme.spacing(3),
      marginBottom: theme.spacing(2),
      paddingLeft: theme.spacing(5),
      color: theme.palette.text.primary
    },
    account: {
      display: 'flex',
      alignItems: 'center',
      padding: theme.spacing(2, 2.5),
      margin: theme.spacing(1, 2.5, 5),
      borderRadius: theme.shape.borderRadiusSm,
      background: theme.palette.grey[isLight ? 200 : 800]
    },
    doc: {
      padding: theme.spacing(2.5),
      borderRadius: theme.shape.borderRadiusMd,
      backgroundColor: isLight
        ? alpha(theme.palette.primary.main, 0.08)
        : theme.palette.primary.lighter
    }
  };
});

// ----------------------------------------------------------------------

function reduceChild({ array, item, pathname, level }) {
  const key = item.href + level;
  if (item.items) {
    const match = matchPath(pathname, {
      path: item.href,
      exact: false
    });

    array = [
      ...array,
      <NavItem
        key={key}
        level={level}
        icon={item.icon}
        info={item.info}
        href={item.href}
        title={item.title}
        open={Boolean(match)}
      >
        {renderNavItems({
          pathname,
          level: level + 1,
          items: item.items
        })}
      </NavItem>
    ];
  } else {
    array = [
      ...array,
      <NavItem
        key={key}
        level={level}
        href={item.href}
        icon={item.icon}
        info={item.info}
        title={item.title}
      />
    ];
  }
  return array;
}

function renderNavItems({ items, pathname, level = 0 }) {
  const role = JSON.parse(localStorage.getItem('roleAccount'));

  return (
    <List disablePadding>
      {items.reduce((array, item) => {
        if ((role?.roleType !== 'agency' && item.key) || !item.key)
          return reduceChild({ array, item, pathname, level });
        return [];
      }, [])}
    </List>
  );
}

NavBar.propTypes = {
  onCloseNav: PropTypes.func,
  isOpenNav: PropTypes.bool
};

function NavBar({ isOpenNav, onCloseNav }) {
  const classes = useStyles();
  const { pathname } = useLocation();
  const profile = useSelector(state => state.auth.myProfile);
  const history = useHistory();

  const handleClickLogout = async () => {
    await authService.logout();
    history.push('/auth/login');
  };

  useEffect(() => {
    if (isOpenNav && onCloseNav) {
      onCloseNav();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pathname]);

  const renderContent = (
    <Scrollbars>
      <Box sx={{ px: 2.5, py: 3 }}>
        <RouterLink to="/">
          <Logo />
        </RouterLink>
      </Box>

      <Link
        underline="none"
        component={RouterLink}
        to={
          profile?.accountId?.role?.roleType === 'agency'
            ? PATH_APP.main.myAccount
            : PATH_APP.main.dashboard
        }
      >
        <div className={classes.account}>
          <MyAvatar />
          <Box sx={{ ml: 2 }}>
            <Typography variant="subtitle2" color="textPrimary">
              {profile?.agencyName || 'Admin'}
            </Typography>
            <Typography variant="body2" color="textSecondary">
              {profile?.accountId?.role?.roleType}
            </Typography>
          </Box>
        </div>
      </Link>

      {MenuLinks.map(list => (
        <List
          disablePadding
          key={list.subheader}
          subheader={
            <ListSubheader
              disableSticky
              disableGutters
              className={classes.subHeader}
            >
              {list.subheader}
            </ListSubheader>
          }
        >
          {renderNavItems({
            items: list.items,
            pathname: pathname
          })}
        </List>
      ))}

      <Box sx={{ px: 2.5, pb: 3, mt: 10 }}>
        <div className={classes.doc}>
          <Box
            component="img"
            alt="doc"
            src="/static/brand/ecommerce.png"
            sx={{ height: 56, mb: 2 }}
          />
          <Box
            component="h6"
            sx={{ mb: 1, typography: 'subtitle1', color: 'grey.800' }}
          >
            Xin chào, {profile?.agencyName}
          </Box>
          <Box sx={{ mb: 2, typography: 'body2', color: 'grey.600' }}>
            Cảm ơn bạn đã sử dụng Rent Mart
          </Box>

          <Button
            fullWidth
            to={PATH_DOCS.root}
            variant="contained"
            onClick={handleClickLogout}
          >
            Đăng xuất
          </Button>
        </div>
      </Box>
    </Scrollbars>
  );

  const hidden = useMediaQuery(theme => theme.breakpoints.up('xl'));
  return (
    <nav className={classes.drawer}>
      {!hidden && (
        <Drawer
          anchor="left"
          open={isOpenNav}
          variant="temporary"
          onClose={onCloseNav}
          classes={{ paper: classes.drawerPaper }}
        >
          {renderContent}
        </Drawer>
      )}

      {hidden && (
        <Drawer
          open
          anchor="left"
          variant="persistent"
          classes={{ paper: classes.drawerPaper }}
        >
          {renderContent}
        </Drawer>
      )}
    </nav>
  );
}

export default NavBar;
