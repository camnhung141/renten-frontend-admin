import axiosInstance from '../utils/axiosApiInstance';
import { apiUrl } from '../config';

const API_URL = apiUrl;
class AccountService {
  login(idata) {
    return axiosInstance
      .post(API_URL + 'auths/signin', idata)
      .then(response => {
        const data = response.data.payload;
        if (data) {
          localStorage.setItem('accessToken', JSON.stringify(data.accessToken));
          localStorage.setItem(
            'refreshToken',
            JSON.stringify(data.refreshToken)
          );
          localStorage.setItem(
            'roleAccount',
            JSON.stringify(data.account.role)
          );
        }
        return data;
      });
  }

  register(idata) {
    return axiosInstance
      .post(API_URL + 'auths/shops/signup', idata)
      .then(() => {
        return this.login({
          email: idata.email,
          password: idata.password
        });
      });
  }

  logout() {
    // return axiosApiInstance.post(API_URL + "logout").then((response) => {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('refreshToken');
    localStorage.removeItem('i18nextLng');
    localStorage.removeItem('roleAccount');
    // });
  }

  getMyProfile() {
    return axiosInstance.get(API_URL + 'agencies/info');
  }

  updateMyProfile(data) {
    return axiosInstance.put(API_URL + 'agencies/info', data);
  }

  verify(data) {
    return axiosInstance.put(API_URL + 'auths/verify', data);
  }

  async updatePassword(data) {
    return axiosInstance.put(API_URL + 'auths/update-password', data);
  }

  async reviewOrder(data) {
    return axiosInstance.post(API_URL + 'account-reviews/', data);
  }
}
export default new AccountService();
