import axiosInstance from '../utils/axiosApiInstance';
import { apiUrl } from '../config';

const API_URL = apiUrl + 'orders';
class OrderService {
  list() {
    return axiosInstance.get(API_URL).then(response => {
      const idata = response.data.payload;
      return idata;
    });
  }

  listOfAgency(agencyId) {
    const API_PRODUCTS_LIST = API_URL + '?agencyId=' + agencyId;
    return axiosInstance.get(API_PRODUCTS_LIST).then(response => {
      const idata = response.data.payload;
      return idata;
    });
  }

  listOfCustomer(customerId) {
    const API_PRODUCTS_LIST = API_URL + '?customerId=' + customerId;
    return axiosInstance.get(API_PRODUCTS_LIST).then(response => {
      const idata = response.data.payload;
      return idata;
    });
  }

  getDetailOrder(id) {
    return axiosInstance.get(API_URL + '/' + id).then(response => {
      return response.data.payload;
    });
  }

  updateOrder(data, id) {
    return axiosInstance.put(API_URL + '/' + id, data).then(response => {
      return response.data.payload;
    });
  }
}
export default new OrderService();
