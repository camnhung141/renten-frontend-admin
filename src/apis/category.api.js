import axiosInstance from '../utils/axiosApiInstance';
import { apiUrl } from '../config';

const API_URL = apiUrl + 'categories/';
class CategoryService {
  list() {
    return axiosInstance.get(API_URL).then(response => {
      const idata = response.data.payload;
      return idata;
    });
  }

  create(data) {
    return axiosInstance.post(API_URL, data).then(response => {
      const idata = response.data.payload;
      return idata;
    });
  }
  update(data, id) {
    return axiosInstance.put(API_URL + id, data).then(response => {
      const idata = response.data.payload;
      return idata;
    });
  }
  delete(id) {
    return axiosInstance.delete(API_URL + id);
  }
}
export default new CategoryService();
