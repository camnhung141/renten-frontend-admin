import axiosInstance from '../utils/axiosApiInstance';
import { apiUrl } from '../config';

const API_URL = apiUrl;

class CustomerService {
  updateStatus(data) {
    return axiosInstance.put(API_URL + 'customers/info', data);
  }

  getProfileUser(id) {
    return axiosInstance.get(API_URL + 'customers/' + id);
  }

  listDeliveryUser(accountId) {
    return axiosInstance.get(
      API_URL + `customers/${accountId}/delivery-addresses`
    );
  }

  list() {
    return axiosInstance.get(API_URL + 'customers/');
  }
}
export default new CustomerService();
