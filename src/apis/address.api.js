import axios from 'axios';
import { apiUrl } from '../config';

const API_URL = apiUrl + 'locations/';
class AddressService {
  listCity() {
    return axios.get(API_URL + 'cities');
  }

  listDistrict(cityCode) {
    return axios.get(API_URL + `cities/${cityCode}/districts`);
  }

  listWard(districtCode) {
    return axios.get(API_URL + `districts/${districtCode}/wards`);
  }
}
export default new AddressService();
