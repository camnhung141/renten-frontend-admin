import axiosInstance from '../utils/axiosApiInstance';
import { apiUrl } from '../config';

const API_URL = apiUrl + 'product-templates';

class ProductService {
  list(data) {
    let API_PRODUCTS_LIST;
    if (data) {
      API_PRODUCTS_LIST = API_URL + '?agencyId=' + data.agencyId;
    } else {
      API_PRODUCTS_LIST = API_URL;
    }
    return axiosInstance.get(API_PRODUCTS_LIST).then(response => {
      const idata = response.data.payload;
      return idata;
    });
  }

  listOfAgency(data) {
    const API_PRODUCTS_LIST = API_URL + '?agencyId=' + data.agencyId;
    return axiosInstance.get(API_PRODUCTS_LIST).then(response => {
      const idata = response.data.payload;
      return idata;
    });
  }

  getDetailProduct(id) {
    return axiosInstance.get(API_URL + '/' + id).then(async response => {
      return response.data.payload;
    });
  }

  productsCategoryList(categoriesId, keyword) {
    return axiosInstance
      .get(API_URL, {
        params: {
          categoriesId: categoriesId,
          keyword: keyword
        }
      })
      .then(response => {
        return response.data.payload;
      })
      .catch(() => {
        return null;
      });
  }
  getReviewsProduct(id) {
    return axiosInstance
      .get(API_URL + '/' + id + '/reviews')
      .then(async response => response.data.payload);
  }

  createProduct(data, productTemplateId) {
    for (let i = 0; i < data.length; i++) {
      data[i].template = productTemplateId;
    }
    return axiosInstance
      .post(API_URL + `/${productTemplateId}/products`, data)
      .then(response => response.data.payload);
  }

  async createProductTemplate(data) {
    const options = data.options;
    delete data.options;
    return axiosInstance.post(API_URL, data).then(async response => {
      const iOptions = await this.createProduct(
        options,
        response.data.payload._id
      );
      return {
        ...response.data.payload,
        options: iOptions
      };
    });
  }

  updateProductTemplate(data, id) {
    return axiosInstance.put(API_URL + '/' + id, data).then(async response => {
      return {
        ...response.data.payload
      };
    });
  }

  updateProduct(data, productTemplateId) {
    const input = [];
    for (let i = 0; i < data.length; i++) {
      const temp = {
        appearance: data[i].appearance,
        images: data[i].images,
        originalPrice: data[i].originalPrice,
        productCode: data[i].productCode,
        rentPrice: data[i].rentPrice,
        template: productTemplateId,
        thumbnail: data[i].thumbnail ? data[i].thumbnail : data[i].images[0]
      };
      if (data[i]._id) {
        temp._id = data[i]._id;
      }
      input.push(temp);
    }

    return axiosInstance
      .put(API_URL + `/${productTemplateId}/products`, input)
      .then(response => response.data.payload);
  }

  deleteProduct(id) {
    return axiosInstance.delete(API_URL + id);
  }

  updateHistoryProduct(data, productTemplateId, productId) {
    return axiosInstance.put(
      `${API_URL}/${productTemplateId}/products/${productId}`,
      data
    );
  }

  getHistoryProduct(productId) {
    return axiosInstance.get(`${apiUrl}products/${productId}/appearances`);
  }
}
export default new ProductService();
