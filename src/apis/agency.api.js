import axiosInstance from '../utils/axiosApiInstance';

import { apiUrl } from '../config';

const API_URL = apiUrl + 'agencies/';
class CustomerService {
  updateStatus(data) {
    return axiosInstance.put(API_URL + 'info', data);
  }

  getProfileAgency(id) {
    return axiosInstance.get(API_URL + id);
  }

  list() {
    return axiosInstance.get(API_URL);
  }
}
export default new CustomerService();
