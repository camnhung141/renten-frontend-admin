import axiosInstance from '../utils/axiosApiInstance';
import { apiUrl } from '../config';

const API_URL = apiUrl + 'images/';
class ImagesService {
  upload(images, path) {
    const formData = new FormData();
    images.forEach(file => {
      formData.append('images', file);
    });

    formData.append('path', path);

    return axiosInstance.post(API_URL, formData).then(response => {
      const idata = response.data.payload;

      return idata;
    });
  }
}
export default new ImagesService();
