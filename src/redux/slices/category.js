import { createSlice } from '@reduxjs/toolkit';
import categoryService from '../../apis/category.api';
// ----------------------------------------------------------------------

const initialState = {
  isLoading: false,
  error: false,
  categories: [],
  subCategories: []
};

const slice = createSlice({
  name: 'category',
  initialState,
  reducers: {
    // START LOADING
    startLoading(state) {
      state.isLoading = true;
    },

    // HAS ERROR
    hasError(state, action) {
      state.isLoading = false;
      state.error = action.payload;
    },

    // GET CATEGORIES
    getCategoriesSuccess(state, action) {
      state.isLoading = false;
      state.categories = action.payload;
    },

    getSubCategoriesSuccess(state, action) {
      state.isLoading = false;
      state.subCategories = action.payload;
    }
  }
});

// Reducer
export default slice.reducer;

// Actions
export const { getCategoriesSuccess } = slice.actions;

// ----------------------------------------------------------------------

export function getCategories() {
  return async dispatch => {
    dispatch(slice.actions.startLoading());
    try {
      const response = await categoryService.list();
      dispatch(slice.actions.getCategoriesSuccess(response));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    }
  };
}

export function getSubCategories(data) {
  return async dispatch => {
    try {
      dispatch(slice.actions.getSubCategoriesSuccess(data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    }
  };
}
