import { createSlice } from '@reduxjs/toolkit';
import addressService from '../../apis/address.api';
// ----------------------------------------------------------------------

const initialState = {
  isLoading: false,
  error: false,
  districts: [],
  cities: [],
  wards: [],
  idistricts: [],
  icities: [],
  iwards: []
};

const slice = createSlice({
  name: 'address',
  initialState,
  reducers: {
    // START LOADING
    startLoading(state) {
      state.isLoading = true;
    },

    // HAS ERROR
    hasError(state, action) {
      state.isLoading = false;
      state.error = action.payload;
    },

    // GET CITY
    getCitiesSuccess(state, action) {
      state.isLoading = false;
      state.cities = action.payload;
    },

    // GET DISTRICTS
    getDistrictsSuccess(state, action) {
      state.isLoading = false;
      state.districts = action.payload;
    },

    // GET WARDS
    getWardsSuccess(state, action) {
      state.isLoading = false;
      state.wards = action.payload;
    },
    // GET CITY
    getICitiesSuccess(state, action) {
      state.isLoading = false;
      state.icities = action.payload;
    },

    // GET DISTRICTS
    getIDistrictsSuccess(state, action) {
      state.isLoading = false;
      state.idistricts = action.payload;
    },

    // GET WARDS
    getIWardsSuccess(state, action) {
      state.isLoading = false;
      state.iwards = action.payload;
    }
  }
});

// Reducer
export default slice.reducer;

// Actions
export const {
  onGotoStep,
  onBackStep,
  onNextStep,
  deleteCart,
  createBilling,
  applyShipping,
  applyDiscount,
  increaseQuantity,
  decreaseQuantity,
  sortByOrders,
  filterOrders
} = slice.actions;

// ----------------------------------------------------------------------

//Get cities
export function getCities() {
  return async dispatch => {
    dispatch(slice.actions.startLoading());
    try {
      const response = await addressService.listCity();
      dispatch(slice.actions.getCitiesSuccess(response.data.payload));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    }
  };
}

// Get districs
export function getDistricts(cityCode) {
  return async dispatch => {
    dispatch(slice.actions.startLoading());
    try {
      const response = await addressService.listDistrict(cityCode);
      dispatch(slice.actions.getDistrictsSuccess(response.data.payload));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    }
  };
}
// ----------------------------------------------------------------------

// get wards
export function getWards(districtCode) {
  return async dispatch => {
    dispatch(slice.actions.startLoading());
    try {
      const response = await addressService.listWard(districtCode);
      dispatch(slice.actions.getWardsSuccess(response.data.payload));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    }
  };
}

//Get cities
export function getICities() {
  return async dispatch => {
    dispatch(slice.actions.startLoading());
    try {
      const response = await addressService.listCity();
      dispatch(slice.actions.getICitiesSuccess(response.data.payload));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    }
  };
}

// Get districs
export function getIDistricts(cityCode) {
  return async dispatch => {
    dispatch(slice.actions.startLoading());
    try {
      const response = await addressService.listDistrict(cityCode);
      dispatch(slice.actions.getIDistrictsSuccess(response.data.payload));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    }
  };
}
// ----------------------------------------------------------------------

// get wards
export function getIWards(districtCode) {
  return async dispatch => {
    dispatch(slice.actions.startLoading());
    try {
      const response = await addressService.listWard(districtCode);
      dispatch(slice.actions.getIWardsSuccess(response.data.payload));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    }
  };
}
