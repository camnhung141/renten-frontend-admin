import { map } from 'lodash';
import { createSlice } from '@reduxjs/toolkit';
import orderService from '../../apis/order.api';
// ----------------------------------------------------------------------

const initialState = {
  isLoading: false,
  error: false,
  aOrders: [],
  cOrders: [],
  myOrders: [],
  orderDetail: null,
  sortBy: null,
  filters: {
    gender: [],
    category: 'All',
    colors: [],
    priceRange: '',
    rating: ''
  },
  checkout: {
    activeStep: 0,
    cart: [],
    subtotal: null,
    total: null,
    discount: 0,
    shipping: 0,
    billing: null
  }
};

const slice = createSlice({
  name: 'order',
  initialState,
  reducers: {
    // START LOADING
    startLoading(state) {
      state.isLoading = true;
    },

    // HAS ERROR
    hasError(state, action) {
      state.isLoading = false;
      state.error = action.payload;
    },

    // GET ALL ORDERS
    getOrdersSuccess(state, action) {
      state.isLoading = false;
      state.myOrders = action.payload;
    },

    // GET ORDERS AGENCY
    getOrdersAgencySuccess(state, action) {
      state.isLoading = false;
      state.aOrders = action.payload;
    },

    // GET ORDERS CUSTOMER
    getOrdersCustomerSuccess(state, action) {
      state.isLoading = false;
      state.cOrders = action.payload;
    },

    // GET DETAIL PRODUCT
    getOrderSuccess(state, action) {
      state.isLoading = false;
      state.orderDetail = action.payload;
    },

    //  SORT & FILTER ORDERS
    sortByOrders(state, action) {
      state.sortBy = action.payload;
    },

    filterOrders(state, action) {
      state.filters.gender = action.payload.gender;
      state.filters.category = action.payload.category;
      state.filters.colors = action.payload.colors;
      state.filters.priceRange = action.payload.priceRange;
      state.filters.rating = action.payload.rating;
    },

    onBackStep(state) {
      state.checkout.activeStep -= 1;
    },

    onNextStep(state) {
      state.checkout.activeStep += 1;
    },

    onGotoStep(state, action) {
      const goToStep = action.payload;
      state.checkout.activeStep = goToStep;
    },

    increaseQuantity(state, action) {
      const productId = action.payload;
      const updateCart = map(state.checkout.cart, product => {
        if (product.id === productId) {
          return {
            ...product,
            quantity: product.quantity + 1
          };
        }
        return product;
      });

      state.checkout.cart = updateCart;
    },

    decreaseQuantity(state, action) {
      const productId = action.payload;
      const updateCart = map(state.checkout.cart, product => {
        if (product.id === productId) {
          return {
            ...product,
            quantity: product.quantity - 1
          };
        }
        return product;
      });

      state.checkout.cart = updateCart;
    },

    createBilling(state, action) {
      state.checkout.billing = action.payload;
    },

    applyDiscount(state, action) {
      const discount = action.payload;
      state.checkout.discount = discount;
      state.checkout.total = state.checkout.subtotal - discount;
    },

    applyShipping(state, action) {
      const shipping = action.payload;
      state.checkout.shipping = shipping;
      state.checkout.total =
        state.checkout.subtotal - state.checkout.discount + shipping;
    }
  }
});

// Reducer
export default slice.reducer;

// Actions
export const {
  onGotoStep,
  onBackStep,
  onNextStep,
  deleteCart,
  createBilling,
  applyShipping,
  applyDiscount,
  increaseQuantity,
  decreaseQuantity,
  sortByOrders,
  filterOrders
} = slice.actions;

// ----------------------------------------------------------------------

// get all order
export function getAllOrders() {
  return async dispatch => {
    dispatch(slice.actions.startLoading());
    try {
      const response = await orderService.list();
      dispatch(slice.actions.getOrdersSuccess(response));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    }
  };
}

//Get orders agency
export function getOrdersOfAgency(agencyId) {
  return async dispatch => {
    dispatch(slice.actions.startLoading());
    try {
      const response = await orderService.listOfAgency(agencyId);
      dispatch(slice.actions.getOrdersAgencySuccess(response));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    }
  };
}

// Get order customer
export function getOrdersOfCustomer(customerId) {
  return async dispatch => {
    dispatch(slice.actions.startLoading());
    try {
      const response = await orderService.listOfCustomer(customerId);
      dispatch(slice.actions.getOrdersCustomerSuccess(response));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    }
  };
}
// ----------------------------------------------------------------------

export function getDetailOrder(id) {
  return async dispatch => {
    dispatch(slice.actions.startLoading());
    try {
      const response = await orderService.getDetailOrder(id);
      dispatch(slice.actions.getOrderSuccess(response));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    }
  };
}
