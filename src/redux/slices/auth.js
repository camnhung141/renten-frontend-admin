import { createSlice } from '@reduxjs/toolkit';
import accountService from '../../apis/auth.api';
// ----------------------------------------------------------------------

const initialState = {
  isLoading: false,
  error: false,
  isUpdate: false,
  isLogin: localStorage.getItem('accessToken') ? true : false,
  myProfile: null,
  updatePassword: false
};

const slice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    // START LOADING
    startLoading(state) {
      state.isLoading = true;
    },

    // HAS ERROR
    hasError(state, action) {
      state.isLoading = false;
      state.error = action.payload;
    },

    // GET Login
    loginSuccess(state, action) {
      state.isLoading = false;
      state.isLogin = true;
    },

    // GET PROFILE
    getMyProfileSuccess(state, action) {
      state.isLoading = false;
      state.myProfile = action.payload;
    },

    // GET PROFILE
    updateMyProfileSuccess(state, action) {
      state.isLoading = false;
      state.isUpdate = action.payload;
    },

    // UPDATE PASSWORD
    updatePassword(state, action) {
      state.isLoading = false;
      state.updatePassword = true;
    }
  }
});

// Reducer
export default slice.reducer;

// Actions
export const { onToggleFollow } = slice.actions;

// ----------------------------------------------------------------------

export function login() {
  return async dispatch => {
    dispatch(slice.actions.startLoading());
    try {
      dispatch(slice.actions.loginSuccess());
    } catch (error) {
      dispatch(slice.actions.hasError(true));
    }
  };
}

export function getMyProfile() {
  return async dispatch => {
    dispatch(slice.actions.startLoading());
    try {
      const response = await accountService.getMyProfile();
      dispatch(slice.actions.getMyProfileSuccess(response.data.payload));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    }
  };
}

export function updateMyProfile(data) {
  return async dispatch => {
    dispatch(slice.actions.startLoading());
    try {
      const response = true;
      dispatch(slice.actions.updateMyProfileSuccess(response));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    }
  };
}

export function updateMyPassword(data) {
  return async dispatch => {
    dispatch(slice.actions.startLoading());
    try {
      const response = await accountService.updatePassword(data);
      dispatch(slice.actions.updatePassword(response.data.payload));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    }
  };
}
