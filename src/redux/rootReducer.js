import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { firestoreReducer } from 'redux-firestore';
import darkModeReducer from './slices/dark-mode';
import productReducer from './slices/product';
import userReducer from './slices/user';
import agencyReducer from './slices/agency';
import authReducer from './slices/auth';
import orderReducer from './slices/order';
import categoryReducer from './slices/category';
import notificationsReducer from './slices/notifications';
import addressReducer from './slices/address';

// ----------------------------------------------------------------------

const rootPersistConfig = {
  key: 'root',
  storage: storage,
  keyPrefix: 'redux-',
  version: 1,
  whitelist: ['theme']
};

const productPersistConfig = {
  key: 'product',
  storage: storage,
  keyPrefix: 'redux-',
  blacklist: ['isLoading', 'error', 'products', 'product', 'filters']
};

const rootReducer = combineReducers({
  firestore: firestoreReducer,
  theme: darkModeReducer,
  product: persistReducer(productPersistConfig, productReducer),
  user: userReducer,
  order: orderReducer,
  agency: agencyReducer,
  auth: authReducer,
  category: categoryReducer,
  address: addressReducer,
  notifications: notificationsReducer
});

export { rootPersistConfig, rootReducer };
