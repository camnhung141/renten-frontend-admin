import clsx from 'clsx';
import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import {
  ListItem,
  Typography,
  ListItemText,
  ListItemIcon
} from '@material-ui/core';
// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {
    height: 48,
    ...theme.typography.body2,
    textTransform: 'capitalize',
    padding: theme.spacing(0, 3),
    position: 'relative',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
      alignItems: 'center'
    },
    color: theme.palette.text.secondary,
    '&:hover': {
      zIndex: 999,
      position: 'relative',
      boxShadow: theme.shadows[25].z24,
      '& $actions': { opacity: 1 }
    }
  },
  listItemIcon: { color: 'inherit' },
  isActive: {
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightMedium,
    backgroundColor: theme.palette.action.selected
  }
}));

// ----------------------------------------------------------------------

CategoryItem.propTypes = {
  category: PropTypes.object.isRequired,
  className: PropTypes.string
};

function CategoryItem({ category, className, ...other }) {
  const classes = useStyles();

  return (
    <ListItem
      button
      className={clsx(classes.root, className)}
      activeClassName={classes.isActive}
      {...other}
    >
      <ListItemIcon className={classes.listItemIcon}>
        <img alt={category._id} src={category.icon} width={24} height={24} />
      </ListItemIcon>

      <ListItemText disableTypography primary={category.name} />

      <Typography variant="caption">
        {category.subCategories?.length}
      </Typography>
    </ListItem>
  );
}

export default CategoryItem;
