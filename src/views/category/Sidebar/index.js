import clsx from 'clsx';
import CategoryItem from './CategoryItem';
import React, { useEffect, useState } from 'react';
import Scrollbars from '../../../components/Scrollbars';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  List,
  Drawer,
  Button,
  Divider,
  LinearProgress
} from '@material-ui/core';
import SubCategoryList from '../SubCategories';
import { getSubCategories } from '../../../redux/slices/category';
import plusFill from '@iconify-icons/eva/plus-fill';
import { Icon } from '@iconify/react';
import AddDialog from '../Dialog/AddNew';
// ----------------------------------------------------------------------

const useStyles = makeStyles(() => ({
  root: {},
  drawerPaper: {
    width: 350
  },
  drawerPaperDesktop: {
    position: 'relative'
  }
}));

function Sidebar() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { categories, isLoading } = useSelector(state => state.category);
  const [subCate, setSubCate] = useState([]);
  const [open, setOpen] = React.useState(false);
  const [category, setCategory] = useState();

  const color = 'white';

  useEffect(() => {
    setSubCate(categories && categories[0] ? categories[0].subCategories : []);
  }, [categories]);

  useEffect(() => {
    dispatch(getSubCategories(subCate));
  }, [categories, subCate, dispatch]);

  const handleClick = (data, category) => {
    setSubCate(data);
    setCategory(category);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  if (isLoading) {
    return <LinearProgress />;
  }

  return (
    <>
      <Drawer
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper, classes.drawerPaperDesktop)
        }}
      >
        <Scrollbars>
          <Box sx={{ p: 3 }}>
            <Button
              fullWidth
              variant="contained"
              startIcon={<Icon icon={plusFill} />}
              color="primary"
              onClick={handleClickOpen}
            >
              Thêm danh mục
            </Button>
            <AddDialog
              open={open}
              setOpen={setOpen}
              handleClose={handleClose}
              parentId={null}
            />
          </Box>
          <Divider />
          <List disablePadding>
            {categories?.map(category => (
              <CategoryItem
                style={{ backgroundColor: `${color}` }}
                key={category._id}
                category={category}
                onClick={() => handleClick(category.subCategories, category)}
              />
            ))}
          </List>
        </Scrollbars>
      </Drawer>
      <SubCategoryList
        subCate={subCate}
        category={
          category ? category : categories && categories[0] ? categories[0] : []
        }
      />
    </>
  );
}

export default Sidebar;
