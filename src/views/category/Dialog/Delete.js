import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles } from '@material-ui/core/styles';
import categoryService from '../../../apis/category.api';
const useStyles = makeStyles(theme => ({
  root: {
    color: 'gray',
    borderColor: 'gray'
  },
  margin: {
    marginBottom: theme.spacing(3)
  }
}));
export default function DeleteDialog({ open, handleClose, id }) {
  const classes = useStyles();
  const handleDelete = async () => {
    await categoryService.delete(id);
    window.location.reload();
  };
  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">XÁC NHẬN XÓA ?</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Nếu bạn nhấn vào nút xóa, mọi dữ liệu sẽ hoàn toàn biến mất và không
            thể khôi phục lại
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} className={classes.root}>
            HỦY BỎ
          </Button>
          <Button onClick={handleDelete} autoFocus>
            XÓA
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
