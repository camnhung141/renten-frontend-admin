import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles } from '@material-ui/core/styles';
import { UploadMultiFile } from '../../../components/Upload';
import imageService from '../../../apis/images.api';
import categoryService from '../../../apis/category.api';

const useStyles = makeStyles(theme => ({
  root: {
    color: 'gray',
    borderColor: 'gray'
  },
  margin: {
    marginBottom: theme.spacing(3)
  }
}));

export default function Edit({ open, handleClose, id, oldName, oldIcon }) {
  const classes = useStyles();
  const [image, setImage] = React.useState([]);
  const [name, setName] = React.useState('');
  const [error, setError] = React.useState('');

  React.useEffect(() => {
    setName(oldName);
  }, [oldName]);
  const handleEdit = async () => {
    if (!name || name === '' || name === oldName) {
      setError('* Không được bỏ trống hay trùng tên cũ');
      return;
    }
    let data = {
      name
    };
    if (image.length > 0) {
      const iconCate = await imageService.upload(image, 1);
      data.icon = iconCate[0];
    } else {
      data.icon = oldIcon;
    }

    await categoryService.update(data, id);
    window.location.reload();
  };

  return (
    <div>
      <Dialog
        className={classes.root}
        open={open}
        onClose={handleClose}
        maxWidth="lg"
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Chỉnh sửa danh mục</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Điền đầy đủ các thông tin bên dưới
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            value={name}
            onChange={e => setName(e.target.value)}
            label="Tên danh mục"
            type="text"
            fullWidth
            className={classes.margin}
          />
          <UploadMultiFile
            value={image}
            onChange={setImage}
            // value={values.cover}
            // onChange={val => setFieldValue('cover', val)}
            caption="Chọn biểu tượng cho danh mục"
          />
        </DialogContent>
        <p className={classes.error}>{error}</p>

        <DialogActions>
          <Button onClick={handleClose} className={classes.root}>
            ĐÓNG
          </Button>
          <Button onClick={handleEdit} autoFocus>
            CẬP NHẬT
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
