import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles } from '@material-ui/core/styles';
import { UploadMultiFile } from '../../../components/Upload';
import categoryService from '../../../apis/category.api';
import imageService from '../../../apis/images.api';
const useStyles = makeStyles(theme => ({
  root: {
    color: 'gray',
    borderColor: 'gray'
  },
  error: {
    color: 'red',
    fontSize: '1rem'
  },
  margin: {
    marginBottom: theme.spacing(3)
  }
}));

export default function AddNew({ open, setOpen, handleClose, parentId }) {
  const classes = useStyles();
  const [image, setImage] = React.useState([]);
  const [name, setName] = React.useState();
  const [error, setError] = React.useState('');

  const handleSubmit = async () => {
    if (!name || name === '') {
      setError('* Không được bỏ trống');
      return;
    }
    if (image && image.length > 0) {
      const iconCate = await imageService.upload(image, 1);
      let data = {
        icon: iconCate[0],
        name
      };
      if (parentId) {
        data.parentId = parentId;
      }
      await categoryService.create(data);
      window.location.reload();
      return;
    }
    setError('* Vui lòng đăng tải một biểu tượng');
  };

  return (
    <div>
      <Dialog
        className={classes.root}
        open={open}
        onClose={handleClose}
        maxWidth="lg"
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Thêm mới danh mục</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Điền đầy đủ các thông tin bên dưới
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            value={name}
            onChange={e => setName(e.target.value)}
            label="Tên danh mục"
            type="text"
            fullWidth
            className={classes.margin}
          />
          <UploadMultiFile
            value={image}
            onChange={setImage}
            // value={values.cover}
            // onChange={val => setFieldValue('cover', val)}
            caption="Chọn biểu tượng cho danh mục"
          />
          <p className={classes.error}>{error}</p>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} className={classes.root}>
            ĐÓNG
          </Button>
          <Button onClick={handleSubmit} autoFocus>
            TẠO MỚI
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
