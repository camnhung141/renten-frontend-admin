import clsx from 'clsx';
import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from '@iconify/react';
import editFill from '@iconify-icons/eva/edit-2-fill';
import { makeStyles } from '@material-ui/core/styles';
import { Tooltip, IconButton } from '@material-ui/core';
import DeleteDialog from '../Dialog/Delete';
import EditDialog from '../Dialog/Edit';
// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {
    height: 40,
    zIndex: 99,
    opacity: 0,
    margin: 'auto',
    display: 'flex',
    position: 'absolute',
    alignItems: 'center',
    top: theme.spacing(1),
    right: theme.spacing(1),
    bottom: theme.spacing(1),
    justifyContent: 'center',
    padding: theme.spacing(0, 0.75),
    boxShadow: theme.shadows[25].z12,
    borderRadius: theme.shape.borderRadius,
    backgroundColor: theme.palette.background.paper,
    transition: theme.transitions.create('opacity')
  },
  button: {
    margin: theme.spacing(0, 0.75),
    '&:hover': { color: theme.palette.text.primary }
  }
}));

// ----------------------------------------------------------------------

Action.propTypes = {
  handleArchive: PropTypes.func,
  handleDelete: PropTypes.func,
  handleMarkRead: PropTypes.func,
  handleHidden: PropTypes.func,
  className: PropTypes.string
};

function Action({ subCategory, className }) {
  const [openDelete, setOpenDelete] = React.useState(false);
  const [openEdit, setOpenEdit] = React.useState(false);

  const handleCloseDelete = () => {
    setOpenDelete(false);
  };

  const handleClickOpenEdit = () => {
    setOpenEdit(true);
  };
  const handleCloseEdit = () => {
    setOpenEdit(false);
  };

  const classes = useStyles();

  const CATEGORY_ACTIONS = [
    {
      name: 'Chỉnh sửa',
      icon: editFill,
      action: handleClickOpenEdit
    }
    // {
    //   name: 'Xóa',
    //   icon: trash2Fill,
    //   action: handleClickOpenDelete
    // }
  ];

  return (
    <div className={clsx(classes.root, className)}>
      {CATEGORY_ACTIONS.map(action => (
        <Tooltip key={action.name} title={action.name}>
          <IconButton
            size="small"
            onClick={action.action}
            className={classes.button}
          >
            <Icon icon={action.icon} width={24} height={24} />
          </IconButton>
        </Tooltip>
      ))}
      <DeleteDialog
        open={openDelete}
        setOpen={setOpenDelete}
        handleClose={handleCloseDelete}
        id={subCategory._id}
      />
      <EditDialog
        open={openEdit}
        setOpen={setOpenEdit}
        handleClose={handleCloseEdit}
        id={subCategory._id}
        oldName={subCategory.name}
        oldIcon={subCategory.icon}
      />
    </div>
  );
}

export default Action;
