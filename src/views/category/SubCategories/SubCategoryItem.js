import clsx from 'clsx';
import React from 'react';
import PropTypes from 'prop-types';
import Action from './Action';
import createAvatar from '../../../utils/createAvatar';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Link, Typography } from '@material-ui/core';
import { MAvatar } from '../../../@material-extend';

// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {
    position: 'relative',
    padding: theme.spacing(0, 2),
    color: theme.palette.text.secondary,
    backgroundColor: theme.palette.background.neutral,
    borderBottom: `1px solid ${theme.palette.divider}`,
    [theme.breakpoints.up('md')]: {
      display: 'flex',
      alignItems: 'center'
    },
    '&:hover': {
      zIndex: 999,
      position: 'relative',
      boxShadow: theme.shadows[25].z24,
      '& $actions': { opacity: 1 }
    }
  },
  wrap: {
    minWidth: 0,
    display: 'flex',
    padding: theme.spacing(2, 0),
    transition: theme.transitions.create('padding')
  },
  name: {
    minWidth: 200,
    paddingRight: theme.spacing(2)
  },
  date: {
    flexShrink: 0,
    minWidth: 120,
    marginLeft: 'auto',
    textAlign: 'right'
  },
  unread: {
    color: theme.palette.text.primary,
    backgroundColor: theme.palette.background.paper,
    '& $name': { fontWeight: theme.typography.fontWeightBold },
    '& $date': { fontWeight: theme.typography.fontWeightBold },
    '& $subject': { fontWeight: theme.typography.fontWeightBold },
    '& $message': { color: theme.palette.text.secondary }
  },
  isDense: { padding: theme.spacing(1, 0) },
  subject: {},
  message: {},
  actions: {}
}));

// ----------------------------------------------------------------------

SubCategoryItem.propTypes = {
  subCategory: PropTypes.object
};

function SubCategoryItem({ subCategory, className, ...other }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.root, className)} {...other}>
      <Box
        sx={{
          ml: 2,
          minWidth: '0',
          alignItems: 'center',
          display: { md: 'flex' }
        }}
      ></Box>
      <Link color="inherit" underline="none" className={clsx(classes.wrap)}>
        <MAvatar
          alt={subCategory._id}
          src={subCategory.icon}
          color={createAvatar(subCategory.name).color}
          sx={{ width: 32, height: 32 }}
        >
          {createAvatar(subCategory.name).name}
        </MAvatar>

        <Box
          sx={{
            ml: 2,
            minWidth: 0,
            alignItems: 'center',
            display: { md: 'flex' }
          }}
        >
          <Box
            noWrap
            component={Typography}
            sx={{ typography: 'body2', pr: 2 }}
          >
            <Typography variant="body2" className={classes.name} noWrap>
              {subCategory.name}
            </Typography>
          </Box>
        </Box>
      </Link>
      <Action className={classes.actions} subCategory={subCategory} />
    </div>
  );
}

export default SubCategoryItem;
