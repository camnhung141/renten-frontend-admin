import clsx from 'clsx';
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Icon } from '@iconify/react';
import { useDispatch } from 'react-redux';
import searchFill from '@iconify-icons/eva/search-fill';
import { makeStyles } from '@material-ui/core/styles';
import editFill from '@iconify-icons/eva/edit-2-fill';
import plusFill from '@iconify-icons/eva/plus-fill';
import {
  Box,
  useMediaQuery,
  Tooltip,
  IconButton,
  Typography,
  FormControl,
  OutlinedInput,
  InputAdornment
} from '@material-ui/core';
import { getSubCategories } from '../../../redux/slices/category';
import EditDialog from '../Dialog/Edit';
import AddDialog from '../Dialog/AddNew';
// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {
    height: 84,
    flexShrink: 0,
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 2)
  },
  icon: {
    width: 20,
    height: 20
  },
  name: {
    fontSize: '1rem'
  },
  search: {
    transition: theme.transitions.create(['box-shadow', 'width'], {
      easing: theme.transitions.easing.easeInOut,
      duration: theme.transitions.duration.shorter
    }),
    '&.Mui-focused': { boxShadow: theme.shadows[25].z8 },
    '& fieldset': {
      borderWidth: `1px !important`,
      borderColor: `${theme.palette.grey[500_32]} !important`
    },
    [theme.breakpoints.up('md')]: {
      width: 240,
      flexDirection: 'row',
      '&.Mui-focused': { width: 280 }
    }
  }
}));

// ----------------------------------------------------------------------

Toolbar.propTypes = {
  list: PropTypes.array.isRequired
};

function Toolbar({ list, subCate, category, className, ...other }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [keyword, setKeyword] = useState('');
  const [openEdit, setOpenEdit] = React.useState(false);
  const [openAdd, setOpenAdd] = React.useState(false);

  const handleClickOpenEdit = () => {
    setOpenEdit(true);
  };

  const handleCloseAdd = () => {
    setOpenAdd(false);
  };

  const handleClickOpenAdd = () => {
    setOpenAdd(true);
  };

  const handleCloseEdit = () => {
    setOpenEdit(false);
  };

  const CATEGORY_ACTIONS = [
    {
      name: 'Thêm',
      icon: plusFill,
      action: handleClickOpenAdd
    },
    {
      name: 'Sửa',
      icon: editFill,
      action: handleClickOpenEdit
    }
  ];
  const hidden = useMediaQuery(theme => theme.breakpoints.up('xl'));
  useEffect(() => {
    if (keyword !== '' && keyword) {
      const listSearch = subCate?.filter(e => e.name === keyword);
      dispatch(getSubCategories(listSearch));
    }
  }, [keyword, dispatch, subCate]);
  return (
    <div className={clsx(classes.root, className)} {...other}>
      <Box
        sx={{
          ml: 2,
          minWidth: 0
        }}
      >
        <Box noWrap component={Typography} sx={{ typography: 'body2', pr: 2 }}>
          <Typography variant="body2" className={classes.name} noWrap>
            Danh mục con -{' '}
            <span style={{ color: 'green' }}>{` ${category?.name}`}</span>
            <span>
              {CATEGORY_ACTIONS.map(action => (
                <Tooltip
                  key={action.name}
                  title={action.name}
                  style={{ marginLeft: '0.5rem' }}
                >
                  <IconButton
                    size="small"
                    onClick={action.action}
                    className={classes.button}
                  >
                    <Icon icon={action.icon} width={24} height={24} />
                  </IconButton>
                </Tooltip>
              ))}
              <EditDialog
                open={openEdit}
                setOpen={setOpenEdit}
                handleClose={handleCloseEdit}
                id={category._id}
                oldName={category.name}
                oldIcon={category.icon}
              />
              <AddDialog
                open={openAdd}
                setOpen={setOpenAdd}
                handleClose={handleCloseAdd}
                parentId={category._id}
              />
            </span>
          </Typography>
        </Box>
      </Box>
      <Box sx={{ flexGrow: 1 }} />

      <FormControl size="small">
        <OutlinedInput
          placeholder="Tìm kiếm"
          value={keyword}
          onChange={e => setKeyword(e.target.value)}
          startAdornment={
            <InputAdornment position="start">
              <Box
                component={Icon}
                icon={searchFill}
                sx={{ color: 'text.disabled' }}
              />
            </InputAdornment>
          }
          className={classes.search}
        />
      </FormControl>

      {hidden && (
        <Box sx={{ display: 'flex', alignItems: 'center', flexShrink: 0 }}>
          <Box sx={{ mx: 2, typography: 'body2', color: 'text.secondary' }}>
            1 - {list?.length} of {list?.length}
          </Box>
        </Box>
      )}
    </div>
  );
}

export default Toolbar;
