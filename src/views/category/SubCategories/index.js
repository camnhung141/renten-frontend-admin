import Toolbar from './Toolbar';
import SubCategoryItem from './SubCategoryItem';
import Scrollbars from '../../../components/Scrollbars';
import React from 'react';
import EmptyContent from '../../../components/EmptyContent';
import { useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { Divider, Box, LinearProgress } from '@material-ui/core';

// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    display: 'flex',
    overflow: 'hidden',
    flexDirection: 'column'
  }
}));

function SubCategoryList({ subCate, category }) {
  const classes = useStyles();
  const { subCategories, isLoading } = useSelector(state => state.category);
  const isEmpty = subCategories?.length < 1;

  if (isLoading) {
    return <LinearProgress />;
  }

  return (
    <div className={classes.root}>
      <Toolbar list={subCategories} subCate={subCate} category={category} />
      <Divider />

      {!isEmpty ? (
        <Scrollbars>
          <Box sx={{ minWidth: { md: 800 } }}>
            {subCategories?.map(subCategory => (
              <SubCategoryItem
                key={subCategory._id}
                subCategory={subCategory}
              />
            ))}
          </Box>
        </Scrollbars>
      ) : (
        <EmptyContent
          title="Danh sách rỗng"
          img="/static/illustrations/illustration_empty_mail.svg"
          sx={{ flexGrow: 1, height: 'auto' }}
        />
      )}
    </div>
  );
}

export default SubCategoryList;
