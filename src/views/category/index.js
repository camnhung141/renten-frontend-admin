import Sidebar from './Sidebar';
import Page from '../../components/Page';
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { PATH_APP } from '../../routes/paths';
import { getCategories } from '../../redux/slices/category';
import HeaderDashboard from '../../components/HeaderDashboard';
import { makeStyles } from '@material-ui/core/styles';
import { Container, Card } from '@material-ui/core';
// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {},
  card: {
    [theme.breakpoints.up('md')]: {
      height: '72vh',
      display: 'flex'
    }
  }
}));

// ----------------------------------------------------------------------

function CategoryView() {
  const classes = useStyles();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCategories());
  }, [dispatch]);

  return (
    <Page className={classes.root} title="Danh mục">
      <Container maxWidth="xl">
        <HeaderDashboard
          heading="Danh mục"
          links={[
            {
              name: 'Bảng điều khiển',
              href: PATH_APP.management.category.list
            },
            { name: 'Danh mục' }
          ]}
        />

        <Card className={classes.card}>
          <Sidebar />
        </Card>
      </Container>
    </Page>
  );
}

export default CategoryView;
