import React, { useEffect, useState } from 'react';
import Welcome from './Welcome';
import Page from '../../../components/Page';
import { useSelector, useDispatch } from 'react-redux';
import FeaturedApp from './FeaturedApp';
import TotalIncome from './TotalIncome';
import TotalInstalled from './TotalInstalled';
import StatusOrder from './StatusOrder';
import TotalOrders from './TotalOrders';
import { makeStyles } from '@material-ui/core/styles';
import { Container, Grid } from '@material-ui/core';
import { getAllOrders } from '../../../redux/slices/order';

// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {}
}));

function DashboardAppView() {
  const classes = useStyles();
  const profile = useSelector(state => state.auth.myProfile);
  const [displayName, setDisplayName] = useState('');
  const orders = useSelector(state => state.order.myOrders);
  const dispatch = useDispatch();

  const totalRevenue = () => {
    let total = 0;
    orders.forEach(element => {
      total += element.totalPrice;
    });
    return total;
  };

  const totalIncome = () => {
    let total = 0;
    orders.forEach(element => {
      if (element.payment.paid && element.status !== 'cancel') {
        total += element.totalPrice;
      }
    });
    return total;
  };

  const roundChartData = () => {
    const data = [0, 0, 0, 0];
    orders.forEach(element => {
      switch (element.status) {
        case 'cancel': {
          data[1] = data[1] + 1;
          break;
        }
        case 'notReturned': {
          data[3] = data[3] + 1;
          break;
        }
        case 'lated': {
          data[2] = data[2] + 1;
          break;
        }
        default: {
          data[0] = data[0] + 1;
        }
      }
    });
    return data;
  };

  useEffect(() => {
    setDisplayName(profile?.agencyName);
  }, [profile]);

  useEffect(() => {
    dispatch(getAllOrders());
  }, [dispatch]);

  return (
    <Page title="Dashboard" className={classes.root}>
      <Container maxWidth="xl">
        <Grid container spacing={3}>
          {/*********************/}
          <Grid item xs={12} md={8}>
            <Welcome displayName={displayName} />
          </Grid>

          {/*********************/}
          <Grid item xs={12} md={4}>
            <FeaturedApp />
          </Grid>

          {/*********************/}
          <Grid item xs={12} md={4}>
            <TotalOrders total={orders?.length} />
          </Grid>

          <Grid item xs={12} md={4}>
            <TotalInstalled total={totalRevenue()} />
          </Grid>

          <Grid item xs={12} md={4}>
            <TotalIncome total={totalIncome()} />
          </Grid>

          {/*********************/}
          <Grid item xs={12} md={6} lg={4}>
            <StatusOrder data={roundChartData()} />
          </Grid>

          {/*********************/}
          <Grid item xs={12} md={6} lg={8}>
            {/* <AreaInstalled /> */}
          </Grid>
          {/*********************/}
        </Grid>
      </Container>
    </Page>
  );
}

export default DashboardAppView;
