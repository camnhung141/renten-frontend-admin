import clsx from 'clsx';
import React from 'react';
import * as Yup from 'yup';
import PropTypes from 'prop-types';
import { useSnackbar } from 'notistack';
import fakeRequest from '../../../utils/fakeRequest';
import { useFormik, Form, FormikProvider } from 'formik';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Card, TextField } from '@material-ui/core';
import { LoadingButton } from '@material-ui/lab';
import { useDispatch } from 'react-redux';
import { updateMyPassword } from '../../../redux/slices/auth';
// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  margin: {
    marginBottom: theme.spacing(3)
  }
}));

// ----------------------------------------------------------------------

ChangePassword.propTypes = {
  className: PropTypes.string
};

function ChangePassword({ className }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();

  const passwordRegex = RegExp(
    /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])/
  );

  const ChangePassWordSchema = Yup.object().shape({
    oldPassword: Yup.string().required('Mật khẩu cũ là bắt buộc'),
    newPassword: Yup.string()
      .required('Mật khẩu là bắt buộc')
      .matches(
        passwordRegex,
        'Mật khẩu phải có ít nhất 1 kí tự đặc biệt, một số và một chữ viết hoa, một chữ thường'
      )
      .min(8, 'Mật khẩu ít nhất 8 kí tự'),
    confirmNewPassword: Yup.string().oneOf(
      [Yup.ref('newPassword'), null],
      'Mật khẩu không khớp'
    )
  });

  const formik = useFormik({
    initialValues: {
      oldPassword: '',
      newPassword: '',
      confirmNewPassword: ''
    },
    validationSchema: ChangePassWordSchema,
    onSubmit: async (values, { setSubmitting }) => {
      const data = {
        oldPassword: values.oldPassword,
        newPassword: values.newPassword,
        confirmPassword: values.confirmNewPassword
      };
      try {
        dispatch(updateMyPassword(data));
        await fakeRequest(500);
        setSubmitting(false);
        enqueueSnackbar('Lưu thành công', { variant: 'success' });
        window.location.reload();
      } catch (err) {
        await fakeRequest(500);
        setSubmitting(false);
        enqueueSnackbar('Không thể cập nhật', { variant: 'eror' });
      }
    }
  });

  const { errors, touched, isSubmitting, handleSubmit, getFieldProps } = formik;

  return (
    <Card className={clsx(classes.root, className)}>
      <FormikProvider value={formik}>
        <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
          <TextField
            {...getFieldProps('oldPassword')}
            fullWidth
            type="password"
            label="Mật khẩu cũ"
            error={Boolean(touched.oldPassword && errors.oldPassword)}
            helperText={touched.oldPassword && errors.oldPassword}
            className={classes.margin}
          />

          <TextField
            {...getFieldProps('newPassword')}
            fullWidth
            type="password"
            label="Mật khẩu mới"
            error={Boolean(touched.newPassword && errors.newPassword)}
            helperText={
              (touched.newPassword && errors.newPassword) ||
              'Mật khẩu phải ít nhất 6 kí tự'
            }
            className={classes.margin}
          />

          <TextField
            {...getFieldProps('confirmNewPassword')}
            fullWidth
            type="password"
            label="Nhập lại mật khẩu mới"
            error={Boolean(
              touched.confirmNewPassword && errors.confirmNewPassword
            )}
            helperText={touched.confirmNewPassword && errors.confirmNewPassword}
            className={classes.margin}
          />

          <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
            <LoadingButton
              type="submit"
              variant="contained"
              pending={isSubmitting}
            >
              Lưu thay đổi
            </LoadingButton>
          </Box>
        </Form>
      </FormikProvider>
    </Card>
  );
}

export default ChangePassword;
