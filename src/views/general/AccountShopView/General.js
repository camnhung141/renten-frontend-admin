import clsx from 'clsx';
import React, { useEffect, useState } from 'react';
import * as Yup from 'yup';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Form, FormikProvider, useFormik } from 'formik';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Grid,
  TextField,
  MenuItem,
  LinearProgress,
  FormControl,
  Button,
  Typography,
  CircularProgress,
  Select
} from '@material-ui/core';
import { useSnackbar } from 'notistack';
import useIsMountedRef from '../../../hooks/useIsMountedRef';
import { useDispatch, useSelector } from 'react-redux';
import {
  getCities,
  getDistricts,
  getWards,
  getIDistricts,
  getIWards
} from '../../../redux/slices/address';
import { updateMyProfile } from '../../../redux/slices/auth';
import accountService from '../../../apis/auth.api';

// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {}
}));

// ----------------------------------------------------------------------

General.propTypes = {
  className: PropTypes.string
};

function General({ className, profile, isLoading }) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const isMountedRef = useIsMountedRef();
  const [icity, setICity] = useState();
  const [idistrict, setIDistrict] = useState();
  const [iward, setIWard] = useState();
  const [load, setLoad] = useState(false);
  const [gender, setGender] = useState('');

  const [city, setCity] = useState();
  const [district, setDistrict] = useState();
  const [ward, setWard] = useState();
  const cities = useSelector(state => state.address.cities);
  const districts = useSelector(state => state.address.districts);
  const wards = useSelector(state => state.address.wards);
  const idistricts = useSelector(state => state.address.idistricts);
  const iwards = useSelector(state => state.address.iwards);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getDistricts(city?.cityCode));
  }, [dispatch, city]);

  useEffect(() => {
    dispatch(getWards(district?.districtCode));
  }, [dispatch, district]);

  useEffect(() => {
    dispatch(getCities());
  }, [dispatch]);

  useEffect(() => {
    dispatch(getIDistricts(icity?.cityCode));
  }, [dispatch, icity]);

  useEffect(() => {
    dispatch(getIWards(idistrict?.districtCode));
  }, [dispatch, idistrict]);

  useEffect(() => {
    if (!profile) {
      setLoad(true);
    } else {
      setLoad(false);
    }
    setCity({
      cityName: profile?.address?.cityName,
      cityCode: profile?.address?.cityCode,
      cityPrefix: profile?.address?.cityPrefix
    });
    setDistrict({
      districtName: profile?.address?.districtName,
      districtCode: profile?.address?.districtCode,
      districtPrefix: profile?.address?.districtPrefix
    });
    setWard({
      wardName: profile?.address?.wardName,
      wardCode: profile?.address?.wardCode,
      wardPrefix: profile?.address?.wardPrefix
    });
    setICity({
      cityName: profile?.ownerProfile?.address?.cityName,
      cityCode: profile?.ownerProfile?.address?.cityCode,
      cityPrefix: profile?.ownerProfile?.address?.cityPrefix
    });
    setIDistrict({
      districtName: profile?.ownerProfile?.address?.districtName,
      districtCode: profile?.ownerProfile?.address?.districtCode,
      districtPrefix: profile?.ownerProfile?.address?.districtPrefix
    });
    setIWard({
      wardName: profile?.ownerProfile?.address?.wardName,
      wardCode: profile?.ownerProfile?.address?.wardCode,
      wardPrefix: profile?.ownerProfile?.address?.wardPrefix
    });
    setGender(profile?.ownerProfile?.gender);
  }, [profile]);

  const UpdateUserSchema = Yup.object().shape({
    fullName: Yup.string()
      .min(2, 'Quá ngắn!')
      .required('Họ và tên bắt buộc.'),
    phone: Yup.string().required('Số điện thoại là bắt buộc'),
    city: Yup.string(),
    district: Yup.string(),
    ward: Yup.string(),
    street: Yup.string(),
    agencyName: Yup.string().required('Shop name required'),
    gender: Yup.string()
  });

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      fullName: profile?.ownerProfile?.name || '',
      email: profile?.email || '',
      phone: profile?.ownerProfile?.phoneNumber || '',
      city: profile?.ownerProfile?.address?.city || '',
      district: profile?.ownerProfile?.address?.district || '',
      ward: profile?.ownerProfile?.address?.ward || '',
      gender: profile?.ownerProfile?.gender || '',
      street: profile?.ownerProfile?.address?.streetAndNumber || '',
      dateOfBirth:
        moment(profile?.ownerProfile?.dateOfBirthday).format('DD/MM/YYYY') ||
        '',
      agencyName: profile?.agencyName || '',
      shopCity: profile?.address?.city || '',
      shopPhone: profile?.phoneNumber || '',
      shopDistrict: profile?.address?.district || '',
      shopWard: profile?.address?.ward || '',
      fanPage: profile?.facebook || '',
      shopStreet: profile?.address?.streetAndNumber || '',
      about: profile?.about || '',
      avatar:
        profile?.avatar ||
        'https://image.freepik.com/free-vector/shop-icon-vector_7315-22.jpg'
    },
    validationSchema: UpdateUserSchema,
    onSubmit: async (values, { setErrors, setSubmitting }) => {
      try {
        const data = {
          ownerProfile: {
            name: values.fullName,
            address: {
              streetAndNumber: values.street,
              wardName: iward.wardName,
              wardCode: iward.wardCode,
              wardPrefix: iward.wardPrefix,
              districtName: idistrict.districtName,
              districtCode: idistrict.districtCode,
              districtPrefix: idistrict.districtPrefix,
              cityName: icity.cityName,
              cityCode: icity.cityCode,
              cityPrefix: icity.cityPrefix
            },
            gender: gender,
            phoneNumber: values.phone,
            dateOfBirthday: values.dateOfBirth
          },
          address: {
            streetAndNumber: values.shopStreet,
            wardName: ward.wardName,
            wardCode: ward.wardCode,
            wardPrefix: ward.wardPrefix,
            districtName: district.districtName,
            districtCode: district.districtCode,
            districtPrefix: district.districtPrefix,
            cityName: city.cityName,
            cityCode: city.cityCode,
            cityPrefix: city.cityPrefix
          },
          agencyName: values.agencyName,
          phoneNumber: values.shopPhone,
          facebook: values.fanPage,
          avatar: values.avatar,
          about: values.about
        };
        // dispatch(updateMyProfile(data));
        await accountService.updateMyProfile(data);
        dispatch(updateMyProfile(data));
        enqueueSnackbar('Cập nhật thành công', { variant: 'success' });
        if (isMountedRef.current) {
          setSubmitting(false);
        }
        window.location.reload();
      } catch (err) {
        enqueueSnackbar('Không thể cập nhật', { variant: 'error' });
        if (isMountedRef.current) {
          setErrors({ afterSubmit: err.code });
          setSubmitting(false);
        }
      }
    }
  });

  if (load) {
    return <LinearProgress />;
  }
  const { errors, touched, isSubmitting, handleSubmit, getFieldProps } = formik;

  return (
    <div className={clsx(classes.root, className)}>
      <FormikProvider value={formik}>
        <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
          <p style={{ fontSize: '1.5rem', color: 'green' }}>
            1. Thông tin người đại diện
          </p>
          <Box sx={{ mb: 3 }} />
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <Typography
                variant="subtitle2"
                color="textSecondary"
                gutterBottom
                className={classes.margin}
              >
                Họ và tên người đại diện
              </Typography>
              <TextField
                fullWidth
                required
                {...getFieldProps('fullName')}
                error={Boolean(touched.fullName && errors.fullName)}
                helperText={touched.fullName && errors.fullName}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography
                variant="subtitle2"
                color="textSecondary"
                gutterBottom
                className={classes.margin}
              >
                Số điện thoại của chủ của hàng
              </Typography>
              <TextField
                fullWidth
                name="phone"
                label=""
                {...getFieldProps('phone')}
                error={Boolean(touched.phone && errors.phone)}
                helperText={touched.phone && errors.phone}
              />
            </Grid>
          </Grid>
          <Box sx={{ mb: 3 }} />
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <Typography
                variant="subtitle2"
                color="textSecondary"
                gutterBottom
                className={classes.margin}
              >
                Giới tính
              </Typography>
              <FormControl fullWidth variant="outlined" required>
                <Select
                  displayEmpty
                  value={gender}
                  onChange={e => setGender(e.target.value)}
                >
                  <MenuItem value={gender}>
                    {gender === 'Nữ' ? 'Nữ' : 'Nam'}
                  </MenuItem>
                  <MenuItem value={gender === 'Nữ' ? 'Nam' : 'Nữ'}>
                    {gender === 'Nữ' ? 'Nam' : 'Nữ'}
                  </MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography
                variant="subtitle2"
                color="textSecondary"
                gutterBottom
                className={classes.margin}
              >
                Ngày sinh
              </Typography>
              <TextField
                fullWidth
                {...getFieldProps('dateOfBirth')}
                error={Boolean(touched.dateOfBirth && errors.dateOfBirth)}
                helperText={touched.dateOfBirth && errors.dateOfBirth}
              />
            </Grid>
          </Grid>

          <Box sx={{ mb: 3 }} />
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <Typography
                variant="subtitle2"
                color="textSecondary"
                gutterBottom
              >
                Tỉnh/Thành phố
              </Typography>
              <FormControl fullWidth variant="outlined" required>
                <Select
                  displayEmpty
                  value={icity}
                  onChange={e => setICity(e.target.value)}
                >
                  <em>
                    <MenuItem value={icity}>{icity?.cityName}</MenuItem>
                  </em>
                  <MenuItem value={icity}>{icity?.cityName}</MenuItem>
                  {cities.map(element => (
                    <MenuItem value={element}>{element.cityName}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography
                variant="subtitle2"
                color="textSecondary"
                gutterBottom
              >
                Quận/Huyện
              </Typography>
              <FormControl
                fullWidth
                className={classes.element}
                variant="outlined"
              >
                <FormControl
                  fullWidth
                  className={classes.element}
                  variant="outlined"
                >
                  <Select
                    displayEmpty
                    value={idistrict}
                    onChange={e => setIDistrict(e.target.value)}
                  >
                    <em>
                      <MenuItem value={idistrict}>
                        {idistrict?.districtName}
                      </MenuItem>
                    </em>
                    {idistricts.map(element => (
                      <MenuItem value={element}>
                        {element.districtName}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </FormControl>
            </Grid>
          </Grid>

          <Box sx={{ mb: 3 }} />

          <Grid container spacing={2}>
            <Grid item xs={12} sm={12}>
              <Typography
                variant="subtitle2"
                color="textSecondary"
                gutterBottom
              >
                Phường/Xã
              </Typography>
              <FormControl
                fullWidth
                className={classes.element}
                variant="outlined"
              >
                <Select
                  displayEmpty
                  value={iward}
                  onChange={e => setIWard(e.target.value)}
                >
                  <em>
                    <MenuItem value={iward}>{iward?.wardName}</MenuItem>
                  </em>
                  {iwards.map(element => (
                    <MenuItem value={element}>{element.wardName}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={12}>
              <Typography
                variant="subtitle2"
                color="textSecondary"
                gutterBottom
              >
                Địa chỉ
              </Typography>
              <TextField
                fullWidth
                {...getFieldProps('street')}
                error={Boolean(touched.street && errors.street)}
                helperText={touched.street && errors.street}
              />
            </Grid>
          </Grid>
          <Box sx={{ mt: 3 }}>
            <p style={{ fontSize: '1.5rem', color: 'green' }}>
              2. Thông tin của cửa hàng
            </p>
            <Box sx={{ mb: 3 }} />
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <Typography
                  variant="subtitle2"
                  color="textSecondary"
                  gutterBottom
                >
                  Tên cửa hàng
                </Typography>
                <TextField
                  fullWidth
                  {...getFieldProps('agencyName')}
                  error={Boolean(touched.agencyName && errors.agencyName)}
                  helperText={touched.agencyName && errors.agencyName}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <Typography
                  variant="subtitle2"
                  color="textSecondary"
                  gutterBottom
                >
                  Hotline/Số điện thoại liên hệ
                </Typography>
                <TextField
                  fullWidth
                  {...getFieldProps('shopPhone')}
                  error={Boolean(touched.shopPhone && errors.shopPhone)}
                  helperText={touched.shopPhone && errors.shopPhone}
                />
              </Grid>
            </Grid>
            <Box sx={{ mb: 3 }} />
            <Typography variant="subtitle2" color="textSecondary" gutterBottom>
              Link FanPage/facebook
            </Typography>
            <TextField
              fullWidth
              {...getFieldProps('fanPage')}
              error={Boolean(touched.fanPage && errors.fanPage)}
              helperText={touched.fanPage && errors.fanPage}
            />
            <Box sx={{ mb: 3 }} />
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <Typography
                  variant="subtitle2"
                  color="textSecondary"
                  gutterBottom
                >
                  Tỉnh/Thành phố
                </Typography>
                <FormControl
                  fullWidth
                  className={classes.element}
                  variant="outlined"
                >
                  <Select
                    displayEmpty
                    value={city}
                    onChange={e => setCity(e.target.value)}
                  >
                    <em>
                      <MenuItem value={city}>{city?.cityName}</MenuItem>
                    </em>
                    <MenuItem value={city}>{city?.cityName}</MenuItem>
                    {cities.map(element => (
                      <MenuItem value={element}>{element.cityName}</MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={12} sm={6}>
                <Typography
                  variant="subtitle2"
                  color="textSecondary"
                  gutterBottom
                >
                  Quận/Huyện
                </Typography>
                <FormControl
                  fullWidth
                  className={classes.element}
                  variant="outlined"
                >
                  <Select
                    displayEmpty
                    value={district}
                    onChange={e => setDistrict(e.target.value)}
                  >
                    <em>
                      <MenuItem value={district}>
                        {district?.districtName}
                      </MenuItem>
                    </em>
                    {districts.map(element => (
                      <MenuItem value={element}>
                        {element.districtName}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
            </Grid>

            <Box sx={{ mb: 3 }} />

            <Grid container spacing={2}>
              <Grid item xs={12} sm={12}>
                <Typography
                  variant="subtitle2"
                  color="textSecondary"
                  gutterBottom
                >
                  Phường/Xã
                </Typography>
                <FormControl
                  fullWidth
                  className={classes.element}
                  variant="outlined"
                >
                  <Select
                    displayEmpty
                    value={ward}
                    onChange={e => setWard(e.target.value)}
                  >
                    <em>
                      <MenuItem value={ward}>{ward?.wardName}</MenuItem>
                    </em>
                    {wards.map(element => (
                      <MenuItem value={element}>{element.wardName}</MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={12} sm={12}>
                <Typography
                  variant="subtitle2"
                  color="textSecondary"
                  gutterBottom
                >
                  Địa chỉ
                </Typography>
                <TextField
                  fullWidth
                  {...getFieldProps('shopStreet')}
                  error={Boolean(touched.shopStreet && errors.shopStreet)}
                  helperText={touched.shopStreet && errors.shopStreet}
                />
              </Grid>
            </Grid>
            <Box sx={{ mb: 3 }} />
            <Typography variant="subtitle2" color="textSecondary" gutterBottom>
              Mô tả ngắn về cửa hàng
            </Typography>
            <TextField
              fullWidth
              {...getFieldProps('about')}
              error={Boolean(touched.about && errors.about)}
              helperText={touched.about && errors.about}
            />
            <Box sx={{ mb: 3 }} />
            <Button type="submit" variant="contained" pending={isSubmitting}>
              {isLoading && (
                <CircularProgress size={24} className={classes.loading} />
              )}
              {!isLoading && 'Lưu thay đổi'}
            </Button>
          </Box>
        </Form>
      </FormikProvider>
    </div>
  );
}

export default General;
