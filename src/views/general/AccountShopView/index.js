import General from './General';
import { Icon } from '@iconify/react';
import Page from '../../../components/Page';
import SocialLinks from './SocialLinks';
import { PATH_APP } from '../../../routes/paths';
import ChangePassword from './ChangePassword';
import React, { useState, useEffect } from 'react';
import shareFill from '@iconify-icons/eva/share-fill';
import { useDispatch, useSelector } from 'react-redux';
import roundVpnKey from '@iconify-icons/ic/round-vpn-key';
import HeaderDashboard from '../../../components/HeaderDashboard';
import roundAccountBox from '@iconify-icons/ic/round-account-box';
import { getMyProfile } from '../../../redux/slices/auth';
import { makeStyles } from '@material-ui/core/styles';
import { Container, Tab, Box, Tabs, LinearProgress } from '@material-ui/core';

// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {},
  tabBar: {
    marginBottom: theme.spacing(5)
  },
  profile: {
    backgroundColor: 'white',
    width: '100%',
    border: '1px solid #ddd',
    boxSizing: ' border-box',
    margin: '2rem auto',
    boxShadow: ' 0px 0px 16px -1px rgba(0, 0, 0, 0.55)',
    borderRadius: '5px'
  },
  profileHeaderImageContainer: {
    position: 'relative',
    paddingBottom: '40%',
    overflow: 'hidden',
    marginBottom: '-50px'
  },
  profileHeaderImage: {
    borderTopLeftRadius: '5px',
    borderTopRightRadius: '5px',
    position: 'absolute',
    top: '0',
    left: '0',
    width: '100%'
  },

  // @supports (object-fit: cover) {
  //   .profile__header-image {
  //     object-fit: cover;
  //     height: 100%;
  //   }
  // },
  profileUser: {
    display: 'flex',
    alignItems: 'center',
    margin: '0 40px'
  },
  profileAvatar: {
    borderRadius: '50%',
    border: '10px solid white',
    display: 'inline-block',
    width: '200px',
    position: 'relative',
    top: '-10px'
  },
  profileDetails: {
    display: 'inline-block',
    position: 'relative',
    marginLeft: '30px'
  },
  profileName: {
    marginBottom: '0'
  },
  profileSubtitle: {
    display: 'inline-block',
    color: '#aaa'
  },
  profileActions: {
    flexGrow: '1',
    textAlign: 'right'
  },
  button: {
    display: 'inline-block',
    backgroundColor: '#697663',
    padding: '12px 25px',
    color: 'white',
    textDecoration: 'none',
    border: '2px solid #697663',
    borderRadius: '25px',
    transition: 'all 0.2s ease-out',
    '&hover': {
      background: 'none',
      color: '#697663'
    }
  }
}));

// ----------------------------------------------------------------------

function AccountView() {
  const classes = useStyles();
  const [currentTab, setCurrentTab] = useState('general');
  const dispatch = useDispatch();
  const { myProfile, isLoading, isUpdate } = useSelector(state => state.auth);

  useEffect(() => {
    dispatch(getMyProfile());
  }, [dispatch, isUpdate]);

  // if (!myProfile?.cards) {
  //   return null;
  // }

  // if (!myProfile?.notifications) {
  //   return null;
  // }

  const ACCOUNT_TABS = [
    {
      key: 'general',
      value: 'Chung',
      icon: <Icon icon={roundAccountBox} width={20} height={20} />,
      component: <General profile={myProfile} isLoading={isLoading} />
    },
    {
      key: 'Liên kết',
      value: 'link',
      icon: <Icon icon={shareFill} width={20} height={20} />,
      component: <SocialLinks profileShop={myProfile} />
    },
    {
      key: 'change_password',
      value: 'Đổi mật khẩu',
      icon: <Icon icon={roundVpnKey} width={20} height={20} />,
      component: <ChangePassword isLoading={isLoading} />
    }
  ];

  const handleChangeTab = (event, newValue) => {
    setCurrentTab(newValue);
  };
  if (isLoading) {
    return <LinearProgress />;
  }

  return (
    <Page title="Management | Account Settings" className={classes.root}>
      <Container>
        <HeaderDashboard
          heading="Tài khoản cửa hàng"
          links={[
            { name: 'Bảng điều khiển', href: PATH_APP.root },
            { name: 'Quản lí', href: PATH_APP.management.root },
            { name: 'Cửa hàng', href: PATH_APP.management.shop.root },
            { name: 'Tài khoản cửa hàng' }
          ]}
        />

        <div className={classes.profile}>
          <div className={classes.profileHeaderImageContainer}>
            <img
              className={classes.profileHeaderImage}
              alt={'product'}
              data-src="https://cdn.somedomain.tech/samples/leaf.jpg"
              src="https://img3.thuthuatphanmem.vn/uploads/2019/07/12/anh-bia-fb-don-gian_062636624.jpg"
            />
          </div>
          <div className={classes.profileUser}>
            <img
              alt={myProfile.fullName}
              className={classes.profileAvatar}
              src={
                myProfile.avatar ||
                'https://image.freepik.com/free-vector/shop-icon-vector_7315-22.jpg'
              }
            />
            <div className={classes.profileDetails}>
              <h1 className={classes.profileName}>{myProfile.agencyName}</h1>
              <p className={classes.profileSubtitle}>
                Email: {myProfile.email}
              </p>
            </div>

            <div className={classes.profileActions}></div>
          </div>
        </div>
        <Tabs
          value={currentTab}
          scrollButtons="auto"
          variant="scrollable"
          allowScrollButtonsMobile
          onChange={handleChangeTab}
          className={classes.tabBar}
        >
          {ACCOUNT_TABS.map(tab => (
            <Tab
              disableRipple
              key={tab.key}
              label={tab.value}
              icon={tab.icon}
              value={tab.key}
            />
          ))}
        </Tabs>

        {ACCOUNT_TABS.map(tab => {
          const isMatched = tab.key === currentTab;
          return isMatched && <Box key={tab.key}>{tab.component}</Box>;
        })}
      </Container>
    </Page>
  );
}

export default AccountView;
