import { filter } from 'lodash';
import HeadTable from './HeadTable';
import Page from '../../../components/Page';
import { Icon } from '@iconify/react';
import ToolbarTable from './ToolbarTable';
import { PATH_APP } from '../../../routes/paths';
import { sentenceCase } from 'change-case';
import Scrollbars from '../../../components/Scrollbars';
import { getUserList } from '../../../redux/slices/user';
import React, { useState, useEffect } from 'react';
import { visuallyHidden } from '@material-ui/utils';
import { useDispatch, useSelector } from 'react-redux';
import SearchNotFound from '../../../components/SearchNotFound';
import HeaderDashboard from '../../../components/HeaderDashboard';
import moreVerticalFill from '@iconify-icons/eva/more-vertical-fill';
import { useTheme, makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Card,
  Table,
  Avatar,
  TableRow,
  TableBody,
  TableCell,
  Container,
  IconButton,
  Typography,
  TableContainer,
  TablePagination
} from '@material-ui/core';
import { MLabel } from '../../../@material-extend';
import LoadingScreen from '../../../components/LoadingScreen';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'name', label: 'Họ tên', alignRight: false },
  { id: 'email', label: 'Email', alignRight: false },
  { id: 'role', label: 'Vai trò', alignRight: false },
  { id: 'status', label: 'Trạng thái', alignRight: false },
  { id: 'wait', alignRight: false },
  { id: '' }
];

const useStyles = makeStyles(theme => ({
  root: {},
  fullName: {
    cursor: 'pointer',
    '&:hover': {
      color: 'rgb(7, 177, 77, 0.42)'
    }
  },
  sortSpan: visuallyHidden
}));

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    array = filter(array, _user => {
      return _user.fullName.toLowerCase().indexOf(query.toLowerCase()) !== -1;
    });
    return array;
  }
  return stabilizedThis.map(el => el[0]);
}

function UserListView(props) {
  const classes = useStyles();
  const theme = useTheme();
  const dispatch = useDispatch();
  const { userList, isLoading } = useSelector(state => state.user);
  const [users, setUsers] = useState([]);
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState('asc');
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState('name');
  const [filterName, setFilterName] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(5);

  useEffect(() => {
    dispatch(getUserList());
  }, [dispatch]);

  useEffect(() => {
    setUsers(userList);
  }, [userList]);

  if (isLoading) {
    return <LoadingScreen />;
  }
  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };
  const renderStatus = status => {
    if (status === 'returned' || status === 'rented') {
      return 'makeStyles-ghostSuccess-97';
    }
    if (status === 'cancel') {
      return 'makeStyles-ghostError-82';
    }
    return 'makeStyles-ghostWarning-81';
  };

  const statusOfOrder = status => {
    switch (status) {
      case 'unverified':
        return 'Chưa xác thực';
      case 'verifying':
        return 'Đợi xác thực';
      case 'verified':
        return 'Đã xác thực';
      case 'blocked':
        return 'Bị khóa';
      default:
        break;
    }
  };
  const handleSelectAllClick = event => {
    if (event.target.checked) {
      const newSelecteds = userList.map(n => n.fullName);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  // const handleClick = (event, name) => {
  //   const selectedIndex = selected.indexOf(name);
  //   let newSelected = [];
  //   if (selectedIndex === -1) {
  //     newSelected = newSelected.concat(selected, name);
  //   } else if (selectedIndex === 0) {
  //     newSelected = newSelected.concat(selected.slice(1));
  //   } else if (selectedIndex === selected.length - 1) {
  //     newSelected = newSelected.concat(selected.slice(0, -1));
  //   } else if (selectedIndex > 0) {
  //     newSelected = newSelected.concat(
  //       selected.slice(0, selectedIndex),
  //       selected.slice(selectedIndex + 1)
  //     );
  //   }
  //   setSelected(newSelected);
  // };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = event => {
    setFilterName(event.target.value);
  };

  const handleClickUser = id => {
    props.history.push('/app/management/user/account/' + id);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - userList.length) : 0;

  const filteredUsers = applySortFilter(
    users,
    getComparator(order, orderBy),
    filterName
  );

  const isUserNotFound = filteredUsers.length === 0;
  return (
    <Page title="Quản lý | Danh sách người dùng" className={classes.root}>
      <Container>
        <HeaderDashboard
          heading="Danh sách người dùng"
          links={[
            { name: 'Bảng điều khiển', href: PATH_APP.root },
            { name: 'Quản lý', href: PATH_APP.management.root },
            { name: 'Người dùng', href: PATH_APP.management.user.root },
            { name: 'Danh sách người dùng' }
          ]}
        />
        <Card className={classes.card}>
          <ToolbarTable
            setUsers={setUsers}
            userList={userList}
            numSelected={selected.length}
            filterName={filterName}
            onFilterName={handleFilterByName}
          />

          <Scrollbars>
            <TableContainer component={Box} sx={{ minWidth: 800 }}>
              <Table>
                <HeadTable
                  order={order}
                  classes={classes}
                  orderBy={orderBy}
                  headLabel={TABLE_HEAD}
                  rowCount={userList.length}
                  numSelected={selected.length}
                  onRequestSort={handleRequestSort}
                  onSelectAllClick={handleSelectAllClick}
                />
                <TableBody>
                  {filteredUsers
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row, index) => {
                      const { _id, fullName, accountId, email, avatar } = row;
                      const isItemSelected = selected.indexOf(fullName) !== -1;
                      const status = 'banned';
                      return (
                        <TableRow
                          hover
                          key={_id}
                          tabIndex={-1}
                          role="checkbox"
                          selected={isItemSelected}
                          aria-checked={isItemSelected}
                          className={classes.row}
                        >
                          {/* <TableCell
                            padding="checkbox"
                            onClick={event => handleClick(event, fullName)}
                          >
                            <Checkbox checked={isItemSelected} />
                          </TableCell> */}
                          <TableCell
                            component="th"
                            scope="row"
                            padding="none"
                            className={classes.fullName}
                            onClick={() => handleClickUser(accountId._id)}
                          >
                            <Box
                              sx={{
                                py: 2,
                                display: 'flex',
                                alignItems: 'center'
                              }}
                            >
                              <Box
                                component={Avatar}
                                alt={fullName}
                                src={
                                  avatar ||
                                  'https://www.google.com/url?sa=i&url=https%3A%2F%2Fpngtree.com%2Fso%2Fuser&psig=AOvVaw26D7-_LqLtHfURwhy7YO2Q&ust=1619429183150000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCLiB9PuJmfACFQAAAAAdAAAAABAN'
                                }
                                sx={{ mx: 2 }}
                              />
                              <Typography variant="subtitle2" noWrap>
                                {fullName}
                              </Typography>
                            </Box>
                          </TableCell>
                          <TableCell align="left">{email}</TableCell>
                          <TableCell align="left">
                            {accountId?.role.roleName}
                          </TableCell>
                          <TableCell align="left">
                            <MLabel
                              title={sentenceCase(status)}
                              variant={
                                theme.palette.mode === 'light'
                                  ? 'ghost'
                                  : 'filled'
                              }
                              color={
                                ((accountId?.status === 'locked' ||
                                  accountId?.status === 'blocked') &&
                                  'error') ||
                                (accountId?.status === 'verified' &&
                                  'success') ||
                                ''
                              }
                            >
                              <span
                                className={`makeStyles-root-79 makeStyles-ghost-93 ${renderStatus(
                                  accountId?.status
                                )} css-1a8w37c`}
                              >
                                {statusOfOrder(accountId?.status)}
                              </span>
                            </MLabel>
                          </TableCell>
                          <TableCell align="right">
                            {accountId?.status === 'verifying' && (
                              <FiberManualRecordIcon
                                style={{ color: 'red', fontSize: '0.8rem' }}
                              />
                            )}
                          </TableCell>
                          <TableCell align="right">
                            <IconButton className={classes.margin}>
                              <Icon
                                icon={moreVerticalFill}
                                width={20}
                                height={20}
                              />
                            </IconButton>
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
                {isUserNotFound && (
                  <TableBody>
                    <TableRow>
                      <TableCell align="center" colSpan={6}>
                        <Box sx={{ py: 3 }}>
                          <SearchNotFound searchQuery={filterName} />
                        </Box>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                )}
              </Table>
            </TableContainer>
          </Scrollbars>

          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={users.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>
      </Container>
    </Page>
  );
}

export default UserListView;
