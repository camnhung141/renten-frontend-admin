import clsx from 'clsx';
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Icon } from '@iconify/react';
import searchFill from '@iconify-icons/eva/search-fill';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Toolbar,
  Typography,
  MenuItem,
  Select,
  FormControl,
  OutlinedInput,
  InputAdornment
} from '@material-ui/core';
// import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';

// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => {
  const isLight = theme.palette.mode === 'light';
  return {
    root: {
      height: 96,
      display: 'flex',
      justifyContent: 'space-between',
      padding: theme.spacing(0, 1, 0, 3)
    },
    search: {
      width: 240,
      transition: theme.transitions.create(['box-shadow', 'width'], {
        easing: theme.transitions.easing.easeInOut,
        duration: theme.transitions.duration.shorter
      }),
      '&.Mui-focused': { width: 320, boxShadow: theme.shadows[25].z8 },
      '& fieldset': {
        borderWidth: `1px !important`,
        borderColor: `${theme.palette.grey[500_32]} !important`
      }
    },
    margin: {
      minWidth: '300px',
      height: '60%'
    },
    highlight: isLight
      ? {
          color: theme.palette.primary.main,
          backgroundColor: theme.palette.primary.lighter
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.primary.dark
        }
  };
});

// ----------------------------------------------------------------------

ToolbarTable.propTypes = {
  numSelected: PropTypes.number,
  filterName: PropTypes.string,
  onFilterName: PropTypes.func,
  className: PropTypes.string
};

function ToolbarTable({
  numSelected,
  filterName,
  onFilterName,
  className,
  setUsers,
  userList
}) {
  const classes = useStyles();
  const [status, setStatus] = useState('all');
  useEffect(() => {
    if (status && status !== 'all' && status !== '') {
      const list = userList?.filter(user => user.accountId.status === status);
      setUsers(list);
    } else {
      setUsers(userList);
    }
  }, [userList, status]);

  return (
    <Toolbar
      className={clsx(
        classes.root,
        { [classes.highlight]: numSelected > 0 },
        className
      )}
    >
      {numSelected > 0 ? (
        <Typography component="div" variant="subtitle1">
          {numSelected} selected
        </Typography>
      ) : (
        <OutlinedInput
          value={filterName}
          onChange={onFilterName}
          placeholder="Tìm kiếm..."
          startAdornment={
            <InputAdornment position="start">
              <Box
                component={Icon}
                icon={searchFill}
                sx={{ color: 'text.disabled' }}
              />
            </InputAdornment>
          }
          className={classes.search}
        />
      )}
      <Box sx={{ py: 3, textAlign: 'left' }}>
        <Typography variant="subtitle2" color="textSecondary" gutterBottom>
          Trạng thái
        </Typography>
        <FormControl variant="outlined" className={classes.margin} required>
          <Select
            labelId="category"
            id="status"
            required
            value={status}
            onChange={e => setStatus(e.target.value)}
            size="small"
            className={classes.margin}
          >
            <MenuItem value="all">Tất cả</MenuItem>
            <MenuItem value="unverified">Chưa xác thực</MenuItem>
            <MenuItem value="verifying">Đợi xác thực</MenuItem>
            <MenuItem value="verified">Đã xác thực</MenuItem>
            <MenuItem value="block">Bị khóa</MenuItem>
          </Select>
        </FormControl>
      </Box>
      {/* <div>
        <FiberManualRecordIcon style={{ color: 'red', fontSize: '0.8rem' }} />
        <span style={{ color: 'gray', fontSize: '0.8rem' }}>
          {' '}
          Chờ xác thực tài khoản
        </span>
      </div> */}
    </Toolbar>
  );
}

export default ToolbarTable;
