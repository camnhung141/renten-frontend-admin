import clsx from 'clsx';
import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Card, Typography } from '@material-ui/core';

// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {},
  addressItem: {
    position: 'relative',
    padding: theme.spacing(3),
    marginTop: theme.spacing(3),
    borderRadius: theme.shape.borderRadius,
    backgroundColor: theme.palette.background.neutral
  }
}));

// ----------------------------------------------------------------------

AddressBook.propTypes = {
  addressBook: PropTypes.array,
  className: PropTypes.string
};

function AddressBook({ addressBook, className }) {
  const classes = useStyles();

  return (
    <Card className={clsx(classes.root, className)}>
      <Typography variant="overline" color="textSecondary">
        Thông tin hóa đơn
      </Typography>

      {addressBook.map(address => (
        <div key={address._id} className={classes.addressItem}>
          <Typography variant="subtitle1" gutterBottom>
            {address.fullName}
          </Typography>

          <Typography variant="body2" gutterBottom>
            <Typography variant="body2" component="span" color="textSecondary">
              Address: &nbsp;
            </Typography>
            {`${address.streetAndNumber}, ${address.wardPrefix} ${address.wardName}, ${address.districtPrefix} ${address.districtName}, ${address.cityPrefix} ${address.cityName}`}
          </Typography>

          <Typography variant="body2" gutterBottom>
            <Typography variant="body2" component="span" color="textSecondary">
              Phone: &nbsp;
            </Typography>
            {address.phoneNumber}
          </Typography>

          <Box sx={{ mt: 1 }}>
            {/* <MButton
              color="error"
              size="small"
              startIcon={<Icon icon={trash2Fill} />}
              onClick={() => {}}
              sx={{ mr: 1 }}
            >
              Delete
            </MButton>
            <Button
              size="small"
              color="primary"
              startIcon={<Icon icon={editFill} />}
              onClick={() => {}}
            >
              Edit
            </Button> */}
          </Box>
        </div>
      ))}

      {/* <Button size="small" startIcon={<Icon icon={plusFill} />} sx={{ mt: 3 }}>
        Add new address
      </Button> */}
    </Card>
  );
}

export default AddressBook;
