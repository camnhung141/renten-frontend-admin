import clsx from 'clsx';
import React from 'react';
import * as Yup from 'yup';
import PropTypes from 'prop-types';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import { sentenceCase } from 'change-case';
import useIsMountedRef from '../../../hooks/useIsMountedRef';
import { Form, FormikProvider, useFormik } from 'formik';
import { UploadAvatar } from '../../../components/Upload';
import { useTheme, makeStyles } from '@material-ui/core/styles';
import { Box, Grid, Card, TextField, CardContent } from '@material-ui/core';
import { LoadingButton } from '@material-ui/lab';
import { MLabel } from '../../../@material-extend';
import authService from '../../../apis/auth.api';
// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {},
  margin: {
    color: 'green'
  }
}));

// ----------------------------------------------------------------------

General.propTypes = {
  className: PropTypes.string
};

function General({ className, profile }) {
  const classes = useStyles();
  const theme = useTheme();
  const isMountedRef = useIsMountedRef();
  const { enqueueSnackbar } = useSnackbar();

  const handleVerify = async () => {
    await authService.verify({
      accountId: profile.accountId._id,
      valid: true
    });
    window.location.reload();
  };

  const handleUnVerify = async () => {
    await authService.verify({
      accountId: profile.accountId._id,
      valid: false
    });
    window.location.reload();
  };

  const UpdateUserSchema = Yup.object().shape({
    name: Yup.string().required('Name is required')
  });

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      name: profile.fullName || '',
      email: profile.email,
      avatar: profile.avatar || '',
      phoneNumber: profile.phoneNumber || '',
      gender: profile.gender || '',
      birthday: moment(profile.dateOfBirth).format('DD/MM/YYYY') || '',
      about: profile.about || '',
      isPublic: false
    },
    validationSchema: UpdateUserSchema,
    onSubmit: async ({ setErrors, setSubmitting }) => {
      try {
        enqueueSnackbar('Update success', { variant: 'success' });
        if (isMountedRef.current) {
          setSubmitting(false);
        }
      } catch (err) {
        if (isMountedRef.current) {
          setErrors({ afterSubmit: err.code });
          setSubmitting(false);
        }
      }
    }
  });

  const { handleSubmit, getFieldProps, setFieldValue } = formik;

  return (
    <div className={clsx(classes.root, className)}>
      <FormikProvider value={formik}>
        <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
          <Grid container spacing={3}>
            <Grid item xs={12} md={4}>
              <Card>
                <Box
                  sx={{
                    my: 10,
                    display: 'flex',
                    alignItems: 'center',
                    flexDirection: 'column'
                  }}
                >
                  <UploadAvatar
                    disabled={profile.email} // You can remove this
                    value={
                      profile.avatar ||
                      profile.identityCard?.faceImage ||
                      'https://scontent-xsp1-2.xx.fbcdn.net/v/t1.6435-9/83026747_1041350829579747_3440624298539089920_n.jpg?_nc_cat=104&ccb=1-3&_nc_sid=09cbfe&_nc_ohc=wADePBxB7qwAX9De0Pv&_nc_ht=scontent-xsp1-2.xx&oh=0f6359b4821a8d87065d2671a8f5c7f1&oe=60B03E9B'
                    }
                    onChange={value => setFieldValue('avatar', value)}
                  />
                  <div style={{ marginTop: '10px', color: 'green' }}>
                    {profile.fullName}
                  </div>
                  <div
                    container
                    style={{ textAlign: 'center', marginTop: '2rem' }}
                  >
                    <div style={{ marginBottom: '10px' }}>
                      <span style={{ color: 'gray' }}>Trạng thái:</span>{' '}
                      <MLabel
                        title={sentenceCase(profile.accountId?.status)}
                        variant={
                          theme.palette.mode === 'light' ? 'ghost' : 'filled'
                        }
                        color={
                          ((profile.accountId?.status === 'locked' ||
                            profile.accountId?.status === 'blocked') &&
                            'error') ||
                          (profile.accountId?.status === 'verified' &&
                            'success') ||
                          ''
                        }
                      >
                        {sentenceCase(profile.accountId?.status)}
                      </MLabel>
                    </div>
                    {profile.accountId.status === 'verifying' && (
                      <>
                        <LoadingButton
                          variant="contained"
                          color="primary"
                          onClick={handleUnVerify}
                        >
                          Không xác thực
                        </LoadingButton>
                        <span>
                          {' '}
                          <LoadingButton
                            variant="contained"
                            color="primary"
                            onClick={handleVerify}
                          >
                            Xác thực
                          </LoadingButton>
                        </span>
                      </>
                    )}
                    {/* {profile.accountId.status === 'verified' && (
                      <LoadingButton variant="contained">
                        Khóa tài khoản
                      </LoadingButton>
                    )} */}
                  </div>
                </Box>
              </Card>
            </Grid>

            <Grid item xs={12} md={8}>
              <Card>
                <CardContent>
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        disabled={profile.email} // You can remove this
                        fullWidth
                        label="Họ tên"
                        {...getFieldProps('name')}
                      />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                      <TextField
                        disabled
                        fullWidth
                        label="Địa chỉ email"
                        {...getFieldProps('email')}
                      />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                      <TextField
                        disabled
                        fullWidth
                        label="Số điện thoại"
                        {...getFieldProps('phoneNumber')}
                      />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                      <TextField
                        id="date"
                        disabled
                        label="Ngày sinh"
                        type="text"
                        fullWidth
                        {...getFieldProps('birthday')}
                        className={classes.textField}
                      />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                      <TextField
                        type="text"
                        disabled
                        fullWidth
                        label="Giới tính"
                        placeholder="Giới tính"
                        {...getFieldProps('gender')}
                        className={classes.margin}
                      />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                      <TextField
                        disabled
                        fullWidth
                        label="Trạng thái tài khoản"
                        value={profile.accountId.status}
                      />
                    </Grid>

                    {profile.identityCard && (
                      <>
                        <Grid item xs={12} sm={6}>
                          <TextField
                            fullWidth
                            disabled
                            label="Số CMND/CCCD"
                            value={profile.identityCard?.identityNumber}
                          />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                          <TextField
                            fullWidth
                            disabled
                            label="Ngày cấp"
                            type="date"
                            className={classes.textField}
                            InputLabelProps={{
                              shrink: true
                            }}
                            defaultValue={moment(
                              profile.identityCard?.dateOfIssue
                            ).format('YYYY-MM-DD')}
                          />
                        </Grid>
                        <Grid item xs={12}>
                          <TextField
                            fullWidth
                            disabled
                            label="Nơi cấp"
                            value={profile.identityCard?.placeOfIssue}
                          />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                          <div>Ảnh mặt trước</div>
                          <img
                            alt=""
                            src={profile.identityCard?.frontSideImage}
                            width="100%"
                            height="80%"
                          />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                          <div>Ảnh mặt sau</div>
                          <img
                            alt=""
                            width="100%"
                            height="80%"
                            src={profile.identityCard?.backSideImage}
                          />
                        </Grid>
                      </>
                    )}
                    <Grid item xs={12}>
                      <TextField
                        {...getFieldProps('about')}
                        fullWidth
                        disabled
                        multiline
                        minRows={4}
                        maxRows={4}
                        label="About"
                      />
                    </Grid>
                  </Grid>

                  <Box
                    sx={{ mt: 3, display: 'flex', justifyContent: 'flex-end' }}
                  >
                    {/* <LoadingButton
                      type="submit"
                      variant="contained"
                      pending={isSubmitting}
                    >
                      Lưu thay đổi
                    </LoadingButton> */}
                  </Box>
                </CardContent>
              </Card>
            </Grid>
          </Grid>
        </Form>
      </FormikProvider>
    </div>
  );
}

export default General;
