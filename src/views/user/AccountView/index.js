import General from './General';
import Billing from './Billing';
import InvoiceView from './InvoiceView';
import { Icon } from '@iconify/react';
import Page from '../../../components/Page';
import { useParams } from 'react-router-dom';
import { PATH_APP } from '../../../routes/paths';
import React, { useState, useEffect } from 'react';
import shoppingBagFill from '@iconify-icons/eva/shopping-bag-fill';
import { useDispatch, useSelector } from 'react-redux';
import roundReceipt from '@iconify-icons/ic/round-receipt';
import HeaderDashboard from '../../../components/HeaderDashboard';
import roundAccountBox from '@iconify-icons/ic/round-account-box';
import { getProfile } from '../../../redux/slices/user';
import { makeStyles } from '@material-ui/core/styles';
import { Container, Tab, Box, Tabs } from '@material-ui/core';

// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {},
  tabBar: {
    marginBottom: theme.spacing(5)
  }
}));

// ----------------------------------------------------------------------

function AccountView() {
  const classes = useStyles();
  const { id } = useParams();
  const [currentTab, setCurrentTab] = useState('general');
  const dispatch = useDispatch();
  const { myProfile } = useSelector(state => state.user);

  useEffect(() => {
    dispatch(getProfile(id));
  }, [dispatch, id]);

  if (!myProfile) {
    return null;
  }

  const ACCOUNT_TABS = [
    {
      value: 'Chung',
      key: 'general',
      icon: <Icon icon={roundAccountBox} width={20} height={20} />,
      component: <General profile={myProfile} />
    },
    {
      value: 'Thông tin Hóa đơn',
      key: 'bill',
      icon: <Icon icon={roundReceipt} width={20} height={20} />,
      component: <Billing accountId={myProfile.accountId._id} />
    },
    {
      value: 'Đơn hàng',
      key: 'order',
      icon: <Icon icon={shoppingBagFill} width={20} height={20} />,
      component: <InvoiceView accountId={myProfile.accountId._id} />
    }
  ];

  const handleChangeTab = (event, newValue) => {
    setCurrentTab(newValue);
  };

  return (
    <Page title="Quản lý | Tài khoản người dùng" className={classes.root}>
      <Container>
        <HeaderDashboard
          heading={'Tài khoản: ' + myProfile.fullName}
          links={[
            { name: 'Bảng điều khiển', href: PATH_APP.root },
            { name: 'Quản lý', href: PATH_APP.management.root },
            { name: 'Người dùng', href: PATH_APP.management.user.root },
            { name: 'Tài khoản người dùng' }
          ]}
        />

        <Tabs
          value={currentTab}
          scrollButtons="auto"
          variant="scrollable"
          allowScrollButtonsMobile
          onChange={handleChangeTab}
          className={classes.tabBar}
        >
          {ACCOUNT_TABS.map(tab => (
            <Tab
              disableRipple
              key={tab.key}
              label={tab.value}
              icon={tab.icon}
              value={tab.key}
            />
          ))}
        </Tabs>

        {ACCOUNT_TABS.map(tab => {
          const isMatched = tab.key === currentTab;
          return isMatched && <Box key={tab.key}>{tab.component}</Box>;
        })}
      </Container>
    </Page>
  );
}

export default AccountView;
