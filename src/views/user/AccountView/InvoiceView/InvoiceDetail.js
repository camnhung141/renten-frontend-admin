import React, { useEffect } from 'react';
import Toolbar from '../../../order/InvoiceView/Toolbar';
import Page from '../../../../components/Page';
import CardInvoice from '../../../order/InvoiceView/CardInvoice';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { Container } from '@material-ui/core';
import { getDetailOrder } from '../../../../redux/slices/order';

// ----------------------------------------------------------------------
const useStyles = makeStyles(theme => ({
  root: {},
  card: {
    padding: theme.spacing(5, 5, 0)
  },
  gridItem: {
    marginBottom: theme.spacing(5)
  },
  tableHead: {
    borderBottom: `solid 1px ${theme.palette.divider}`,
    '& th': {
      backgroundColor: 'transparent'
    }
  },
  row: {
    borderBottom: `solid 1px ${theme.palette.divider}`
  },
  rowResult: {
    '& td': {
      paddingTop: theme.spacing(1),
      paddingBottom: theme.spacing(1)
    }
  }
}));

function InvoiceView({ id }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const orderDetail = useSelector(state => state.order.orderDetail);
  useEffect(() => {
    dispatch(getDetailOrder(id));
  }, [dispatch, id]);

  return (
    <Page title="Management | Invoice Details" className={classes.root}>
      <Container>
        <Toolbar orderDetail={orderDetail} />
        <CardInvoice orderDetail={orderDetail} />
      </Container>
    </Page>
  );
}

export default InvoiceView;
