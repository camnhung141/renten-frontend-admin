import React from 'react';
import * as Yup from 'yup';
import Section from './Section';
import { useFormik } from 'formik';
import LoginForm from './LoginForm';
import Page from '../../../components/Page';
import Logo from '../../../components/Logo';
import { Icon } from '@iconify/react';
import { useSnackbar } from 'notistack';
import { PATH_PAGE } from '../../../routes/paths';
import closeFill from '@iconify-icons/eva/close-fill';
import { Link as RouterLink, useHistory } from 'react-router-dom';
import useIsMountedRef from '../../../hooks/useIsMountedRef';
import { otherError } from '../../../utils/firebaseShowError';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Link,
  useMediaQuery,
  Container,
  Typography
} from '@material-ui/core';
import { MIconButton } from '../../../@material-extend';
import { useDispatch } from 'react-redux';
import { login } from '../../../redux/slices/auth';
import authService from '../../../apis/auth.api';

// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {
    [theme.breakpoints.up('md')]: {
      display: 'flex'
    }
  },
  header: {
    top: 0,
    zIndex: 9,
    lineHeight: 0,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    position: 'absolute',
    padding: theme.spacing(3),
    justifyContent: 'space-between',
    [theme.breakpoints.up('md')]: {
      alignItems: 'flex-start',
      padding: theme.spacing(7, 5, 0, 7)
    }
  },
  content: {
    maxWidth: 480,
    margin: 'auto',
    display: 'flex',
    minHeight: '100vh',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: theme.spacing(12, 0)
  },
  divider: {
    margin: theme.spacing(3, 0)
  }
}));

// ----------------------------------------------------------------------

function LoginView() {
  const classes = useStyles();
  const history = useHistory();
  const isMountedRef = useIsMountedRef();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const dispatch = useDispatch();
  const LoginSchema = Yup.object().shape({
    email: Yup.string()
      // .email('Email must be a valid email address')
      .required('Email/Username is required'),
    password: Yup.string().required('Password is required')
  });

  const formik = useFormik({
    initialValues: {
      email: 'quynh@gmail.com',
      password: 'nhung',
      remember: true
    },
    validationSchema: LoginSchema,
    onSubmit: async (values, { setErrors, setSubmitting }) => {
      try {
        await authService.login({
          email: values.email,
          password: values.password
        });
        dispatch(login());
        enqueueSnackbar('Đăng nhập thành công', {
          variant: 'success',
          action: key => (
            <MIconButton size="small" onClick={() => closeSnackbar(key)}>
              <Icon icon={closeFill} />
            </MIconButton>
          )
        });
        history.push('/app/dashboard');
        if (isMountedRef.current) {
          setSubmitting(false);
        }
      } catch (err) {
        enqueueSnackbar('Tên đăng nhập hoặc mật khẩu không hợp lệ', {
          variant: 'error',
          action: key => (
            <MIconButton size="small" onClick={() => closeSnackbar(key)}>
              <Icon icon={closeFill} />
            </MIconButton>
          )
        });
        if (isMountedRef.current) {
          setSubmitting(false);
          setErrors({ afterSubmit: err.code });
        }
        if (otherError(err.code).error) {
          enqueueSnackbar(otherError(err.code).helperText, {
            variant: 'error'
          });
        }
      }
    }
  });
  const hidden = useMediaQuery(theme => theme.breakpoints.up('xl'));

  return (
    <Page title="Rent Mart | Login" className={classes.root}>
      <header className={classes.header}>
        <RouterLink to="/">
          <Logo />
        </RouterLink>
        {hidden && (
          <Box sx={{ mt: { md: -2 }, typography: 'body2' }}>
            Chưa có tài khoản? &nbsp;
            <Link
              underline="none"
              variant="subtitle2"
              component={RouterLink}
              to={PATH_PAGE.auth.register}
            >
              Đăng ký
            </Link>
          </Box>
        )}
      </header>

      {hidden && <Section />}

      <Container>
        <div className={classes.content}>
          <Box sx={{ mb: 5 }}>
            <Typography variant="h4" gutterBottom>
              Đăng nhập quản lý Rent Mart
            </Typography>
            <Typography color="textSecondary">
              Nhập thông tin bên dưới để đăng nhập
            </Typography>
          </Box>

          {/* <SocialLogin /> */}

          {/* <Divider className={classes.divider}>
            <Typography variant="body2" color="textSecondary">
              Hoặc
            </Typography>
          </Divider> */}
          <Box sx={{ mb: 5 }}>
            {/* <Alert severity="info">
              Use email : <strong>demo@minimals.cc</strong> / password :
              <strong>&nbsp;demo1234</strong>
            </Alert> */}
          </Box>

          <LoginForm formik={formik} />

          {!hidden && (
            <Box sx={{ mt: 3, typography: 'body2', textAlign: 'center' }}>
              Chưa có tài khoản?&nbsp;
              <Link
                variant="subtitle2"
                to={PATH_PAGE.auth.register}
                component={RouterLink}
              >
                Đăng ký
              </Link>
            </Box>
          )}
        </div>
      </Container>
    </Page>
  );
}

export default LoginView;
