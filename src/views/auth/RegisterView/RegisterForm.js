import { Icon } from '@iconify/react';
import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { Form, FormikProvider } from 'formik';
import eyeFill from '@iconify-icons/eva/eye-fill';
import { makeStyles } from '@material-ui/core/styles';
import eyeOffFill from '@iconify-icons/eva/eye-off-fill';
import {
  Box,
  Grid,
  TextField,
  IconButton,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  InputAdornment
} from '@material-ui/core';
import { LoadingButton } from '@material-ui/lab';
import { useDispatch, useSelector } from 'react-redux';
import {
  getCities,
  getDistricts,
  getWards,
  getIDistricts,
  getIWards
} from '../../../redux/slices/address';
// ----------------------------------------------------------------------

RegisterForm.propTypes = {
  formik: PropTypes.object.isRequired
};

const useStyles = makeStyles(theme => ({
  root: {},
  margin: {
    marginBottom: theme.spacing(3)
  },
  helperText: {
    padding: theme.spacing(0, 2)
  },
  addBtn: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  card: {
    margin: '1rem'
  }
}));
function RegisterForm({
  formik,
  city,
  district,
  ward,
  setCity,
  setDistrict,
  setWard,
  icity,
  idistrict,
  iward,
  setICity,
  setIDistrict,
  gender,
  setGender,
  setIWard
}) {
  const classes = useStyles();
  const [showPassword, setShowPassword] = useState(false);
  const [showCnfPassword, setShowCnfPassword] = useState(false);
  const dispatch = useDispatch();
  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;
  const cities = useSelector(state => state.address.cities);
  const districts = useSelector(state => state.address.districts);
  const wards = useSelector(state => state.address.wards);
  const idistricts = useSelector(state => state.address.idistricts);
  const iwards = useSelector(state => state.address.iwards);

  useEffect(() => {
    dispatch(getDistricts(city.cityCode));
  }, [dispatch, city]);

  useEffect(() => {
    dispatch(getWards(district.districtCode));
  }, [dispatch, district]);

  useEffect(() => {
    dispatch(getCities());
  }, [dispatch]);

  useEffect(() => {
    dispatch(getIDistricts(icity.cityCode));
  }, [dispatch, icity]);

  useEffect(() => {
    dispatch(getIWards(idistrict.districtCode));
  }, [dispatch, idistrict]);

  return (
    <FormikProvider value={formik}>
      <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
        <p style={{ fontSize: '1.5rem', color: 'green' }}>
          1. Thông tin người đại diện
        </p>
        <Box sx={{ mb: 3 }} />
        <Grid container spacing={2}>
          <Grid item xs={12} sm={6}>
            <TextField
              fullWidth
              label="Họ và tên người đại diện"
              {...getFieldProps('fullName')}
              error={Boolean(touched.fullName && errors.fullName)}
              helperText={touched.fullName && errors.fullName}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              fullWidth
              name="phone"
              label="Số điện thoại của chủ của hàng"
              {...getFieldProps('phone')}
              error={Boolean(touched.phone && errors.phone)}
              helperText={touched.phone && errors.phone}
            />
          </Grid>
        </Grid>
        <Box sx={{ mb: 3 }} />
        <Grid container spacing={2}>
          <Grid item xs={12} sm={6}>
            <FormControl
              variant="outlined"
              className={classes.margin}
              fullWidth
              required
            >
              <InputLabel id="district">Giới tính</InputLabel>
              <Select
                labelId="district"
                id="district"
                value={gender}
                onChange={e => setGender(e.target.value)}
                label="Giới tính"
                fullWidth
                required
              >
                <MenuItem value="Nữ">Nữ</MenuItem>
                <MenuItem value="Nam">Nam</MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              fullWidth
              type="date"
              name="dateOfBirth"
              label="Ngày sinh"
              InputLabelProps={{ shrink: true, required: true }}
              {...getFieldProps('dateOfBirth')}
              error={Boolean(touched.dateOfBirth && errors.dateOfBirth)}
              helperText={touched.dateOfBirth && errors.dateOfBirth}
            />
          </Grid>
        </Grid>

        <Box sx={{ mb: 3 }} />
        <Grid container spacing={2}>
          <Grid item xs={12} sm={6}>
            {/* <TextField
              fullWidth
              name="city"
              label="Thành phố"
              {...getFieldProps('city')}
              error={Boolean(touched.city && errors.city)}
              helperText={touched.city && errors.city}
            /> */}

            <FormControl
              variant="outlined"
              className={classes.margin}
              fullWidth
              required
            >
              <InputLabel id="category">Tỉnh/Thành phố</InputLabel>
              <Select
                labelId="city"
                id="city"
                value={icity}
                onChange={e => setICity(e.target.value)}
                label="Tỉnh/Thành phố"
                fullWidth
                required
              >
                {cities.map(element => (
                  <MenuItem value={element}>{element.cityName}</MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={6}>
            {/* <TextField
              fullWidth
              name="district"
              label="Quận/Huyện"
              {...getFieldProps('district')}
              error={Boolean(touched.district && errors.district)}
              helperText={touched.district && errors.district}
            /> */}
            <FormControl
              variant="outlined"
              className={classes.margin}
              fullWidth
              required
            >
              <InputLabel id="category">Quận/Huyện</InputLabel>
              <Select
                labelId="idistrict"
                id="idistrict"
                value={idistrict}
                onChange={e => setIDistrict(e.target.value)}
                label="Quận/Huyện"
                fullWidth
                required
              >
                {idistricts.map(element => (
                  <MenuItem value={element}>{element.districtName}</MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
        </Grid>

        <Box sx={{ mb: 3 }} />

        <Grid container spacing={2}>
          <Grid item xs={12} sm={6}>
            {/* <TextField
              fullWidth
              name="ward"
              label="Phường/Xã"
              {...getFieldProps('ward')}
              error={Boolean(touched.ward && errors.ward)}
              helperText={touched.ward && errors.ward}
            /> */}
            <FormControl
              variant="outlined"
              className={classes.margin}
              fullWidth
              required
            >
              <InputLabel id="iward">Phường/Xã</InputLabel>
              <Select
                labelId="iward"
                id="iward"
                value={iward}
                onChange={e => setIWard(e.target.value)}
                label="Phường/Xã"
                fullWidth
                required
              >
                {iwards.map(element => (
                  <MenuItem value={element}>{element.wardName}</MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              fullWidth
              name="street"
              label="Địa chỉ"
              {...getFieldProps('street')}
              error={Boolean(touched.street && errors.street)}
              helperText={touched.street && errors.street}
            />
          </Grid>
        </Grid>
        <Box sx={{ mt: 3 }}>
          <p style={{ fontSize: '1.5rem', color: 'green' }}>
            2. Thông tin của cửa hàng
          </p>
          <Box sx={{ mb: 3 }} />
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                name="shop"
                label="Tên cửa hàng"
                {...getFieldProps('agencyName')}
                error={Boolean(touched.agencyName && errors.agencyName)}
                helperText={touched.agencyName && errors.agencyName}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                name="shopPhone"
                label="Hotline/Số điện thoại liên hệ"
                {...getFieldProps('shopPhone')}
                error={Boolean(touched.shopPhone && errors.shopPhone)}
                helperText={touched.shopPhone && errors.shopPhone}
              />
            </Grid>
          </Grid>
          <Box sx={{ mb: 3 }} />
          <TextField
            fullWidth
            name="fanPage"
            label="Link FanPage/facebook"
            {...getFieldProps('fanPage')}
            error={Boolean(touched.fanPage && errors.fanPage)}
            helperText={touched.fanPage && errors.fanPage}
          />
          <Box sx={{ mb: 3 }} />
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <FormControl
                variant="outlined"
                className={classes.margin}
                fullWidth
                required
              >
                <InputLabel id="category">Tỉnh/Thành phố</InputLabel>
                <Select
                  labelId="city"
                  id="city"
                  value={city}
                  onChange={e => setCity(e.target.value)}
                  label="Tỉnh/Thành phố"
                  fullWidth
                  required
                >
                  {cities.map(element => (
                    <MenuItem value={element}>{element.cityName}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={6}>
              {/* <TextField
              fullWidth
              name="district"
              label="Quận/Huyện"
              {...getFieldProps('district')}
              error={Boolean(touched.district && errors.district)}
              helperText={touched.district && errors.district}
            /> */}
              <FormControl
                variant="outlined"
                className={classes.margin}
                fullWidth
                required
              >
                <InputLabel id="district">Quận/Huyện</InputLabel>
                <Select
                  labelId="district"
                  id="district"
                  value={district}
                  onChange={e => setDistrict(e.target.value)}
                  label="Quận/Huyện"
                  fullWidth
                  required
                >
                  {districts.map(element => (
                    <MenuItem value={element}>{element.districtName}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
          </Grid>

          <Box sx={{ mb: 3 }} />

          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              {/* <TextField
              fullWidth
              name="ward"
              label="Phường/Xã"
              {...getFieldProps('ward')}
              error={Boolean(touched.ward && errors.ward)}
              helperText={touched.ward && errors.ward}
            /> */}
              <FormControl
                variant="outlined"
                className={classes.margin}
                fullWidth
                required
              >
                <InputLabel id="ward">Phường/Xã</InputLabel>
                <Select
                  labelId="ward"
                  id="ward"
                  value={ward}
                  onChange={e => setWard(e.target.value)}
                  label="Phường/Xã"
                  fullWidth
                  required
                >
                  {wards.map(element => (
                    <MenuItem value={element}>{element.wardName}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                name="shopStreet"
                label="Địa chỉ"
                {...getFieldProps('shopStreet')}
                error={Boolean(touched.shopStreet && errors.shopStreet)}
                helperText={touched.shopStreet && errors.shopStreet}
              />
            </Grid>
          </Grid>
          <Box sx={{ mb: 3 }} />
          <TextField
            fullWidth
            name="about"
            label="Mô tả ngắn về cửa hàng"
            {...getFieldProps('about')}
            error={Boolean(touched.about && errors.about)}
            helperText={touched.about && errors.about}
          />
          <Box sx={{ mb: 3 }} />
          <p style={{ fontSize: '1.5rem', color: 'green' }}>
            3. Thông tin tài khoản
          </p>
          <Box sx={{ mb: 3 }} />
          <TextField
            fullWidth
            name="email"
            type="email"
            label="Địa chỉ email"
            {...getFieldProps('email')}
            error={Boolean(touched.email && errors.email)}
            helperText={touched.email && errors.email}
          />
          <Box sx={{ mb: 3 }} />
          <TextField
            fullWidth
            type={showPassword ? 'text' : 'password'}
            label="Mật khẩu"
            {...getFieldProps('password')}
            InputProps={{
              endAdornment: (
                <InputAdornment>
                  <IconButton
                    edge="end"
                    onClick={() => setShowPassword(prev => !prev)}
                  >
                    <Icon icon={showPassword ? eyeFill : eyeOffFill} />
                  </IconButton>
                </InputAdornment>
              )
            }}
            error={Boolean(touched.password && errors.password)}
            helperText={touched.password && errors.password}
          />
          <Box sx={{ mb: 3 }} />
          <TextField
            fullWidth
            type={showCnfPassword ? 'text' : 'password'}
            label="Xác nhận mật khẩu"
            {...getFieldProps('cnfPassword')}
            InputProps={{
              endAdornment: (
                <InputAdornment>
                  <IconButton
                    edge="end"
                    onClick={() => setShowCnfPassword(prev => !prev)}
                  >
                    <Icon icon={showCnfPassword ? eyeFill : eyeOffFill} />
                  </IconButton>
                </InputAdornment>
              )
            }}
            error={Boolean(touched.cnfPassword && errors.cnfPassword)}
            helperText={touched.cnfPassword && errors.cnfPassword}
          />
          <Box sx={{ mb: 3 }} />
          <LoadingButton
            fullWidth
            size="large"
            type="submit"
            variant="contained"
            pending={isSubmitting}
          >
            Đăng ký
          </LoadingButton>
        </Box>
      </Form>
    </FormikProvider>
  );
}

export default RegisterForm;
