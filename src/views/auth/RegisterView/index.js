import React, { useState } from 'react';
import * as Yup from 'yup';
import Section from './Section';
import { useFormik } from 'formik';
import Page from '../../../components/Page';
import Logo from '../../../components/Logo';
import RegisterForm from './RegisterForm';
import closeFill from '@iconify-icons/eva/close-fill';
import { Icon } from '@iconify/react';
import { useSnackbar } from 'notistack';
import { PATH_PAGE } from '../../../routes/paths';
// import SocialRegister from './SocialRegister';
import { Link as RouterLink, useHistory } from 'react-router-dom';
import useIsMountedRef from '../../../hooks/useIsMountedRef';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Link,
  useMediaQuery,
  // Divider,
  Container,
  Typography
} from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { MIconButton } from '../../../@material-extend';
import { login } from '../../../redux/slices/auth';
import authService from '../../../apis/auth.api';
// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {
    [theme.breakpoints.up('md')]: {
      display: 'flex'
    }
  },
  header: {
    top: 0,
    zIndex: 9,
    lineHeight: 0,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    position: 'absolute',
    padding: theme.spacing(3),
    justifyContent: 'space-between',
    [theme.breakpoints.up('md')]: {
      alignItems: 'flex-start',
      padding: theme.spacing(7, 5, 0, 7)
    }
  },
  content: {
    maxWidth: 980,
    margin: 'auto',
    display: 'flex',
    minHeight: '100vh',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: theme.spacing(12, 0)
  },
  divider: {
    margin: theme.spacing(3, 0)
  }
}));

// ----------------------------------------------------------------------

function RegisterView(props) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const isMountedRef = useIsMountedRef();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [icity, setICity] = useState({});
  const [idistrict, setIDistrict] = useState({});
  const [iward, setIWard] = useState({});
  const [gender, setGender] = useState('Nữ');
  const history = useHistory();
  const [city, setCity] = useState({});
  const [district, setDistrict] = useState({});
  const [ward, setWard] = useState({});

  const phoneRegex = RegExp(
    /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/
  );

  const passwordRegex = RegExp(
    /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])/
  );

  const RegisterSchema = Yup.object().shape({
    fullName: Yup.string()
      .min(2, 'Quá ngắn!')
      .required('Họ và tên bắt buộc.'),
    phone: Yup.string()
      .matches(phoneRegex, 'Số điện thoại không hợp lệ')
      .required('Số điện thoại là bắt buộc'),
    city: Yup.string(),
    district: Yup.string(),
    ward: Yup.string(),
    street: Yup.string(),
    email: Yup.string()
      .email('Email không hợp lệ')
      .required('Email là bắt buộc'),
    password: Yup.string()
      .required('Mật khẩu là bắt buộc')
      .matches(
        passwordRegex,
        'Mật khẩu phải có ít nhất 1 kí tự đặc biệt, một số và một chữ viết hoa, một chữ thường'
      )
      .min(8, 'Mật khẩu ít nhất 8 kí tự'),
    cnfPassword: Yup.string().oneOf(
      [Yup.ref('password'), null],
      'Mật khẩu không khớp'
    ),
    agencyName: Yup.string().required('Tên cửa hàng là bắt buộc'),
    // identityNumber: Yup.string().required('Số CMND hoặc CCCD là bắt buộc'),
    // dateOfIssue: Yup.string().required('Ngày cấp  bắt buộc'),
    // placeOfIssue: Yup.string().required('Nơi cấp là bắt buộc'),
    gender: Yup.string()
  });

  const formik = useFormik({
    initialValues: {
      fullName: '',
      email: '',
      phone: '',
      city: '',
      district: '',
      ward: '',
      gender: '',
      street: '',
      cnfPassword: '',
      dateOfBirth: '',
      password: '',
      agencyName: '',
      shopCity: '',
      shopPhone: '',
      shopDistrict: '',
      shopWard: '',
      fanPage: '',
      shopStreet: '',
      about: ''
    },
    validationSchema: RegisterSchema,
    onSubmit: async (values, { setErrors, setSubmitting }) => {
      const data = {
        email: values.email,
        password: values.password,
        confirmPassword: values.cnfPassword,
        ownerProfile: {
          name: values.fullName,
          address: {
            streetAndNumber: values.street,
            wardName: iward.wardName,
            wardCode: iward.wardCode,
            wardPrefix: iward.wardPrefix,
            districtName: idistrict.districtName,
            districtCode: idistrict.districtCode,
            districtPrefix: idistrict.districtPrefix,
            cityName: icity.cityName,
            cityCode: icity.cityCode,
            cityPrefix: icity.cityPrefix
          },
          gender: gender,
          phoneNumber: values.phone,
          dateOfBirthday: new Date(values.dateOfBirth)
        },
        address: {
          streetAndNumber: values.shopStreet,
          wardName: ward.wardName,
          wardCode: ward.wardCode,
          wardPrefix: ward.wardPrefix,
          districtName: district.districtName,
          districtCode: district.districtCode,
          districtPrefix: district.districtPrefix,
          cityName: city.cityName,
          cityCode: city.cityCode,
          cityPrefix: city.cityPrefix
        },
        agencyName: values.agencyName,
        avatar: null,
        phoneNumber: values.shopPhone,
        facebook: values.fanPage,
        productTotal: 0,
        about: values.about
      };
      try {
        await authService.register(data).then(() => {
          dispatch(
            login({
              email: values.email,
              password: values.password
            })
          );
          history.push('/app/dashboard');
        });

        enqueueSnackbar('Đăng ký thành công', {
          variant: 'success',
          action: key => (
            <MIconButton size="small" onClick={() => closeSnackbar(key)}>
              <Icon icon={closeFill} />
            </MIconButton>
          )
        });
        if (isMountedRef.current) {
          setSubmitting(false);
        }
      } catch (err) {
        if (isMountedRef.current) {
          setErrors({ afterSubmit: err.code });
          setSubmitting(false);
        }
      }
    }
  });
  const hidden = useMediaQuery(theme => theme.breakpoints.up('xl'));

  return (
    <Page title="Rent Mart | Đăng ký" className={classes.root}>
      <header className={classes.header}>
        <RouterLink to="/">
          <Logo />
        </RouterLink>
        {hidden && (
          <Box sx={{ mt: { md: -2 }, typography: 'body2' }}>
            Bạn đã có tài khoản? &nbsp;
            <Link
              underline="none"
              variant="subtitle2"
              component={RouterLink}
              to={PATH_PAGE.auth.login}
            >
              Đăng nhập
            </Link>
          </Box>
        )}
      </header>

      {hidden && <Section />}

      <Container>
        <div className={classes.content}>
          <Typography
            variant="h4"
            gutterBottom
            style={{ fontSize: '2rem', color: 'green' }}
          >
            Đăng kí cho thuê cùng Rent Mart
          </Typography>
          <Typography color="textSecondary">
            {/* Free forever. No credit card needed. */}
          </Typography>
          <Box sx={{ mb: 5 }} />

          {/* <SocialRegister firebase={firebase} /> */}

          {/* <Divider className={classes.divider}>
            <Typography variant="body2" color="textSecondary">
              OR
            </Typography>
          </Divider> */}

          <RegisterForm
            formik={formik}
            city={city}
            district={district}
            ward={ward}
            setCity={setCity}
            setDistrict={setDistrict}
            setWard={setWard}
            icity={icity}
            idistrict={idistrict}
            iward={iward}
            setICity={setICity}
            setIDistrict={setIDistrict}
            setIWard={setIWard}
            gender={gender}
            setGender={setGender}
          />

          <Box sx={{ mt: 3 }}>
            <Typography variant="body2" align="center" color="textSecondary">
              Đăng ký cửa hàng, Tôi đồng ý với các điều khoản của Rentmart
              về&nbsp;
              <Link color="textPrimary" underline="always">
                Chính sách dịch vụ
              </Link>
              &nbsp;và&nbsp;
              <Link color="textPrimary" underline="always">
                Chính sách bảo mật
              </Link>
              .
            </Typography>
          </Box>

          {!hidden && (
            <Box sx={{ mt: 3, textAlign: 'center' }}>
              Bạn đã có tài khoản? &nbsp;
              <Link
                variant="subtitle2"
                to={PATH_PAGE.auth.login}
                component={RouterLink}
              >
                Đăng nhập
              </Link>
            </Box>
          )}
        </div>
      </Container>
    </Page>
  );
}

export default RegisterView;
