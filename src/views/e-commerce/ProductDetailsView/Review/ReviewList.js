import clsx from 'clsx';
import PropTypes from 'prop-types';
import { findIndex } from 'lodash';
import React, { useEffect, useState } from 'react';
import { fDate } from '../../../../utils/formatTime';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  List,
  Rating,
  Avatar,
  ListItem,
  Typography
} from '@material-ui/core';
import { getProfile } from '../../../../redux/slices/user';
import Lightbox from '../../../../components/ModalLightbox';
// ----------------------------------------------------------------------
const THUMB_SIZE = 64;

const useStyles = makeStyles(theme => ({
  root: { padding: theme.spacing(3, 3, 5) },
  item: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    padding: theme.spacing(0),
    marginBottom: theme.spacing(5),
    [theme.breakpoints.up('sm')]: { flexDirection: 'row' }
  },
  avatar: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      marginRight: 0,
      marginBottom: theme.spacing(2)
    },
    [theme.breakpoints.up('md')]: {
      width: theme.spacing(8),
      height: theme.spacing(8)
    }
  },
  info: {
    display: 'flex',
    alignItems: 'center',
    marginRight: theme.spacing(2),
    marginBottom: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      minWidth: 160,
      textAlign: 'center',
      flexDirection: 'column',
      marginBottom: theme.spacing(0)
    },
    [theme.breakpoints.up('md')]: { minWidth: 240 }
  },
  thumbItem: {
    width: THUMB_SIZE,
    height: THUMB_SIZE,
    float: 'right',
    cursor: 'pointer',
    overflow: 'hidden',
    position: 'relative',
    margin: theme.spacing(0, 1),
    borderRadius: theme.shape.borderRadiusSm
  }
}));

// ----------------------------------------------------------------------

function ReviewItem({ review }) {
  const classes = useStyles();
  const { rating, content, timeCreated, name, images } = review;
  const dispatch = useDispatch();
  const user = useSelector(state => state.user.myProfile);
  const [openLightbox, setOpenLightbox] = useState(false);
  const [selectedImage, setSelectedImage] = useState(null);
  const imagesLightbox = images.map(img => img);
  useEffect(() => {
    dispatch(getProfile(review.customerId));
  }, [dispatch, review.customerId]);

  const handleOpenLightbox = url => {
    const selectedImage = findIndex(imagesLightbox, index => {
      return index === url;
    });
    setOpenLightbox(true);
    setSelectedImage(selectedImage);
  };

  return (
    <>
      <ListItem disableGutters className={clsx(classes.item)}>
        <div className={classes.info}>
          <Avatar src={user?.avatar} className={classes.avatar} />
          <div>
            <Typography variant="subtitle2" noWrap>
              {name}
            </Typography>
            <Typography variant="caption" color="textSecondary" noWrap>
              {fDate(timeCreated)}
            </Typography>
          </div>
        </div>

        <div className={classes.content}>
          <Rating size="small" value={rating} precision={0.1} readOnly />
          <Typography variant="body2">{content}</Typography>
          {images.map(item => (
            <div className={classes.thumbItem}>
              <Box
                component="img"
                alt={item}
                src="/static/images/placeholder.svg"
                data-src={item}
                onClick={() => handleOpenLightbox(item)}
                className="lazyload blur-up"
                sx={{ width: '100%', height: '100%', objectFit: 'cover' }}
              />
            </div>
          ))}
        </div>
      </ListItem>
      <Lightbox
        images={imagesLightbox}
        photoIndex={selectedImage}
        setPhotoIndex={setSelectedImage}
        isOpen={openLightbox}
        onClose={() => setOpenLightbox(false)}
      />
    </>
  );
}

ReviewList.propTypes = {
  product: PropTypes.object,
  className: PropTypes.string
};

function ReviewList({ reviews, className }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.root, className)}>
      <List disablePadding>
        {reviews.map(review => (
          <ReviewItem key={review._id} review={review} />
        ))}
      </List>
    </div>
  );
}

export default ReviewList;
