import clsx from 'clsx';
import PropTypes from 'prop-types';
import ReviewList from './ReviewList';
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Divider } from '@material-ui/core';

// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {}
}));

// ----------------------------------------------------------------------

Review.propTypes = {
  product: PropTypes.object,
  className: PropTypes.string
};

function Review({ reviews, className }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.root, className)}>
      {/* <Overview product={product} onOpen={handleOpenReviewBox} /> */}
      <Divider />

      {/* <Collapse in={reviewBox}>
        <ReviewForm onClose={handleCloseReviewBox} id="move_add_review" />
        <Divider />
      </Collapse> */}

      <ReviewList reviews={reviews} />
    </div>
  );
}

export default Review;
