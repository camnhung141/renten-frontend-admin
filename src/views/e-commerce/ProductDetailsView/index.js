import { Icon } from '@iconify/react';
import Page from '../../../components/Page';
import Description from './Description';
import { PATH_APP } from '../../../routes/paths';
import { useParams, useHistory } from 'react-router-dom';
import React, { useEffect, useState } from 'react';
import clockFill from '@iconify-icons/eva/clock-fill';
import { useDispatch, useSelector } from 'react-redux';
import roundVerified from '@iconify-icons/ic/round-verified';
import HeaderDashboard from '../../../components/HeaderDashboard';
import {
  getProduct,
  getReviews,
  updateProduct
} from '../../../redux/slices/product';
import Review from './Review';
import roundVerifiedUser from '@iconify-icons/ic/round-verified-user';
import * as Yup from 'yup';
import { useFormik } from 'formik';
import fakeRequest from '../../../utils/fakeRequest';
import ProductTemplate from './NewPostForm';
import { useSnackbar } from 'notistack';
import { makeStyles } from '@material-ui/core/styles';
import { alpha } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import {
  Box,
  Tab,
  Card,
  Grid,
  Divider,
  Skeleton,
  Container,
  Typography,
  LinearProgress
} from '@material-ui/core';
import { TabContext, TabList, TabPanel } from '@material-ui/lab';
import productService from '../../../apis/product.api';
// ----------------------------------------------------------------------

const PRODUCT_DESCRIPTION = [
  {
    title: '100% Original',
    description: 'Chocolate bar candy canes ice cream toffee cookie halvah.',
    icon: roundVerified
  },
  {
    title: '10 Day Replacement',
    description: 'Marshmallow biscuit donut dragée fruitcake wafer.',
    icon: clockFill
  },
  {
    title: 'Year Warranty',
    description: 'Cotton candy gingerbread cake I love sugar sweet.',
    icon: roundVerifiedUser
  }
];

const useStyles = makeStyles(theme => ({
  root: {},
  icon: {
    margin: 'auto',
    display: 'flex',
    borderRadius: '50%',
    alignItems: 'center',
    width: theme.spacing(8),
    justifyContent: 'center',
    height: theme.spacing(8),
    marginBottom: theme.spacing(3),
    color: theme.palette.primary.main,
    backgroundColor: `${alpha(theme.palette.primary.main, 0.08)}`
  },
  tabList: {
    padding: theme.spacing(0, 3),
    backgroundColor: theme.palette.background.neutral
  },
  button: {
    float: 'right'
  },
  linear: {
    zIndex: '100'
  },
  noReview: {
    fontSize: '2rem',
    textAlign: 'center',
    width: '100%',
    minHeight: '10rem',
    color: '#d8d4d4'
  },
  tab: { whiteSpace: 'nowrap' }
}));

// ----------------------------------------------------------------------

const SkeletonLoad = (
  <Grid container spacing={3}>
    <Grid item xs={12} md={6} lg={7}>
      <Skeleton
        variant="rectangular"
        component={Box}
        sx={{ width: '100%', paddingTop: '100%', borderRadius: 2 }}
      />
    </Grid>
    <Grid item xs={12} md={6} lg={5}>
      <Skeleton variant="circular" width={80} height={80} />
      <Skeleton variant="text" height={240} />
      <Skeleton variant="text" height={40} />
      <Skeleton variant="text" height={40} />
      <Skeleton variant="text" height={40} />
    </Grid>
  </Grid>
);

function ProductDetailsView() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const [value, setValue] = useState('1');
  const { name } = useParams();
  const [category, setCategory] = useState();
  const [subCategory, setSubCategory] = useState();
  const { product, error, reviews, isLoading } = useSelector(
    state => state.product
  );
  const { enqueueSnackbar } = useSnackbar();
  const [files, setFiles] = useState([]);
  const profile = useSelector(state => state.auth.myProfile);
  const [options, setOptions] = useState([]);
  const [newOptions, setNewOptions] = useState([]);
  const [unitRent, setUnitRent] = useState();

  useEffect(() => {
    dispatch(getProduct(name));
    dispatch(getReviews(name));
    setOptions(product ? product.listProducts : []);
    setUnitRent(product?.rentDetail?.unitRent);
    setSubCategory(product?.subCategoriesId[0]);
    setCategory(product?.categoriesId[0]);
  }, [dispatch, name]);

  useEffect(() => {
    setOptions(product?.listProducts);
    setUnitRent(product?.rentDetail?.unitRent);
    setSubCategory(product?.subCategoriesId[0]);
    setCategory(product?.categoriesId[0]);
  }, [product]);

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleDeleteProduct = async () => {
    await productService.deleteProduct(product._id).then(() => {
      history.push('/app/management/e-commerce/list');
    });
    enqueueSnackbar('Xóa thành công', { variant: 'success' });
  };

  const NewProductSchema = Yup.object().shape({
    name: Yup.string().required('Tên sản phẩm là bắt buộc'),
    description: Yup.string().required('Nhập mô tả sản phẩm'),
    brand: Yup.string(),
    pointAccept: Yup.number()
      .min(0, 'Điểm phải lớn hơn 0')
      .max(5, 'Điểm phải nhỏ hơn hoặc bằng 5')
  });
  const formik = useFormik({
    initialValues: {
      name: product?.name,
      tags: product?.tags[0],
      brand: product?.brand,
      categoriesId: product?.categories[0],
      subCategoriesId: product?.subCategories[0],
      large: product?.size?.large,
      width: product?.size?.width,
      height: product?.size?.height,
      description: product?.description,
      weight: product?.weight,
      requiredPaperWorks: product?.rentDetail?.requiredPaperWorks[0],
      deposit: product?.rentDetail.deposit,
      pointAccept: product?.rentDetail.requiredPoint
    },
    validationSchema: NewProductSchema,
    enableReinitialize: true,
    onSubmit: async (values, { setSubmitting, resetForm, setErrors }) => {
      // const thumbnailProduct = await imageService.upload(files);
      const data = {
        name: values.name,
        thumbnail: product.thumbnail,
        brand: values.brand,
        rentDetail: {
          unitRent: unitRent,
          requiredPaperWorks: [values.requiredPaperWorks],
          requiredPoint: values.pointAccept,
          deposit: values.deposit
        },
        weight: values.weight,
        originalPrice:
          options.length !== 0
            ? parseFloat(options[0].originalPrice)
            : newOptions.length !== 0
            ? parseFloat(newOptions[0].originalPrice)
            : 0,
        size: {
          large: values.large,
          width: values.width,
          height: values.height
        },
        rentPrice:
          options.length !== 0
            ? parseFloat(options[0].rentPrice)
            : newOptions.length !== 0
            ? parseFloat(newOptions[0].rentPrice)
            : 0,
        agencyId: profile.accountId?._id,
        categoriesId: [category],
        subCategoriesId: [subCategory],
        tags: [values.tags],
        description: values.description
      };
      try {
        await fakeRequest(500);
        dispatch(updateProduct(data, product._id, options, newOptions));
        resetForm();
        setSubmitting(false);
        if (!isLoading) {
          enqueueSnackbar('Cập nhật thành công', { variant: 'success' });
        }
      } catch (error) {
        enqueueSnackbar('Không thể cập nhật sản phẩm', { variant: 'error' });
        setSubmitting(false);
        setErrors({ afterSubmit: error.code });
      }
    }
  });

  if (isLoading) {
    return <LinearProgress />;
  }
  return (
    <Page title="Management | Product Details" className={classes.root}>
      <Container>
        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={handleDeleteProduct}
          startIcon={<DeleteIcon />}
        >
          Xóa sản phẩm này
        </Button>
        <HeaderDashboard
          heading="Chi tiết sản phẩm"
          links={[
            { name: 'Bảng điều khiển', href: PATH_APP.root },
            { name: 'Quản lý', href: PATH_APP.management.root },
            { name: 'Sản phẩm', href: PATH_APP.management.eCommerce.root },
            { name: `${product?.name}` }
          ]}
        />

        {/* <CartWidget length={totalItems} /> */}

        {product && (
          <>
            <Card>
              <Grid container>
                {/* <Grid item xs={12} md={6} lg={5}>
                  <Slide product={product} />
                </Grid> */}
                <Grid item xs={12} md={12}>
                  {/* <Sumary
                    product={product}
                    cart={checkout.cart}
                    onAddCart={handleAddCart}
                    onGotoStep={handleGotoStep}
                  /> */}
                  <ProductTemplate
                    formik={formik}
                    category={category}
                    subCategory={subCategory}
                    setSubCategory={setSubCategory}
                    product={product}
                    files={files}
                    setFiles={setFiles}
                    setCate={setCategory}
                    options={options}
                    setOptions={setOptions}
                    newOptions={newOptions}
                    setNewOptions={setNewOptions}
                    unitRent={unitRent}
                    change={product?.categories[0]._id}
                    changeSub={product?.subCategories[0]._id}
                    setUnitRent={setUnitRent}
                    isLoading={isLoading}
                  />
                </Grid>
              </Grid>
            </Card>

            <Box sx={{ my: 8 }}>
              <Grid container>
                {PRODUCT_DESCRIPTION.map(item => (
                  <Grid item xs={12} md={4} key={item.title}>
                    <Box
                      sx={{
                        my: 2,
                        mx: 'auto',
                        maxWidth: 280,
                        textAlign: 'center'
                      }}
                    >
                      <div className={classes.icon}>
                        <Icon icon={item.icon} width={36} height={36} />
                      </div>
                      <Typography variant="subtitle1" gutterBottom>
                        {item.title}
                      </Typography>
                      <Typography color="textSecondary">
                        {item.description}
                      </Typography>
                    </Box>
                  </Grid>
                ))}
              </Grid>
            </Box>

            <Card>
              <TabContext value={value}>
                <TabList onChange={handleChangeTab} className={classes.tabList}>
                  <Tab
                    className={classes.tabItem}
                    disableRipple
                    value="1"
                    label="Lịch sử sản phẩm"
                  />
                  <Tab
                    disableRipple
                    value="2"
                    label={`Đánh giá (${reviews?.length})`}
                    classes={{ wrapper: classes.tab }}
                  />
                </TabList>
                <Divider />

                <TabPanel value="1" className={classes.tabPanel}>
                  <Description products={options} />
                </TabPanel>
                <TabPanel value="2" className={classes.tabPanel}>
                  {reviews?.length > 0 && <Review reviews={reviews} />}
                  {reviews?.length === 0 && (
                    <div className={classes.noReview}>
                      <p style={{ marginTop: '3rem' }}>
                        Không có đánh giá nào cho sản phẩm này
                      </p>
                    </div>
                  )}
                </TabPanel>
              </TabContext>
            </Card>
          </>
        )}

        {!product && SkeletonLoad}

        {error && <Typography variant="h6">404 Product not found</Typography>}
      </Container>
    </Page>
  );
}

export default ProductDetailsView;
