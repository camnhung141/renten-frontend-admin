import clsx from 'clsx';
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Form, FormikProvider } from 'formik';
import Slide from './Carousel';
import { QuillEditor } from '../../../components/Editor';
import { makeStyles } from '@material-ui/core/styles';
import { UploadMultiFile } from '../../../components/Upload';
import NumberFormat from 'react-number-format';
import {
  Box,
  Button,
  Grid,
  TextField,
  Select,
  Typography,
  FormHelperText,
  CircularProgress,
  FormControl
} from '@material-ui/core';
import ShowOption from './ShowOption';
import categoryService from '../../../apis/category.api';
// ----------------------------------------------------------------------
const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(5)
  },
  margin: {
    marginTop: theme.spacing(3)
  },
  helperText: {
    padding: theme.spacing(0, 2)
  },
  addBtn: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  card: {
    margin: '1rem',
    cursor: 'pointer'
  },
  divider: {
    borderBottom: 'solid 1px #e4e1e1',
    marginTop: '1rem',
    marginBottom: '2rem'
  },
  loading: {
    color: '#fff'
  },
  close: {
    color: 'gray',
    marginLeft: 'auto',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer'
  }
}));

// ----------------------------------------------------------------------

PostDetailsView.propTypes = {
  formik: PropTypes.object.isRequired,
  onOpenPreview: PropTypes.func,
  className: PropTypes.string
};

function NumberFormatCustom(props) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={values => {
        onChange({
          target: {
            name: props.name,
            value: values.value
          }
        });
      }}
      thousandSeparator
      isNumericString
      prefix="₫"
    />
  );
}

NumberFormatCustom.propTypes = {
  inputRef: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
};
function PostDetailsView({
  formik,
  onOpenPreview,
  category,
  setCate,
  className,
  product,
  files,
  setFiles,
  subCategory,
  setSubCategory,
  options,
  setOptions,
  newOptions,
  setNewOptions,
  unitRent,
  setUnitRent,
  change,
  changeSub,
  isLoading,
  ...other
}) {
  const classes = useStyles();
  const {
    errors,
    values,
    touched,
    handleSubmit,
    isSubmitting,
    setFieldValue,
    getFieldProps
  } = formik;

  const [categories, setCategories] = useState([]);
  const [subCategories, setSubCategories] = useState([]);
  const [code, setCode] = useState();
  const [appearance, setAppearance] = useState('');
  const [price, setPrice] = useState();
  const [originalPrice, setOriginalPrice] = useState();
  const [images, setImages] = useState([]);
  const [iimages, setIImages] = useState([]);
  const [error, setError] = useState('');
  const [titleBtn, setTitleBtn] = useState('Cập nhật');

  const handleAddOption = () => {
    const check1 = options?.filter(option => option.productCode === code);
    const check2 = newOptions?.filter(option => option.productCode === code);
    if (check1.length > 0) {
      const iOptions = [...options];
      for (let i = 0; i < iOptions.length; i++) {
        if (iOptions[i].productCode === code) {
          iOptions[i] = {
            ...iOptions[i],
            productCode: code,
            appearance: appearance,
            rentPrice: parseFloat(price),
            originalPrice: parseFloat(originalPrice),
            images
          };
          setOptions(iOptions);
          return;
        }
      }
      return;
    }
    if (check2.length > 0) {
      setError(' *Mã phảm phẩm đã được thêm mới');
      return;
    }
    if (!code) {
      setError(' *Mã phảm phẩm là bắt buộc');
      return;
    }
    const iOption = [
      ...newOptions,
      {
        productCode: code,
        appearance: appearance,
        rentPrice: parseFloat(price),
        originalPrice: parseFloat(originalPrice),
        images
      }
    ];
    setNewOptions(iOption);
  };

  const handleClear = () => {
    setCode('');
    setAppearance('');
    setPrice('');
    setOriginalPrice('');
  };

  const handleClick = option => {
    setCode(option.productCode);
    setAppearance(option.appearance);
    setPrice(option.rentPrice);
    setOriginalPrice(option.originalPrice);
    setImages(option.images);
    setIImages(option.images);
  };

  useEffect(() => {
    const getCategories = async () => {
      await categoryService.list().then(response => {
        const result = response?.filter(iCategory => iCategory._id === change);
        setCategories(response);
        setSubCategories(result[0]?.subCategories);
      });
    };
    getCategories();
  }, [change]);

  useEffect(() => {
    setCode(options ? options[0]?.productCode : '');
    setAppearance(options ? options[0]?.appearance : '');
    setPrice(options ? ` ${options[0]?.rentPrice}` : '');
    setOriginalPrice(options ? ` ${options[0]?.originalPrice}` : '');
    setImages(options && options[0] ? options[0]?.images : []);
    setIImages(options && options[0] ? options[0]?.images : []);
  }, []);

  useEffect(() => {
    const check = options?.filter(e => e.productCode === code);
    if (check && check.length === 0) {
      setTitleBtn('Thêm mới');
    } else {
      setTitleBtn('Cập nhật');
    }
  }, [code]);

  useEffect(() => {
    const result = categories?.filter(iCategory => iCategory._id === category);
    setSubCategories(result[0]?.subCategories);
  }, [category, categories]);

  return (
    <FormikProvider value={formik}>
      <Form
        noValidate
        autoComplete="off"
        onSubmit={handleSubmit}
        className={clsx(classes.root, className)}
        {...other}
      >
        <Typography variant="subtitle2" color="textSecondary" gutterBottom>
          Ảnh sản phẩm
        </Typography>

        <Grid container spacing={3}>
          <Grid item xs={12} md={6}>
            <img
              src={product.thumbnail}
              alt={product.name}
              style={{ margin: '0 auto' }}
              width="300"
              height="300"
            />
          </Grid>

          <Grid item xs={12} md={6}>
            <Typography
              variant="subtitle2"
              color="textSecondary"
              gutterBottom
              className={classes.margin}
            >
              Tên sản phẩm
            </Typography>
            <TextField
              fullWidth
              required
              {...getFieldProps('name')}
              error={Boolean(touched.name && errors.name)}
              helperText={touched.name && errors.name}
            />
            <Typography
              variant="subtitle2"
              color="textSecondary"
              gutterBottom
              className={classes.margin}
            >
              Tags
            </Typography>
            <TextField
              fullWidth
              required
              {...getFieldProps('tags')}
              error={Boolean(touched.tags && errors.tags)}
              helperText={touched.tags && errors.tags}
            />
            <Typography
              variant="subtitle2"
              color="textSecondary"
              gutterBottom
              className={classes.margin}
            >
              Thương hiệu/Tác giả
            </Typography>
            <TextField
              fullWidth
              {...getFieldProps('brand')}
              error={Boolean(touched.brand && errors.brand)}
              helperText={touched.brand && errors.brand}
            />
            <Typography
              variant="subtitle2"
              color="textSecondary"
              gutterBottom
              className={classes.margin}
            >
              Điểm cho phép
            </Typography>
            <TextField
              fullWidth
              placeholder="Điểm tối thiểu của khách hàng để được phép mua sản phẩm"
              {...getFieldProps('pointAccept')}
              error={Boolean(touched.pointAccept && errors.pointAccept)}
              helperText={touched.pointAccept && errors.pointAccept}
            />
          </Grid>
        </Grid>
        <Typography
          variant="subtitle2"
          color="textSecondary"
          gutterBottom
          className={classes.margin}
        >
          Giấy tờ
        </Typography>
        <TextField
          fullWidth
          type="text"
          required
          {...getFieldProps('requiredPaperWorks')}
          error={Boolean(
            touched.requiredPaperWorks && errors.requiredPaperWorks
          )}
          helperText={touched.requiredPaperWorks && errors.requiredPaperWorks}
        />
        <Typography
          variant="subtitle2"
          color="textSecondary"
          gutterBottom
          className={classes.margin}
        >
          Đền bù
        </Typography>
        <TextField
          fullWidth
          type="number"
          required
          {...getFieldProps('deposit')}
          error={Boolean(touched.deposit && errors.deposit)}
          helperText={touched.deposit && errors.deposit}
        />
        <FormControl
          variant="outlined"
          className={classes.margin}
          fullWidth
          required
        >
          <Typography variant="subtitle2" color="textSecondary" gutterBottom>
            Đơn vị
          </Typography>
          <Select
            native
            value={unitRent}
            onChange={e => setUnitRent(e.target.value)}
            inputProps={{
              name: 'unitRent',
              id: 'filled-age-native-simple'
            }}
          >
            <option value={unitRent}>
              {unitRent === 'hour' ? 'X Đồng/Giờ' : 'X Đồng/Ngày'}
            </option>
            <option value={unitRent === 'hour' ? 'date' : 'hour'}>
              {unitRent === 'hour' ? 'X Đồng/Ngày' : 'X Đồng/Giờ'}
            </option>
          </Select>
          {/* <InputLabel id="category">Đơn vị thuê</InputLabel>
          <Select
            labelId="unitRent"
            id="unitRent"
            value={unitRent}
            onChange={e => setUnitRent(e.target.value)}
            label="Đơn vị thuê"
            fullWidth
            required
          >
            <MenuItem value="date">X Đồng/Ngày</MenuItem>
            <MenuItem value="hour">X Đồng/Giờ</MenuItem>
          </Select> */}
        </FormControl>

        <Grid container spacing={3}>
          <Grid item xs={6}>
            <FormControl
              variant="outlined"
              key=""
              className={classes.margin}
              fullWidth
              required
            >
              <Typography
                variant="subtitle2"
                color="textSecondary"
                gutterBottom
              >
                Danh mục
              </Typography>
              <Select
                native
                key=""
                value={category}
                onChange={e => {
                  setCate(e.target.value);
                }}
                inputProps={{
                  name: 'unitRent',
                  id: 'filled-age-native-simple'
                }}
              >
                {categories.map(element => {
                  if (element?._id === change) {
                    return (
                      <option value={element?._id}>{element?.name}</option>
                    );
                  }
                  return <></>;
                })}
                {categories.map(element => {
                  if (element?._id !== change) {
                    return (
                      <option value={element?._id}>{element?.name}</option>
                    );
                  }
                  return <></>;
                })}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={6}>
            <FormControl
              variant="outlined"
              className={classes.margin}
              fullWidth
              required
            >
              <Typography
                variant="subtitle2"
                color="textSecondary"
                gutterBottom
              >
                Danh mục con
              </Typography>
              <Select
                native
                value={subCategory}
                onChange={e => setSubCategory(e.target.value)}
                inputProps={{
                  name: 'unitRent',
                  id: 'filled-age-native-simple'
                }}
              >
                {subCategories?.map(element => {
                  if (element?._id === changeSub) {
                    return (
                      <option value={element?._id}>{element?.name}</option>
                    );
                  }
                  return <></>;
                })}
                {subCategories?.map(element => {
                  if (element?._id !== changeSub) {
                    return (
                      <option value={element?._id}>{element?.name}</option>
                    );
                  }
                  return <></>;
                })}
              </Select>
            </FormControl>
          </Grid>
        </Grid>

        <Grid container spacing={3}>
          <Grid item xs={12} md={6}>
            <Typography
              variant="subtitle2"
              color="textSecondary"
              gutterBottom
              className={classes.margin}
            >
              Độ lớn(cm)
            </Typography>
            <TextField
              fullWidth
              type="number"
              required
              {...getFieldProps('large')}
              error={Boolean(touched.large && errors.large)}
              helperText={touched.large && errors.large}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <Typography
              variant="subtitle2"
              color="textSecondary"
              gutterBottom
              className={classes.margin}
            >
              Chiều rộng(cm)
            </Typography>
            <TextField
              fullWidth
              type="number"
              required
              {...getFieldProps('width')}
              error={Boolean(touched.width && errors.width)}
              helperText={touched.width && errors.width}
            />
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          <Grid item xs={12} md={6}>
            <Typography
              variant="subtitle2"
              color="textSecondary"
              gutterBottom
              className={classes.margin}
            >
              Chiều cao(cm)
            </Typography>
            <TextField
              fullWidth
              type="number"
              required
              {...getFieldProps('height')}
              error={Boolean(touched.height && errors.height)}
              helperText={touched.height && errors.height}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <Typography
              variant="subtitle2"
              color="textSecondary"
              gutterBottom
              className={classes.margin}
            >
              Trọng lượng(g)
            </Typography>
            <TextField
              fullWidth
              type="number"
              required
              {...getFieldProps('weight')}
              error={Boolean(touched.weight && errors.weight)}
              helperText={touched.weight && errors.weight}
            />
          </Grid>
        </Grid>
        {/* <div className={classes.margin}>
          <Typography variant="subtitle2" color="textSecondary" gutterBottom>
            Ảnh đại diện cho sản phẩm
          </Typography>

          <UploadMultiFile
            value={files}
            onChange={setFiles}
            caption="Tải ảnh lên"
          />

          <FormHelperText error className={classes.helperText}>
            {touched.cover && errors.cover}
          </FormHelperText>
        </div> */}

        <div className={classes.margin}>
          <Typography variant="subtitle2" color="textSecondary" gutterBottom>
            Mô tả sản phẩm
          </Typography>
          <QuillEditor
            id="post-content"
            value={values.description}
            onChange={val => setFieldValue('description', val)}
            error={Boolean(touched.description && errors.description)}
          />
          <FormHelperText error className={classes.helperText}>
            {touched.description && errors.description}
          </FormHelperText>
        </div>
        <Typography
          variant="subtitle2"
          color="textSecondary"
          gutterBottom
          className={classes.margin}
        >
          Chi tiết từng sản phẩm
        </Typography>
        <Box className={classes.divider}> </Box>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <Typography variant="subtitle2" color="textSecondary" gutterBottom>
              Mã sản phẩm
            </Typography>
            <TextField
              fullWidth
              type="text"
              value={code}
              onChange={e => setCode(e.target.value)}
            />
            <p style={{ fontSize: '0.8rem', color: 'red' }}>{error}</p>
            <Typography
              variant="subtitle2"
              color="textSecondary"
              gutterBottom
              className={classes.margin}
            >
              Tình trạng sản phẩm
            </Typography>
            <TextField
              fullWidth
              type="text"
              placeholder="Tốt/Bị trầy xước, bị rách nhỏ..."
              value={appearance}
              onChange={e => setAppearance(e.target.value)}
            />
            <Typography
              variant="subtitle2"
              color="textSecondary"
              gutterBottom
              className={classes.margin}
            >
              Giá gốc sản phẩm (VNĐ)
            </Typography>
            <TextField
              fullWidth
              key="price"
              defaultValue=""
              value={`${originalPrice}`}
              onChange={e => setOriginalPrice(e.target.value)}
              error={Boolean(touched.price && errors.price)}
              helperText={touched.price && errors.price}
              name="price"
              id="formatted-numberformat-input"
              InputProps={{
                inputComponent: NumberFormatCustom
              }}
            />
            <Typography
              variant="subtitle2"
              color="textSecondary"
              gutterBottom
              className={classes.margin}
            >
              Giá thuê sản phẩm (VNĐ)
            </Typography>
            <TextField
              fullWidth
              label=""
              value={price}
              onChange={e => setPrice(e.target.value)}
              error={Boolean(touched.price && errors.price)}
              helperText={touched.price && errors.price}
              name="price"
              id="formatted-numberformat-input"
              InputProps={{
                inputComponent: NumberFormatCustom
              }}
            />
            {iimages?.length > 0 && <Slide images={iimages} />}
            <div className={classes.margin}>
              <Typography
                variant="subtitle2"
                color="textSecondary"
                gutterBottom
              >
                Ảnh sản phẩm
              </Typography>
              <UploadMultiFile
                value={images}
                onChange={setImages}
                // value={values.cover}
                // onChange={val => setFieldValue('cover', val)}
                caption="Chọn ảnh sản phẩm tương ứng"
              />

              <FormHelperText error className={classes.helperText}>
                {touched.images && errors.images}
              </FormHelperText>
            </div>
            <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
              <Button
                type="button"
                color="inherit"
                variant="outlined"
                onClick={handleClear}
                sx={{ mr: 1.5 }}
              >
                Làm mới
              </Button>
              <Button variant="contained" onClick={handleAddOption}>
                {titleBtn}
              </Button>
            </Box>
          </Grid>
          {/* <Grid item xs={12} sm={1} className={classes.addBtn}>
            <LoadingButton variant="contained" onClick={handleAddOption}>
              Thêm
            </LoadingButton>
          </Grid> */}

          <Grid item xs={12} sm={6}>
            <div
              style={{
                border: '1px solid #dfdada',
                margin: '0 0.5rem',
                minHeight: '14rem'
              }}
            >
              {options?.map(element => (
                <ShowOption element={element} handleClick={handleClick} />
              ))}
              {newOptions?.map(element => (
                <ShowOption element={element} handleClick={handleClick} />
              ))}
            </div>
          </Grid>
        </Grid>

        <Box className={classes.divider}> </Box>
        <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
          {/* <Button
            type="button"
            color="inherit"
            variant="outlined"
            onClick={onOpenPreview}
            sx={{ mr: 1.5 }}
          >
            Xem trước
          </Button> */}
          <Button type="submit" variant="contained" pending={isSubmitting}>
            {isLoading && (
              <CircularProgress size={24} className={classes.loading} />
            )}
            {!isLoading && ' Lưu thay đổi'}
          </Button>
        </Box>
      </Form>
    </FormikProvider>
  );
}

export default PostDetailsView;
