import clsx from 'clsx';
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import {
  Typography,
  Grid,
  FormControl,
  MenuItem,
  TableRow,
  TableCell,
  TableHead,
  TableContainer,
  Box,
  Table,
  TableBody,
  Select
} from '@material-ui/core';
import productService from '../../../apis/product.api';
import Scrollbars from '../../../components/Scrollbars';
import moment from 'moment';

// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3),
    margin: theme.spacing(3),
    minHeight: '40rem'
  },
  table: {
    marginTop: theme.spacing(3)
  }
}));

// ----------------------------------------------------------------------

Description.propTypes = {
  product: PropTypes.object,
  className: PropTypes.string
};

function Description({ products, className }) {
  const classes = useStyles();
  const [code, setCode] = useState();
  const [information, setInformation] = useState([]);

  useEffect(() => {
    if (products?.length > 0) {
      setCode(products[0]._id);
    }
  }, [products]);

  useEffect(() => {
    const infor = async () => {
      await productService.getHistoryProduct(code).then(res => {
        setInformation(res.data.payload);
      });
    };
    infor();
  }, [code]);

  return (
    <div className={clsx(classes.root, className)}>
      <Grid container spacing={1}>
        <Grid xs={12} md={6}>
          <Typography
            variant="h4"
            color="textSecondary"
            gutterBottom
            className={classes.margin}
          >
            Thông tin lịch sử tình trạng sản phẩm:
          </Typography>
        </Grid>
        <Grid xs={12} md={6}>
          <FormControl
            fullWidth
            variant="outlined"
            className={classes.margin}
            required
          >
            <Select
              labelId="category"
              id="status"
              required
              value={code}
              onChange={e => setCode(e.target.value)}
              size="small"
              className={classes.margin}
            >
              {products?.map(e => (
                <MenuItem value={e._id}>{e.productCode}</MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
      </Grid>

      <Scrollbars>
        <TableContainer
          component={Box}
          sx={{ minWidth: 960 }}
          className={classes.table}
        >
          <Table>
            <TableHead className={classes.tableHead}>
              <TableRow>
                <TableCell width={40}>STT</TableCell>
                <TableCell align="center">Ghi chú</TableCell>
                <TableCell align="center">Ngày cập nhật</TableCell>
                <TableCell align="center">Tình trạng sản phẩm</TableCell>
                <TableCell align="left">Ảnh minh chứng</TableCell>
              </TableRow>
            </TableHead>

            <TableBody>
              {information?.map((e, index) => (
                <TableRow key={e?.product?._id} className={classes.row}>
                  <TableCell>{index + 1}</TableCell>
                  <TableCell align="center">{e.note}</TableCell>
                  <TableCell align="center">
                    {moment(e.updatedAt).format('DD/MM/YYYY HH:mm:ss')}
                  </TableCell>
                  <TableCell align="center">{e.appearance}</TableCell>
                  <TableCell align="center">
                    <Box
                      sx={{
                        mx: 2,
                        width: 100,
                        height: 100,
                        borderRadius: 1.5
                      }}
                    >
                      <img
                        component="img"
                        alt={e._id}
                        src="/static/images/placeholder.svg"
                        src={
                          e.images[0] ||
                          'https://lh3.googleusercontent.com/proxy/lwawa6p0OG6aZRzjI-FA5UQlGHNXjJzJPDtnG617UinaQGMnSrZtMJen3n9GC7gkf0Z9ySt3m5uPnqezZnb46D65_ZBI0AjKXYBVBJ1h4RUeSvDHvScRrWsuq7ZTPXi8bi83'
                        }
                        className="lazyload blur-up"
                      />
                    </Box>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Scrollbars>
    </div>
  );
}

export default Description;
