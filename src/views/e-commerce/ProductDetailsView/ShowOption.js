import React from 'react';
import { Typography, CardContent, Card } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
// import { UploadMultiFile } from '../../../components/Upload';
import Grid from '@material-ui/core/Grid';

// ----------------------------------------------------------------
const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(5)
  },
  margin: {
    marginTop: theme.spacing(3)
  },
  helperText: {
    padding: theme.spacing(0, 2)
  },
  addBtn: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  card: {
    margin: '1rem',
    cursor: 'pointer'
  },
  divider: {
    borderBottom: 'solid 1px #e4e1e1',
    marginTop: '1rem',
    marginBottom: '2rem'
  },
  close: {
    color: 'gray',
    marginLeft: 'auto',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer'
  }
}));
export default function ShowOption({ element, handleClick }) {
  const classes = useStyles();
  return (
    <Card className={classes.card} variant="outlined">
      <CardContent>
        <Grid container spacing={1} onClick={() => handleClick(element)}>
          <Grid xs={12} md={8} style={{ paddingRight: '0.5rem' }}>
            <Typography color="textSecondary" gutterBottom>
              Mã sản phẩm : {element.productCode}
            </Typography>
            <Typography variant="body2" component="p">
              {element.appearance}
            </Typography>
          </Grid>
          <Grid xs={12} md={3}>
            <Typography
              variant="body2"
              component="p"
              style={{ color: 'green' }}
            >
              Giá thuê: {element.rentPrice} đ
            </Typography>
            <Typography variant="body2" component="p" style={{ color: 'gray' }}>
              Giá gốc: {element.originalPrice} đ
            </Typography>
          </Grid>
          <Grid xs={12} md={1} className={classes.close}>
            {/* <Icon icon={closeFill} width={20} height={20} /> */}
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
}
