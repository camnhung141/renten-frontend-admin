import { filter } from 'lodash';
import HeadTable from './HeadTable';
import { Icon } from '@iconify/react';
import Page from '../../../components/Page';
import ToolbarTable from './ToolbarTable';
import { sentenceCase } from 'change-case';
import { PATH_APP } from '../../../routes/paths';
import { fCurrency } from '../../../utils/formatNumber';
import React, { useState, useEffect } from 'react';
import { visuallyHidden } from '@material-ui/utils';
import { getProducts } from '../../../redux/slices/product';
import { useDispatch, useSelector } from 'react-redux';
import SearchNotFound from '../../../components/SearchNotFound';
import HeaderDashboard from '../../../components/HeaderDashboard';
import Scrollbars from '../../../components/Scrollbars';
import moreVerticalFill from '@iconify-icons/eva/more-vertical-fill';
import { useTheme, makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Card,
  Table,
  TableRow,
  TableBody,
  TableCell,
  Container,
  IconButton,
  Typography,
  TableContainer,
  TablePagination
} from '@material-ui/core';
import LoadingScreen from '../../../components/LoadingScreen';
import { MLabel } from '../../../@material-extend';
import moment from 'moment';

// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'name', label: 'Sản phẩm', alignRight: 'left' },
  { id: 'createdAt', label: 'Ngày tạo', alignRight: 'center' },
  { id: 'rentCount', label: 'Đã thuê', alignRight: 'center' },
  { id: 'originalPrice', label: 'Giá cơ bản', alignRight: 'center' },
  { id: 'rentPrice', label: 'Giá thuê', alignRight: 'center' },
  { id: '' }
];
function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });

  if (query) {
    array = filter(array, _product => {
      return _product.name.toLowerCase().indexOf(query.toLowerCase()) !== -1;
    });
    return array;
  }
  return stabilizedThis.map(el => el[0]);
}

const useStyles = makeStyles(theme => ({
  root: {},
  nameProduct: {
    cursor: 'pointer',
    '&:hover': {
      color: 'rgb(7, 177, 77, 0.42)'
    }
  },
  sortSpan: visuallyHidden
}));

// ----------------------------------------------------------------------

function ProductListView(props) {
  const classes = useStyles();
  const theme = useTheme();
  const dispatch = useDispatch();
  const { products } = useSelector(state => state.product);
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState('asc');
  const [filterName, setFilterName] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [orderBy, setOrderBy] = useState('');
  const profile = useSelector(state => state.auth.myProfile);
  useEffect(() => {
    dispatch(
      getProducts(
        profile
          ? {
              agencyId: profile.accountId._id
            }
          : null
      )
    );
  }, [dispatch, profile]);

  if (!products) {
    return <LoadingScreen />;
  }
  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  // const handleClick = (event, name) => {
  //   const selectedIndex = selected.indexOf(name);
  //   let newSelected = [];
  //   if (selectedIndex === -1) {
  //     newSelected = newSelected.concat(selected, name);
  //   } else if (selectedIndex === 0) {
  //     newSelected = newSelected.concat(selected.slice(1));
  //   } else if (selectedIndex === selected.length - 1) {
  //     newSelected = newSelected.concat(selected.slice(0, -1));
  //   } else if (selectedIndex > 0) {
  //     newSelected = newSelected.concat(
  //       selected.slice(0, selectedIndex),
  //       selected.slice(selectedIndex + 1)
  //     );
  //   }
  //   setSelected(newSelected);
  // };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = event => {
    setFilterName(event.target.value);
  };

  const handleClickDetail = id => {
    props.history.push('/app/management/e-commerce/product/' + id);
  };
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - products.length) : 0;

  const filteredProducts = applySortFilter(
    products,
    getComparator(order, orderBy),
    filterName
  );

  const isProductNotFound = filteredProducts.length === 0;

  return (
    <Page title="Quản lý | Sản phẩm" className={classes.root}>
      <Container>
        <HeaderDashboard
          heading="Danh sách sản phẩm"
          links={[
            { name: 'Bảng điều khiển', href: PATH_APP.root },
            { name: 'Quản lý', href: PATH_APP.management.root },
            { name: 'Sản phẩm', href: PATH_APP.management.eCommerce.root },
            { name: 'Danh sách sản phẩm' }
          ]}
        />

        <Card className={classes.card}>
          <ToolbarTable
            filterName={filterName}
            onFilterName={handleFilterByName}
          />

          <Scrollbars>
            <TableContainer component={Box} sx={{ minWidth: 800 }}>
              <Table>
                <HeadTable
                  order={order}
                  classes={classes}
                  orderBy={orderBy}
                  headLabel={TABLE_HEAD}
                  rowCount={products.length}
                  onRequestSort={handleRequestSort}
                />
                <TableBody>
                  {filteredProducts
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row, index) => {
                      const {
                        _id,
                        name,
                        originalPrice,
                        rentPrice,
                        rentCount,
                        createdAt
                      } = row;

                      return (
                        <TableRow
                          hover
                          key={_id}
                          tabIndex={-1}
                          className={classes.row}
                        >
                          {/* <TableCell
                            padding="checkbox"
                            onClick={event => handleClick(event, name)}
                          >
                            <Checkbox checked={isItemSelected} />
                          </TableCell> */}
                          <TableCell
                            component="th"
                            scope="row"
                            style={{ maxWidth: 460 }}
                            padding="none"
                            className={classes.nameProduct}
                            onClick={() => handleClickDetail(_id)}
                          >
                            <Box
                              sx={{
                                py: 2,
                                display: 'flex',
                                alignItems: 'center'
                              }}
                            >
                              <Box
                                component="img"
                                alt={name}
                                src="/static/images/placeholder.svg"
                                data-src={row.thumbnail}
                                className="lazyload blur-up"
                                sx={{
                                  mx: 2,
                                  width: 64,
                                  height: 64,
                                  borderRadius: 1.5
                                }}
                              />
                              <Typography variant="subtitle2" noWrap>
                                {name}
                              </Typography>
                            </Box>
                          </TableCell>
                          <TableCell align="center" style={{ minWidth: 160 }}>
                            {moment(createdAt).format('DD/MM/YYYY')}
                          </TableCell>
                          <TableCell align="center" style={{ minWidth: 160 }}>
                            {rentCount}
                          </TableCell>
                          <TableCell align="center">
                            <MLabel
                              variant={
                                theme.palette.mode === 'light'
                                  ? 'ghost'
                                  : 'filled'
                              }
                              color={'success'}
                            >
                              {sentenceCase(`${fCurrency(originalPrice)}₫`)}
                            </MLabel>
                          </TableCell>
                          <TableCell align="center">
                            <MLabel
                              variant={
                                theme.palette.mode === 'light'
                                  ? 'ghost'
                                  : 'filled'
                              }
                              color={'warning'}
                            >
                              {sentenceCase(`${fCurrency(rentPrice)}₫`)}
                            </MLabel>
                          </TableCell>
                          <TableCell align="right">
                            <IconButton className={classes.margin}>
                              <Icon
                                icon={moreVerticalFill}
                                width={20}
                                height={20}
                              />
                            </IconButton>
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
                {isProductNotFound && (
                  <TableBody>
                    <TableRow>
                      <TableCell align="center" colSpan={6}>
                        <Box sx={{ py: 3 }}>
                          <SearchNotFound searchQuery={filterName} />
                        </Box>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                )}
              </Table>
            </TableContainer>
          </Scrollbars>

          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={products.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>
      </Container>
    </Page>
  );
}

export default ProductListView;
