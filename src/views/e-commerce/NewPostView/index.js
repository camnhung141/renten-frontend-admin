import * as Yup from 'yup';
import { useFormik } from 'formik';
import Page from '../../../components/Page';
import PreviewPost from './PreviewPost';
import React, { useState } from 'react';
import NewPostForm from './NewPostForm';
import { useHistory } from 'react-router-dom';
import { useSnackbar } from 'notistack';
import { PATH_APP } from '../../../routes/paths';
import fakeRequest from '../../../utils/fakeRequest';
import HeaderDashboard from '../../../components/HeaderDashboard';
import { makeStyles } from '@material-ui/core/styles';
import {
  Container,
  Card,
  CardContent,
  LinearProgress
} from '@material-ui/core';
import imageService from '../../../apis/images.api';
import { useSelector, useDispatch } from 'react-redux';
import { createProduct } from '../../../redux/slices/product';
// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {}
}));

// ----------------------------------------------------------------------

function NewPostView() {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [category, setCategory] = useState();
  const { enqueueSnackbar } = useSnackbar();
  const [files, setFiles] = useState([]);
  const [options, setOptions] = useState([]);
  const history = useHistory();
  const [unitRent, setUnitRent] = useState();
  const profile = useSelector(state => state.auth.myProfile);
  const { isLoading } = useSelector(state => state.product);
  const dispatch = useDispatch();

  const handleOpenPreview = () => {
    setOpen(true);
  };

  const handleClosePreview = () => {
    setOpen(false);
  };

  const NewProductSchema = Yup.object().shape({
    name: Yup.string().required('Tên sản phẩm là bắt buộc'),
    description: Yup.string().required('Nhập mô tả sản phẩm'),
    brand: Yup.string(),
    pointAccept: Yup.number()
      .min(1, 'Điểm phải lớn hơn 0')
      .max(5, 'Điểm tối đa là 5'),
    cover: Yup.mixed()
    // .test(
    //   'fileSize',
    //   `File is larger than ${fData(FILE_SIZE)}`,
    //   value => value && value.size <= FILE_SIZE
    // )
    // .test(
    //   'fileFormat',
    //   'File type must be *.jpeg, *.jpg, *.png, *.gif',
    //   value => value && FILE_FORMATS.includes(value.type)
    // )
  });

  const formik = useFormik({
    initialValues: {
      name: '',
      tags: '',
      brand: '',
      categoriesId: '',
      subCategoriesId: '',
      description: '',
      weight: 0,
      large: 0,
      width: 0,
      requiredPaperWorks: '',
      deposit: 0,
      pointAccept: 5,
      height: 0,
      cover: null
    },
    validationSchema: NewProductSchema,
    onSubmit: async (values, { setSubmitting, resetForm, setErrors }) => {
      const thumbnailProduct = await imageService.upload(files);
      for (let i = 0; i < options.length; i++) {
        if (options && options[i]) {
          const iimages = await imageService.upload(options[i].images, 6);
          options[i].images = iimages;
        }
      }
      const data = {
        name: values.name,
        thumbnail: thumbnailProduct[0],
        brand: values.brand,
        rentDetail: {
          unitRent: unitRent,
          requiredPaperWorks: [values.requiredPaperWorks],
          requiredPoint: values.pointAccept,
          deposit: values.deposit
        },
        weight: values.weight,
        originalPrice: parseFloat(options[0].originalPrice),
        size: {
          large: values.large,
          width: values.width,
          height: values.height
        },
        rentPrice: parseFloat(options[0].rentPrice),
        agencyId: profile.accountId?._id,
        categoriesId: [category],
        subCategoriesId: [values.subCategoriesId],
        tags: [values.tags],
        options: options,
        description: values.description
      };

      try {
        dispatch(createProduct(data));
        await fakeRequest(500);
        resetForm();
        handleClosePreview();
        setSubmitting(false);
        history.push('/app/management/e-commerce/list/');
        enqueueSnackbar('Đăng sản phẩm thành công', { variant: 'success' });
      } catch (error) {
        enqueueSnackbar('Không thể tạo sản phảm', { variant: 'error' });
        setSubmitting(false);
        setErrors({ afterSubmit: error.code });
      }
    }
  });

  if (isLoading) {
    return <LinearProgress />;
  }

  return (
    <Page title="Renter Mart | Tạo sản phẩm" className={classes.root}>
      <Container>
        <HeaderDashboard
          heading="Tạo sản phẩm"
          links={[
            { name: 'Bảng điều khiển', href: PATH_APP.root },
            { name: 'Quản lý', href: PATH_APP.management.root },
            { name: 'Sản phẩm', href: PATH_APP.management.eCommerce.root },
            { name: 'Tạo sản phẩm' }
          ]}
        />

        <Card>
          <CardContent>
            <NewPostForm
              formik={formik}
              onOpenPreview={handleOpenPreview}
              category={category}
              files={files}
              options={options}
              setOptions={setOptions}
              setFiles={setFiles}
              unitRent={unitRent}
              setUnitRent={setUnitRent}
              setCate={setCategory}
              isLoading={isLoading}
            />
          </CardContent>
        </Card>

        <PreviewPost
          formik={formik}
          openPreview={open}
          onClosePreview={handleClosePreview}
        />
      </Container>
    </Page>
  );
}

export default NewPostView;
