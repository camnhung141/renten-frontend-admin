import clsx from 'clsx';
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Form, FormikProvider } from 'formik';
import { LoadingButton } from '@material-ui/lab';
import { QuillEditor } from '../../../components/Editor';
import { makeStyles } from '@material-ui/core/styles';
import { UploadMultiFile } from '../../../components/Upload';
import { Icon } from '@iconify/react';
import closeFill from '@iconify-icons/eva/close-fill';

import { Grid, CircularProgress } from '@material-ui/core';
import NumberFormat from 'react-number-format';
import {
  Box,
  Button,
  TextField,
  Select,
  Typography,
  FormHelperText,
  FormControl,
  InputLabel,
  Card,
  CardContent,
  MenuItem
} from '@material-ui/core';
import categoryService from '../../../apis/category.api';
// ----------------------------------------------------------------------
const useStyles = makeStyles(theme => ({
  root: {},
  margin: {
    marginTop: theme.spacing(3)
  },
  imargin: {
    marginBottom: theme.spacing(2)
  },
  helperText: {
    padding: theme.spacing(0, 2)
  },
  addBtn: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  card: {
    margin: '1rem'
  },
  close: {
    color: 'gray',
    marginLeft: 'auto',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer'
  }
}));

// ----------------------------------------------------------------------

PostDetailsView.propTypes = {
  formik: PropTypes.object.isRequired,
  onOpenPreview: PropTypes.func,
  className: PropTypes.string
};

function NumberFormatCustom(props) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={values => {
        onChange({
          target: {
            name: props.name,
            value: values.value
          }
        });
      }}
      thousandSeparator
      isNumericString
      prefix="₫"
    />
  );
}

NumberFormatCustom.propTypes = {
  inputRef: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
};
function PostDetailsView({
  formik,
  onOpenPreview,
  category,
  setCate,
  files,
  setFiles,
  options,
  setOptions,
  unitRent,
  setUnitRent,
  isLoading,
  className,
  ...other
}) {
  const classes = useStyles();
  const {
    errors,
    values,
    touched,
    handleSubmit,
    isSubmitting,
    setFieldValue,
    getFieldProps
  } = formik;

  const [categories, setCategories] = useState([]);
  const [subCategories, setSubCategories] = useState([]);
  const [code, setCode] = useState();
  const [appearance, setAppearance] = useState('');
  const [price, setPrice] = useState();
  const [originalPrice, setOriginalPrice] = useState();
  const [images, setImages] = useState([]);
  const [error, setError] = useState('');
  const handleAddOption = () => {
    const check = options?.filter(option => option.productCode === code);
    if (check.length > 0) {
      setError(' *Mã đã được thêm vào trước đó');
      return;
    }
    if (!code) {
      setError(' *Mã phảm phẩm là bắt buộc');
      return;
    }
    const iOption = [
      ...options,
      {
        productCode: code,
        appearance: appearance,
        rentPrice: parseFloat(price),
        originalPrice: parseFloat(originalPrice),
        images
      }
    ];
    setOptions(iOption);
  };

  const handleClear = () => {
    setCode('');
    setAppearance('');
    setPrice('');
  };

  const handleCloseOption = code => {
    const iOption = options?.filter(e => e.productCode !== code);
    setOptions(iOption);
  };
  useEffect(() => {
    const getCategories = async () => {
      await categoryService.list().then(response => {
        setCategories(response);
      });
    };
    getCategories();
  }, []);

  useEffect(() => {
    const result = categories?.filter(iCategory => iCategory._id === category);
    setSubCategories(result[0]?.subCategories);
  }, [category, categories]);

  return (
    <FormikProvider value={formik}>
      <Form
        noValidate
        autoComplete="off"
        onSubmit={handleSubmit}
        className={clsx(classes.root, className)}
        {...other}
      >
        <TextField
          fullWidth
          label="Tên sản phẩm"
          required
          {...getFieldProps('name')}
          error={Boolean(touched.name && errors.name)}
          helperText={touched.name && errors.name}
          className={classes.margin}
        />
        <TextField
          fullWidth
          label="Tags"
          placeholder="Ex: xe-may-vision"
          required
          {...getFieldProps('tags')}
          error={Boolean(touched.tags && errors.tags)}
          helperText={touched.tags && errors.tags}
          className={classes.margin}
        />
        <TextField
          fullWidth
          label="Thương hiệu/Tác giả"
          {...getFieldProps('brand')}
          error={Boolean(touched.brand && errors.brand)}
          helperText={touched.brand && errors.brand}
          className={classes.margin}
        />
        <TextField
          fullWidth
          label="Điểm cho phép"
          placeholder="Điểm tối thiểu của khách hàng để được phép mua sản phẩm"
          {...getFieldProps('pointAccept')}
          error={Boolean(touched.pointAccept && errors.pointAccept)}
          helperText={touched.pointAccept && errors.pointAccept}
          className={classes.margin}
        />
        {/* <TextField
          fullWidth
          label="Giá gốc sản phẩm"
          {...getFieldProps('price')}
          error={Boolean(touched.price && errors.price)}
          helperText={touched.price && errors.price}
          className={classes.margin}
          name="price"
          id="formatted-numberformat-input"
          InputProps={{
            inputComponent: NumberFormatCustom
          }}
        />
        <TextField
          fullWidth
          label="Giá cho thuê"
          {...getFieldProps('rentPrice')}
          error={Boolean(touched.rentPrice && errors.rentPrice)}
          helperText={touched.rentPrice && errors.rentPrice}
          className={classes.margin}
          name="rentPrice"
          id="formatted-numberformat-input"
          InputProps={{
            inputComponent: NumberFormatCustom
          }}
        /> */}
        <FormControl
          variant="outlined"
          className={classes.margin}
          fullWidth
          required
        >
          <InputLabel id="category">Đơn vị thuê</InputLabel>
          <Select
            labelId="unitRent"
            id="unitRent"
            value={unitRent}
            onChange={e => setUnitRent(e.target.value)}
            label="Đơn vị thuê"
            fullWidth
            required
          >
            <MenuItem value="date">X Đồng/Ngày</MenuItem>
            <MenuItem value="hour">X Đồng/Giờ</MenuItem>
          </Select>
        </FormControl>
        <Grid container spacing={3}>
          <Grid item xs={12} md={6}>
            <TextField
              fullWidth
              type="text"
              label="Giấy tờ"
              required
              {...getFieldProps('requiredPaperWorks')}
              error={Boolean(
                touched.requiredPaperWorks && errors.requiredPaperWorks
              )}
              helperText={
                touched.requiredPaperWorks && errors.requiredPaperWorks
              }
              className={classes.margin}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField
              fullWidth
              type="number"
              label="Đền bù"
              required
              {...getFieldProps('deposit')}
              error={Boolean(touched.deposit && errors.deposit)}
              helperText={touched.deposit && errors.deposit}
              className={classes.margin}
            />
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          <Grid item xs={6}>
            <FormControl
              variant="outlined"
              className={classes.margin}
              fullWidth
              required
            >
              <InputLabel id="category">Danh mục</InputLabel>
              <Select
                labelId="category"
                id="category"
                value={category}
                onChange={e => setCate(e.target.value)}
                label="Danh mục"
                fullWidth
                required
              >
                {categories.map(element => (
                  <MenuItem value={element._id}>{element.name}</MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={6}>
            <FormControl
              variant="outlined"
              className={classes.margin}
              fullWidth
              required
            >
              <InputLabel id="category">Danh mục con</InputLabel>
              <Select
                labelId="category"
                id="subCategory"
                // value={age}
                {...getFieldProps('subCategoriesId')}
                label="Danh mục con"
                fullWidth
                required
              >
                {subCategories?.map(element => (
                  <MenuItem value={element._id}>{element.name}</MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
        </Grid>

        <Grid container spacing={3}>
          <Grid item xs={12} md={6}>
            <TextField
              fullWidth
              type="number"
              label="Large(cm)"
              required
              {...getFieldProps('large')}
              error={Boolean(touched.large && errors.large)}
              helperText={touched.large && errors.large}
              className={classes.margin}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField
              fullWidth
              type="number"
              label="Width(cm)"
              required
              {...getFieldProps('width')}
              error={Boolean(touched.width && errors.width)}
              helperText={touched.width && errors.width}
              className={classes.margin}
            />
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          <Grid item xs={12} md={6}>
            <TextField
              fullWidth
              type="number"
              label="Height(cm)"
              required
              {...getFieldProps('height')}
              error={Boolean(touched.height && errors.height)}
              helperText={touched.height && errors.height}
              className={classes.margin}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField
              fullWidth
              type="number"
              label="Weight(Kg)"
              required
              {...getFieldProps('weight')}
              error={Boolean(touched.weight && errors.weight)}
              helperText={touched.weight && errors.weight}
              className={classes.margin}
            />
          </Grid>
        </Grid>
        <div className={classes.margin}>
          <Typography variant="subtitle2" color="textSecondary" gutterBottom>
            Ảnh đại diện cho sản phẩm
          </Typography>
          {/* <UploadSingleFile
            value={values.cover}
            onChange={val => setFieldValue('cover', val)}
            caption="(Only *.jpeg and *.png images will be accepted)"
            error={Boolean(touched.cover && errors.cover)}
          /> */}
          <UploadMultiFile
            value={files}
            onChange={setFiles}
            caption="Tải ảnh lên"
          />

          <FormHelperText error className={classes.helperText}>
            {touched.cover && errors.cover}
          </FormHelperText>
        </div>

        <Typography variant="subtitle2" color="textSecondary" gutterBottom>
          Chi tiết từng sản phẩm
        </Typography>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={5}>
            <TextField
              fullWidth
              type="text"
              label="Mã sản phẩm"
              value={code}
              onChange={e => setCode(e.target.value)}
            />
            <p style={{ fontSize: '0.8rem', color: 'red' }}>{error}</p>
            <TextField
              fullWidth
              type="text"
              label="Tình trạng sản phẩm"
              placeholder="Tốt/Bị trầy xước, bị rách nhỏ..."
              value={appearance}
              onChange={e => setAppearance(e.target.value)}
              className={classes.margin}
            />
            <TextField
              fullWidth
              label="Giá gốc sản phẩm (VNĐ)"
              type="text"
              value={originalPrice}
              onChange={e => setOriginalPrice(e.target.value)}
              error={Boolean(touched.price && errors.price)}
              helperText={touched.price && errors.price}
              className={classes.margin}
              name="price"
              id="formatted-numberformat-input"
              InputProps={{
                inputComponent: NumberFormatCustom
              }}
            />
            <TextField
              fullWidth
              type="text"
              label="Giá thuê sản phẩm (VNĐ)"
              value={price}
              onChange={e => setPrice(e.target.value)}
              className={classes.margin}
              name="price"
              id="formatted-numberformat-input"
              InputProps={{
                inputComponent: NumberFormatCustom
              }}
            />
            <div className={classes.margin}>
              <Typography
                variant="subtitle2"
                color="textSecondary"
                gutterBottom
              >
                Ảnh sản phẩm
              </Typography>
              {/* <UploadSingleFile
            value={values.cover}
            onChange={val => setFieldValue('cover', val)}
            caption="(Only *.jpeg and *.png images will be accepted)"
            error={Boolean(touched.cover && errors.cover)}
          /> */}
              <UploadMultiFile
                value={images}
                onChange={setImages}
                // value={values.cover}
                // onChange={val => setFieldValue('cover', val)}
                caption="Chọn ảnh sản phẩm tương ứng"
              />

              <FormHelperText error className={classes.helperText}>
                {touched.images && errors.images}
              </FormHelperText>
            </div>
            <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
              <Button
                type="button"
                color="inherit"
                variant="outlined"
                onClick={handleClear}
                sx={{ mr: 1.5 }}
              >
                Làm mới
              </Button>
              <LoadingButton variant="contained" onClick={handleAddOption}>
                Thêm
              </LoadingButton>
            </Box>
            <Typography variant="subtitle2" color="textSecondary" gutterBottom>
              * Vui long nhấn thêm để lưu sản phẩm này, sản phẩm sau khi thêm sẽ
              nằm ô bên phải
            </Typography>
          </Grid>
          {/* <Grid item xs={12} sm={1} className={classes.addBtn}>
            <LoadingButton variant="contained" onClick={handleAddOption}>
              Thêm
            </LoadingButton>
          </Grid> */}

          <Grid item xs={12} sm={7}>
            <div
              style={{
                border: '1px solid #dfdada',
                margin: '0 0.5rem',
                minHeight: '14rem'
              }}
            >
              {options.map(element => (
                <Card className={classes.card} variant="outlined">
                  <CardContent>
                    <Grid container spacing={1}>
                      <Grid xs={12} md={8} style={{ paddingRight: '0.5rem' }}>
                        <Typography color="textSecondary" gutterBottom>
                          Code : {element.productCode}
                        </Typography>
                        <Typography variant="body2" component="p">
                          {element.appearance}
                        </Typography>
                      </Grid>
                      <Grid xs={12} md={3} style={{ color: 'green' }}>
                        <Typography variant="body2" component="p">
                          Giá: {element.rentPrice} đ
                        </Typography>
                      </Grid>
                      <Grid
                        xs={12}
                        md={1}
                        className={classes.close}
                        onClick={() => handleCloseOption(element.code)}
                      >
                        <Icon icon={closeFill} width={20} height={20} />
                      </Grid>
                    </Grid>
                  </CardContent>
                </Card>
              ))}
            </div>
          </Grid>
        </Grid>

        <div className={classes.margin}>
          <Typography variant="subtitle2" color="textSecondary" gutterBottom>
            Mô tả sản phẩm
          </Typography>
          <QuillEditor
            id="post-content"
            value={values.description}
            onChange={val => setFieldValue('description', val)}
            error={Boolean(touched.description && errors.description)}
          />
          <FormHelperText error className={classes.helperText}>
            {touched.description && errors.description}
          </FormHelperText>
        </div>
        <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
          {/* <Button
            type="button"
            color="inherit"
            variant="outlined"
            onClick={onOpenPreview}
            sx={{ mr: 1.5 }}
          >
            Xem trước
          </Button> */}
          <Button type="submit" variant="contained" pending={isSubmitting}>
            {isLoading && (
              <CircularProgress size={24} className={classes.loading} />
            )}
            {!isLoading && 'Tạo sản phẩm'}
          </Button>
        </Box>
      </Form>
    </FormikProvider>
  );
}

export default PostDetailsView;
