import clsx from 'clsx';
import React from 'react';
import * as Yup from 'yup';
import PropTypes from 'prop-types';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import useIsMountedRef from '../../../hooks/useIsMountedRef';
import { Form, FormikProvider, useFormik } from 'formik';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Grid, TextField } from '@material-ui/core';

// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {},
  margin: {
    color: 'green'
  }
}));

// ----------------------------------------------------------------------

General.propTypes = {
  className: PropTypes.string
};

function General({ className, profile }) {
  const classes = useStyles();
  const isMountedRef = useIsMountedRef();
  const { enqueueSnackbar } = useSnackbar();

  const UpdateUserSchema = Yup.object().shape({
    fullName: Yup.string()
      .min(2, 'Quá ngắn!')
      .required('Họ và tên bắt buộc.'),
    phone: Yup.string().required('Số điện thoại là bắt buộc'),
    city: Yup.string(),
    district: Yup.string(),
    ward: Yup.string(),
    street: Yup.string(),
    email: Yup.string()
      .email('Email không hợp lệ')
      .required('Email là bắt buộc'),
    password: Yup.string().required('Password is required'),
    cnfPassword: Yup.string().required('Password is required'),
    agencyName: Yup.string().required('Shop name required'),
    // identityNumber: Yup.string().required('Số CMND hoặc CCCD là bắt buộc'),
    // dateOfIssue: Yup.string().required('Ngày cấp  bắt buộc'),
    // placeOfIssue: Yup.string().required('Nơi cấp là bắt buộc'),
    gender: Yup.string()
  });

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      fullName: profile.ownerProfile?.name || '',
      email: profile.email || '',
      phone: profile.ownerProfile?.phoneNumber || '',
      city: profile.ownerProfile?.address?.city || '',
      district: profile.ownerProfile?.address?.district || '',
      ward: profile.ownerProfile?.address?.ward || '',
      gender: profile.ownerProfile?.gender || '',
      street: profile.ownerProfile?.address?.street || '',
      dateOfBirth:
        moment(profile.ownerProfile?.dateOfBirthday).format('DD/MM/YYYY') || '',
      // identityNumber: '',
      // dateOfIssue: '',
      // placeOfIssue: '',
      agencyName: profile?.agencyName || '',
      shopCity: profile.address?.city || '',
      shopPhone: profile?.phoneNumber || '',
      shopDistrict: profile.address?.district || '',
      shopWard: profile.address?.ward || '',
      fanPage: profile?.facebook || '',
      shopStreet: profile.address?.street || '',
      about: profile.about || ''
    },
    validationSchema: UpdateUserSchema,
    onSubmit: async ({ setErrors, setSubmitting }) => {
      try {
        enqueueSnackbar('Update success', { variant: 'success' });
        if (isMountedRef.current) {
          setSubmitting(false);
        }
      } catch (err) {
        if (isMountedRef.current) {
          setErrors({ afterSubmit: err.code });
          setSubmitting(false);
        }
      }
    }
  });

  const { errors, touched, handleSubmit, getFieldProps } = formik;

  return (
    <div className={clsx(classes.root, className)}>
      <FormikProvider value={formik}>
        <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
          <p style={{ fontSize: '1.5rem', color: 'green' }}>
            1. Thông tin người đại diện
          </p>
          <Box sx={{ mb: 3 }} />
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                disabled
                label="Họ và tên người đại diện"
                {...getFieldProps('fullName')}
                error={Boolean(touched.fullName && errors.fullName)}
                helperText={touched.fullName && errors.fullName}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                disabled
                name="phone"
                label="Số điện thoại của chủ của hàng"
                {...getFieldProps('phone')}
                error={Boolean(touched.phone && errors.phone)}
                helperText={touched.phone && errors.phone}
              />
            </Grid>
          </Grid>
          <Box sx={{ mb: 3 }} />
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                disabled
                name="gender"
                label="Giới tính"
                {...getFieldProps('gender')}
                error={Boolean(touched.gender && errors.gender)}
                helperText={touched.gender && errors.gender}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                disabled
                name="dateOfBirth"
                label="Ngày sinh"
                {...getFieldProps('dateOfBirth')}
                error={Boolean(touched.dateOfBirth && errors.dateOfBirth)}
                helperText={touched.dateOfBirth && errors.dateOfBirth}
              />
            </Grid>
          </Grid>

          <Box sx={{ mb: 3 }} />
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                name="city"
                disabled
                label="Thành phố"
                {...getFieldProps('city')}
                error={Boolean(touched.city && errors.city)}
                helperText={touched.city && errors.city}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                name="district"
                disabled
                label="Quận/Huyện"
                {...getFieldProps('district')}
                error={Boolean(touched.district && errors.district)}
                helperText={touched.district && errors.district}
              />
            </Grid>
          </Grid>

          <Box sx={{ mb: 3 }} />

          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                name="ward"
                disabled
                label="Phường/Xã"
                {...getFieldProps('ward')}
                error={Boolean(touched.ward && errors.ward)}
                helperText={touched.ward && errors.ward}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                name="street"
                disabled
                label="Địa chỉ"
                {...getFieldProps('street')}
                error={Boolean(touched.street && errors.street)}
                helperText={touched.street && errors.street}
              />
            </Grid>
          </Grid>
          <Box sx={{ mt: 3 }}>
            <p style={{ fontSize: '1.5rem', color: 'green' }}>
              2. Thông tin của cửa hàng
            </p>
            <Box sx={{ mb: 3 }} />
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  fullWidth
                  disabled
                  name="shop"
                  label="Tên cửa hàng"
                  {...getFieldProps('agencyName')}
                  error={Boolean(touched.agencyName && errors.agencyName)}
                  helperText={touched.agencyName && errors.agencyName}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  disabled
                  fullWidth
                  name="shopPhone"
                  label="Hotline/Số điện thoại liên hệ"
                  {...getFieldProps('shopPhone')}
                  error={Boolean(touched.shopPhone && errors.shopPhone)}
                  helperText={touched.shopPhone && errors.shopPhone}
                />
              </Grid>
            </Grid>
            <Box sx={{ mb: 3 }} />
            <TextField
              disabled
              fullWidth
              name="fanPage"
              label="Link FanPage/facebook"
              {...getFieldProps('fanPage')}
              error={Boolean(touched.fanPage && errors.fanPage)}
              helperText={touched.fanPage && errors.fanPage}
            />
            <Box sx={{ mb: 3 }} />
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  disabled
                  fullWidth
                  name="shopCity"
                  label="Thành phố"
                  {...getFieldProps('shopCity')}
                  error={Boolean(touched.shopCity && errors.shopCity)}
                  helperText={touched.shopCity && errors.shopCity}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  disabled
                  fullWidth
                  name="shopDistrict"
                  label="Quận/Huyện"
                  {...getFieldProps('shopDistrict')}
                  error={Boolean(touched.shopDistrict && errors.shopDistrict)}
                  helperText={touched.shopDistrict && errors.shopDistrict}
                />
              </Grid>
            </Grid>

            <Box sx={{ mb: 3 }} />

            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  disabled
                  fullWidth
                  name="shopWard"
                  label="Phường/Xã"
                  {...getFieldProps('shopWard')}
                  error={Boolean(touched.shopWard && errors.shopWard)}
                  helperText={touched.shopWard && errors.shopWard}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  disabled
                  fullWidth
                  name="shopStreet"
                  label="Địa chỉ"
                  {...getFieldProps('shopStreet')}
                  error={Boolean(touched.shopStreet && errors.shopStreet)}
                  helperText={touched.shopStreet && errors.shopStreet}
                />
              </Grid>
            </Grid>
            <Box sx={{ mb: 3 }} />
            <TextField
              disabled
              fullWidth
              name="about"
              label="Mô tả ngắn về cửa hàng"
              {...getFieldProps('about')}
              error={Boolean(touched.about && errors.about)}
              helperText={touched.about && errors.about}
            />
            <Box sx={{ mb: 3 }} />
            {/* <LoadingButton
              type="submit"
              variant="contained"
              pending={isSubmitting}
            >
              Lưu thay đổi
            </LoadingButton> */}
          </Box>
        </Form>
      </FormikProvider>
    </div>
  );
}

export default General;
