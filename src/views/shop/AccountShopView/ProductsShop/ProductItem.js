import clsx from 'clsx';
import React from 'react';
import PropTypes from 'prop-types';
import { paramCase } from 'change-case';
import { PATH_APP } from '../../../../routes/paths';
import { fCurrency } from '../../../../utils/formatNumber';
import { Link as RouterLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Card, Link, Typography, CardContent } from '@material-ui/core';
import { MLabel } from '../../../../@material-extend';

// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {
    position: 'relative'
  },
  cardMediaWrap: {
    paddingTop: '100%',
    position: 'relative'
  },
  cardMedia: {
    top: 0,
    width: '100%',
    height: '100%',
    position: 'absolute'
  },
  titleProduct: {
    color: 'green',
    fontSize: '1.1rem'
  }
}));

// ----------------------------------------------------------------------

ProductItem.propTypes = {
  product: PropTypes.object,
  className: PropTypes.string
};

function ProductItem({ product, className, ...other }) {
  const classes = useStyles();
  const { name, thumbnail, originalPrice, rentPrice, status } = product;
  const linkTo =
    PATH_APP.management.eCommerce.root + '/product/' + paramCase(product._id);

  return (
    <Card className={clsx(classes.root, className)} {...other}>
      <div className={classes.cardMediaWrap}>
        {status && (
          <MLabel
            variant="filled"
            color={(status === 'sale' && 'error') || 'info'}
            sx={{
              top: 16,
              right: 16,
              position: 'absolute',
              textTransform: 'uppercase'
            }}
          >
            {status}
          </MLabel>
        )}
        <img
          alt="product-images"
          title={name}
          src="/static/images/placeholder.svg"
          data-src={thumbnail}
          className={clsx(classes.cardMedia, 'lazyload blur-up')}
        />
      </div>

      <CardContent>
        <Link to={linkTo} color="inherit" component={RouterLink}>
          <Typography variant="body2" noWrap className={classes.titleProduct}>
            {name}
          </Typography>
        </Link>

        <Box
          sx={{
            mt: 2,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between'
          }}
        >
          {/* <PreviewColor colors={colors} /> */}

          <Typography color="textPrimary" variant="subtitle1">
            <Box
              component="span"
              sx={{
                typography: 'body1'
              }}
            >
              Giá thuê: {fCurrency(rentPrice)}
            </Box>
            <br></br>
            <Box
              component="span"
              sx={{
                typography: 'body1',
                color: 'text.disabled'
              }}
            >
              Giá gốc:{fCurrency(originalPrice)}
            </Box>
          </Typography>
        </Box>
      </CardContent>
    </Card>
  );
}

export default ProductItem;
