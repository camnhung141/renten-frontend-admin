import General from './General';
import { Icon } from '@iconify/react';
import Page from '../../../components/Page';
// import Notifications from './Notifications';
import { useParams } from 'react-router-dom';
import { PATH_APP } from '../../../routes/paths';
import React, { useState, useEffect } from 'react';
import clipboardFill from '@iconify-icons/eva/clipboard-fill';
import listFill from '@iconify-icons/eva/list-fill';
import { useDispatch, useSelector } from 'react-redux';
import HeaderDashboard from '../../../components/HeaderDashboard';
import roundAccountBox from '@iconify-icons/ic/round-account-box';
import {
  getProfileAgency
  // getNotifications
} from '../../../redux/slices/agency';
import { makeStyles } from '@material-ui/core/styles';
import { Container, Tab, Box, Tabs } from '@material-ui/core';
import ProductsShop from './ProductsShop';
import InvoiceView from './InvoiceView';

// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {},
  tabBar: {
    marginBottom: theme.spacing(5)
  },
  profile: {
    backgroundColor: 'white',
    width: '100%',
    border: '1px solid #ddd',
    boxSizing: ' border-box',
    margin: '2rem auto',
    boxShadow: ' 0px 0px 16px -1px rgba(0, 0, 0, 0.55)',
    borderRadius: '5px'
  },
  profileHeaderImageContainer: {
    position: 'relative',
    paddingBottom: '40%',
    overflow: 'hidden',
    marginBottom: '-50px'
  },
  profileHeaderImage: {
    borderTopLeftRadius: '5px',
    borderTopRightRadius: '5px',
    position: 'absolute',
    top: '0',
    left: '0',
    width: '100%'
  },

  // @supports (object-fit: cover) {
  //   .profile__header-image {
  //     object-fit: cover;
  //     height: 100%;
  //   }
  // },
  profileUser: {
    display: 'flex',
    alignItems: 'center',
    margin: '0 40px'
  },
  profileAvatar: {
    borderRadius: '50%',
    border: '10px solid white',
    display: 'inline-block',
    width: '200px',
    position: 'relative',
    top: '-10px'
  },
  profileDetails: {
    display: 'inline-block',
    position: 'relative',
    marginLeft: '30px'
  },
  profileName: {
    marginBottom: '0'
  },
  profileSubtitle: {
    display: 'inline-block',
    color: '#aaa'
  },
  profileActions: {
    flexGrow: '1',
    textAlign: 'right'
  },
  button: {
    display: 'inline-block',
    backgroundColor: '#697663',
    padding: '12px 25px',
    color: 'white',
    textDecoration: 'none',
    border: '2px solid #697663',
    borderRadius: '25px',
    transition: 'all 0.2s ease-out',
    '&hover': {
      background: 'none',
      color: '#697663'
    }
  }
}));

// ----------------------------------------------------------------------

function AccountView() {
  const classes = useStyles();
  const { id } = useParams();
  const [currentTab, setCurrentTab] = useState('general');
  const dispatch = useDispatch();
  const information = useSelector(state => state.agency.profileShop);

  useEffect(() => {
    // dispatch(getCards());
    // dispatch(getAddressBook());
    // dispatch(getInvoices());
    // dispatch(getNotifications());
    dispatch(getProfileAgency(id));
  }, [dispatch, id]);

  if (!information) {
    return null;
  }

  // if (!information?.cards) {
  //   return null;
  // }

  // if (!information?.notifications) {
  //   return null;
  // }

  const ACCOUNT_TABS = [
    {
      key: 'general',
      value: 'Chung',
      icon: <Icon icon={roundAccountBox} width={20} height={20} />,
      component: <General profile={information} />
    },
    {
      value: 'Sản phẩm',
      key: 'product',
      icon: <Icon icon={listFill} width={20} height={20} />,
      component: <ProductsShop agencyId={information.accountId._id} />
    },
    {
      value: 'Đơn hàng',
      key: 'billing',
      icon: <Icon icon={clipboardFill} width={20} height={20} />,
      component: <InvoiceView accountId={information.accountId._id} />
    }
  ];

  const handleChangeTab = (event, newValue) => {
    setCurrentTab(newValue);
  };

  return (
    <Page title="Management | Account Settings" className={classes.root}>
      <Container>
        <HeaderDashboard
          heading="Tài khoản cửa hàng"
          links={[
            { name: 'Bảng điều khiển', href: PATH_APP.root },
            { name: 'Quản lí', href: PATH_APP.management.root },
            { name: 'Cửa hàng', href: PATH_APP.management.shop.root },
            { name: 'Tài khoản cửa hàng' }
          ]}
        />

        <div className={classes.profile}>
          <div className={classes.profileHeaderImageContainer}>
            <img
              alt={''}
              className={classes.profileHeaderImage}
              data-src="https://cdn.somedomain.tech/samples/leaf.jpg"
              src="https://img3.thuthuatphanmem.vn/uploads/2019/07/12/anh-bia-fb-don-gian_062636624.jpg"
            />
          </div>
          <div className={classes.profileUser}>
            <img
              alt={''}
              className={classes.profileAvatar}
              src={
                information.avatar ||
                'https://image.freepik.com/free-vector/shop-icon-vector_7315-22.jpg'
              }
            />
            <div className={classes.profileDetails}>
              <h1 className={classes.profileName}>{information.agencyName}</h1>
              <p className={classes.profileSubtitle}>
                Email: {information.email}
              </p>
            </div>

            <div className={classes.profileActions}>
              <a
                href="https://twitter.com/eduardoboucas"
                className={classes.button}
              >
                Follow
              </a>
            </div>
          </div>
        </div>
        <Tabs
          value={currentTab}
          scrollButtons="auto"
          variant="scrollable"
          allowScrollButtonsMobile
          onChange={handleChangeTab}
          className={classes.tabBar}
        >
          {ACCOUNT_TABS.map(tab => {
            return (
              <Tab
                disableRipple
                key={tab.key}
                label={tab.value}
                icon={tab.icon}
                value={tab.key}
              />
            );
          })}
        </Tabs>

        {ACCOUNT_TABS.map(tab => {
          const isMatched = tab.key === currentTab;
          return isMatched && <Box key={tab.key}>{tab.component}</Box>;
        })}
      </Container>
    </Page>
  );
}

export default AccountView;
