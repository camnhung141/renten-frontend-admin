import React, { useState, useEffect } from 'react';
import Page from '../../../../components/Page';
import InvoiceDetail from './InvoiceDetail';
import { useTheme, makeStyles } from '@material-ui/core/styles';
import { sentenceCase } from 'change-case';
import { Container } from '@material-ui/core';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { MLabel } from '../../../../@material-extend';
import { useDispatch, useSelector } from 'react-redux';
import { getOrdersOfAgency } from '../../../../redux/slices/order';
// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {},
  card: {
    padding: theme.spacing(5, 5, 0)
  },
  gridItem: {
    marginBottom: theme.spacing(5)
  },
  tableHead: {
    borderBottom: `solid 1px ${theme.palette.divider}`,
    '& th': {
      backgroundColor: 'transparent'
    }
  },
  row: {
    borderBottom: `solid 1px ${theme.palette.divider}`
  },
  rowResult: {
    '& td': {
      paddingTop: theme.spacing(1),
      paddingBottom: theme.spacing(1)
    }
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    color: 'green',
    flexShrink: 0
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
    color: theme.palette.text.secondary
  },
  lastHeading: {
    fontSize: theme.typography.pxToRem(15)
  },
  item: {
    marginBottom: '1rem'
  }
}));

// ----------------------------------------------------------------------

function InvoiceView({ accountId }) {
  const classes = useStyles();
  const [expanded, setExpanded] = useState(false);
  const theme = useTheme();
  const dispatch = useDispatch();
  const ordersAgency = useSelector(state => state.order.aOrders);
  const handleChange = panel => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  useEffect(() => {
    dispatch(getOrdersOfAgency(accountId));
  }, [dispatch, accountId]);
  return (
    <Page title="Management | Invoice Details" className={classes.root}>
      <Container>
        {ordersAgency.map(element => (
          <Accordion
            expanded={expanded === element._id}
            onChange={handleChange(element._id)}
            className={classes.item}
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1bh-content"
              id={element._id}
            >
              <Typography className={classes.heading}>
                Mã đơn hàng: {element._id}
              </Typography>
              <Typography className={classes.secondaryHeading}>
                <MLabel
                  variant={theme.palette.mode === 'light' ? 'ghost' : 'filled'}
                  color={
                    ((element.status === 'waitingToConfirm' ||
                      element.status === 'processing' ||
                      element.status === 'transporting') &&
                      'warning') ||
                    ((element.status === 'cancel' ||
                      element.status === 'block') &&
                      'error') ||
                    'success'
                  }
                >
                  {sentenceCase(element.status)}
                </MLabel>
              </Typography>
              <Typography className={classes.lastHeading}>
                Tổng: {element.totalPrice + 'đ'}
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                <InvoiceDetail id={element._id} />
              </Typography>
            </AccordionDetails>
          </Accordion>
        ))}
      </Container>
    </Page>
  );
}

export default InvoiceView;
