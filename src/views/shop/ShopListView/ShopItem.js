import clsx from 'clsx';
import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from '@iconify/react';
import { fShortenNumber } from '../../../utils/formatNumber';
import twitterFill from '@iconify-icons/eva/twitter-fill';
import linkedinFill from '@iconify-icons/eva/linkedin-fill';
import facebookFill from '@iconify-icons/eva/facebook-fill';
import instagramFilled from '@iconify-icons/ant-design/instagram-filled';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Card,
  Grid,
  Avatar,
  Tooltip,
  Divider,
  CardMedia,
  Typography,
  IconButton,
  CardContent
} from '@material-ui/core';
import { MIcon } from '../../../@material-extend';

// ----------------------------------------------------------------------

const SOCIALS = [
  {
    name: 'Facebook',
    icon: <Icon icon={facebookFill} width={20} height={20} color="#1877F2" />
  },
  {
    name: 'Instagram',
    icon: <Icon icon={instagramFilled} width={20} height={20} color="#D7336D" />
  },
  {
    name: 'Linkedin',
    icon: <Icon icon={linkedinFill} width={20} height={20} color="#006097" />
  },
  {
    name: 'Twitter',
    icon: <Icon icon={twitterFill} width={20} height={20} color="#1C9CEA" />
  }
];

const useStyles = makeStyles(theme => ({
  root: {},
  cardMediaWrap: {
    display: 'flex',
    position: 'relative',
    justifyContent: 'center',
    paddingTop: 'calc(100% * 9 / 16)'
    // '&:before': {
    //   top: 0,
    //   zIndex: 9,
    //   content: "''",
    //   width: '100%',
    //   height: '100%',
    //   position: 'absolute',
    //   backdropFilter: 'blur(3px)',
    //   borderTopLeftRadius: theme.shape.borderRadiusMd,
    //   borderTopRightRadius: theme.shape.borderRadiusMd,
    //   backgroundColor: alpha(theme.palette.primary.darker, 0.72)
    // }
  },
  cardMedia: {
    top: 0,
    zIndex: 8,
    width: '100%',
    height: '100%',
    position: 'absolute'
  },
  shopName: {
    cursor: 'pointer',
    '&:hover': {
      color: 'rgb(7, 177, 77, 0.42)'
    }
  },
  avatarShape: {
    zIndex: 10,
    bottom: -26,
    position: 'absolute'
  },
  cardContent: {
    paddingBottom: 0,
    marginTop: theme.spacing(3)
  }
}));

// ----------------------------------------------------------------------

const FollowItem = number => (
  <Grid item xs={4}>
    <Box
      sx={{
        mb: 0.5,
        textAlign: 'center',
        typography: 'caption',
        color: 'text.secondary'
      }}
    >
      Theo dõi
    </Box>
    <Typography align="center" variant="subtitle1">
      {fShortenNumber(number)}
    </Typography>
  </Grid>
);
const RateItem = number => (
  <Grid item xs={4}>
    <Box
      sx={{
        mb: 0.5,
        textAlign: 'center',
        typography: 'caption',
        color: 'text.secondary'
      }}
    >
      Đánh giá
    </Box>
    <Typography align="center" variant="subtitle1">
      {fShortenNumber(number)}
    </Typography>
  </Grid>
);
const ProductItem = number => (
  <Grid item xs={4}>
    <Box
      sx={{
        mb: 0.5,
        textAlign: 'center',
        typography: 'caption',
        color: 'text.secondary'
      }}
    >
      Sản phẩm
    </Box>
    <Typography align="center" variant="subtitle1">
      {fShortenNumber(number)}
    </Typography>
  </Grid>
);

ShopItem.propTypes = {
  user: PropTypes.object.isRequired,
  className: PropTypes.string
};

function ShopItem({ agency, className, history }) {
  const classes = useStyles();
  const {
    accountId,
    agencyName,
    avatar,
    follower,
    productTotal,
    rate,
    address
  } = agency;

  const handleClickShop = id => {
    history.push('/app/management/shop/account/' + id);
  };

  return (
    <Card className={clsx(classes.root, className)}>
      <div className={classes.cardMediaWrap}>
        <MIcon
          size={144}
          color="paper"
          src="/static/icons/shape-avatar.svg"
          className={classes.avatarShape}
        />
        <Avatar
          alt={agencyName}
          src={
            avatar
              ? avatar
              : 'https://beedesign.com.vn/wp-content/uploads/2020/08/tao-logo-shop-quan-ao-ny.jpg'
          }
          sx={{
            width: 64,
            height: 64,
            zIndex: 11,
            position: 'absolute',
            transform: 'translateY(-50%)'
          }}
        />
        <CardMedia
          component="img"
          title="cover"
          data-sizes="auto"
          src="https://cdn4.vectorstock.com/i/1000x1000/03/03/landscape-street-shop-background-vector-13450303.jpg"
          data-src={
            avatar
              ? avatar
              : 'https://cdn4.vectorstock.com/i/1000x1000/03/03/landscape-street-shop-background-vector-13450303.jpg'
          }
          className={clsx(classes.cardMedia, 'lazyload blur-up')}
        />
      </div>

      <CardContent className={classes.cardContent}>
        <Typography
          variant="subtitle1"
          align="center"
          className={classes.shopName}
          onClick={() => handleClickShop(accountId._id)}
        >
          {agencyName}
        </Typography>
        <Typography variant="body2" color="textSecondary" align="center">
          {address ? address.city : 'VN'}
        </Typography>
      </CardContent>

      <Box sx={{ textAlign: 'center', mt: 2, mb: 2.5 }}>
        {SOCIALS?.map(social => (
          <Tooltip key={social.name} title={social.name}>
            <IconButton>{social.icon}</IconButton>
          </Tooltip>
        ))}
      </Box>

      <Divider />

      <Box sx={{ py: 3 }}>
        <Grid container className={classes.footer}>
          {FollowItem(follower)}
          {RateItem(rate?.rateAverage)}
          {ProductItem(productTotal)}
        </Grid>
      </Box>
    </Card>
  );
}

export default ShopItem;
