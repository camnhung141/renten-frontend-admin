import clsx from 'clsx';
import React from 'react';
import ShopItem from './ShopItem';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Grid, Skeleton } from '@material-ui/core';

// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {}
}));

// ----------------------------------------------------------------------

const SkeletonLoad = (
  <Grid container spacing={3}>
    {[...Array(8)].map((item, index) => (
      <Grid item xs={12} sm={6} md={4} key={index}>
        <Skeleton
          component={Box}
          variant="rectangular"
          sx={{ width: '100%', paddingTop: '115%', borderRadius: 2 }}
        />
      </Grid>
    ))}
  </Grid>
);

ShopList.propTypes = {
  agencies: PropTypes.array.isRequired,
  className: PropTypes.string
};

function ShopList({ agencies, className, history }) {
  const classes = useStyles();

  return (
    <Grid container spacing={3} className={clsx(classes.root, className)}>
      {agencies?.map(agency => (
        <Grid key={agency?._id} item xs={12} sm={6} md={4}>
          <ShopItem agency={agency} history={history} />
        </Grid>
      ))}

      {!agencies?.length && SkeletonLoad}
    </Grid>
  );
}

export default ShopList;
