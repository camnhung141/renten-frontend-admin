import ShopList from './ShopList';
import Page from '../../../components/Page';
import React, { useEffect } from 'react';
import { PATH_APP } from '../../../routes/paths';
import { getAgencyList } from '../../../redux/slices/agency';
import { useDispatch, useSelector } from 'react-redux';
import HeaderDashboard from '../../../components/HeaderDashboard';
import { makeStyles } from '@material-ui/core/styles';
import { Container } from '@material-ui/core';

// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {}
}));

// ----------------------------------------------------------------------

function ShopCardsView(props) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { agencyList } = useSelector(state => state.agency);

  useEffect(() => {
    dispatch(getAgencyList());
  }, [dispatch]);

  return (
    <Page title="Quản lý | Các cửa hàng đồ thuê" className={classes.root}>
      <Container>
        <HeaderDashboard
          heading="Danh sách cửa hàng"
          links={[
            { name: 'Bảng điều khiển', href: PATH_APP.root },
            { name: 'Quản lý', href: PATH_APP.management.root },
            { name: 'Cửa hàng', href: PATH_APP.management.user.root },
            { name: 'Danh sách cửa hàng' }
          ]}
        />
        <ShopList agencies={agencyList} history={props.history} />
      </Container>
    </Page>
  );
}

export default ShopCardsView;
