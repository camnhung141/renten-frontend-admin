import Page from '../../../components/Page';
import { PATH_APP } from '../../../routes/paths';
import React, { useEffect } from 'react';
import { visuallyHidden } from '@material-ui/utils';
import { useDispatch, useSelector } from 'react-redux';
import HeaderDashboard from '../../../components/HeaderDashboard';
import { makeStyles } from '@material-ui/core/styles';
import { Container } from '@material-ui/core';
import LoadingScreen from '../../../components/LoadingScreen';
import NewInvoice from './NewInvoice';
import { getAllOrders } from '../../../redux/slices/order';
// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {},
  nameProduct: {
    cursor: 'pointer',
    '&:hover': {
      color: 'rgb(7, 177, 77, 0.42)'
    }
  },
  sortSpan: visuallyHidden
}));

// ----------------------------------------------------------------------

function ProductListView(props) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const orders = useSelector(state => state.order.myOrders);

  useEffect(() => {
    dispatch(getAllOrders());
  }, [dispatch]);

  if (!orders) {
    return <LoadingScreen />;
  }
  return (
    <Page title="Management | Product List" className={classes.root}>
      <Container>
        <HeaderDashboard
          heading="Danh sách đơn hàng"
          links={[
            { name: 'Bảng điều khiển', href: PATH_APP.root },
            { name: 'Quản lí', href: PATH_APP.management.root },
            { name: 'Đơn hàng', href: PATH_APP.management.order.root },
            { name: 'Danh sách đơn hàng' }
          ]}
        />
        <NewInvoice orders={orders} />
      </Container>
    </Page>
  );
}

export default ProductListView;
