import { filter } from 'lodash';
import clsx from 'clsx';
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { fCurrency } from '../../../utils/formatNumber';
import Scrollbars from '../../../components/Scrollbars';
import MoreButton from '../../../components/MoreButton';
import { useHistory } from 'react-router-dom';
import { useTheme, makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Card,
  Table,
  TableRow,
  TableBody,
  TableCell,
  FormControl,
  TablePagination,
  Typography,
  TableHead,
  MenuItem,
  Select,
  Grid,
  TableContainer
} from '@material-ui/core';
import { MLabel } from '../../../@material-extend';
import AboutTime from '../InvoiceView/AboutTime';
import SearchNotFound from '../../../components/SearchNotFound';
// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {},
  warning: {
    backgroundColor: '#fff7dfad'
  },
  detailInvoice: {
    cursor: 'pointer',
    '&:hover': {
      color: 'rgb(7, 177, 77, 0.42)'
    }
  },
  margin: {
    width: '90%',
    height: '60%'
  }
}));

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });

  if (query) {
    array = filter(array, _product => {
      return _product.name.toLowerCase().indexOf(query.toLowerCase()) !== -1;
    });
    return array;
  }
  return stabilizedThis.map(el => el[0]);
}
function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

// ----------------------------------------------------------------------

NewInvoice.propTypes = {
  className: PropTypes.string
};

function NewInvoice({ className, orders, ...other }) {
  const classes = useStyles();
  const theme = useTheme();
  const history = useHistory();
  const [iStatus, setIStatus] = useState('all');
  const filterName = '';
  const [expire, setExpire] = useState('all');
  const [list, setList] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const order = 'asc';
  const [filteredOrders, setFilteredOrders] = useState([]);
  const orderBy = 'createdAt';

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - list.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleClickDetail = id => {
    history.push('/app/management/order/detail/' + id);
  };

  const distanceDate = (a, b) => {
    const date1 = new Date(a);
    const ib = new Date(b);
    const date2 = new Date(
      `${ib.getMonth() + 1}/${ib.getDate() + 1}/${ib.getFullYear()}`
    );

    const diffTime = date2 - date1;
    return diffTime / (1000 * 60 * 60 * 24);
  };

  const filterStatus = () => {
    if (iStatus === 'all' || !iStatus) {
      return orders;
    }
    if (iStatus) {
      const filterStatusList = orders?.filter(e => {
        return (
          e.status === iStatus ||
          (iStatus === 'returned' && e.status === 'late')
        );
      });
      return filterStatusList;
    }
  };

  const isOrderNotFound = orders?.length === 0;
  const filterExpire = myList => {
    if (expire === 'all' || !expire) {
      return myList;
    }
    const filterExpireList = myList?.filter(e => {
      if (e.status !== 'returned' && e.status !== 'cancel') {
        if (expire === 'belowOne') {
          return (
            distanceDate(new Date(), e?.product.endTime) < 1 &&
            distanceDate(new Date(), e?.product.endTime) >= 0
          );
        }
        if (expire === 'aboveTwo') {
          return distanceDate(new Date(), e?.product.endTime) > 2;
        }
        if (expire === 'expired') {
          return distanceDate(new Date(), e?.product.endTime) < 0;
        }
        return (
          distanceDate(new Date(), e?.product.endTime) < 2 &&
          distanceDate(new Date(), e?.product.endTime) > 1
        );
      }
      return false;
    });
    return filterExpireList;
  };
  const renderStatus = status => {
    if (status === 'returned' || status === 'rented') {
      return 'makeStyles-ghostSuccess-97';
    }
    if (status === 'cancel') {
      return 'makeStyles-ghostError-82';
    }
    return 'makeStyles-ghostWarning-81';
  };

  useEffect(() => {
    const listToStatus = filterStatus();
    const listToExpire = filterExpire(listToStatus);
    setList(listToExpire);
  }, [orders, expire, iStatus]);

  useEffect(() => {
    const iFilter = applySortFilter(
      list,
      getComparator(order, orderBy),
      filterName
    );
    setFilteredOrders(iFilter);
  }, [list]);
  const statusOfOrder = status => {
    switch (status) {
      case 'returned':
        return 'Đã hoàn trả';
      case 'late':
        return 'Đã hoàn trả';
      case 'rented':
        return 'Đã thuê';
      case 'cancel':
        return 'Đã hủy';
      case 'waitingToConfirm':
        return 'Đợi xác nhận';
      case 'processing':
        return 'Đã xác nhận';
      case 'transporting':
        return 'Đang vận chuyển';
      case 'notReturned':
        return 'Không hoàn trả';
      default:
        break;
    }
  };

  return (
    <Card className={clsx(classes.root, className)} {...other}>
      <Grid container>
        <Grid item xs={12} md={3}></Grid>
        <Grid item xs={12} md={3}></Grid>
        <Grid item xs={12} md={3}>
          <Box sx={{ py: 3, textAlign: 'left' }}>
            <Typography variant="subtitle2" color="textSecondary" gutterBottom>
              Trạng thái
            </Typography>
            <FormControl variant="outlined" className={classes.margin} required>
              <Select
                labelId="category"
                id="status"
                required
                value={iStatus}
                onChange={e => setIStatus(e.target.value)}
                size="small"
                className={classes.margin}
              >
                <MenuItem value="all">Tất cả</MenuItem>
                <MenuItem value="waitingToConfirm">Đợi xác nhận</MenuItem>
                <MenuItem value="processing">Đã xác nhận</MenuItem>
                <MenuItem value="transporting">Đang vận chuyển</MenuItem>
                <MenuItem value="rented">Đã thuê</MenuItem>
                <MenuItem value="returned">Đã hoàn trả</MenuItem>
                <MenuItem value="notReturned">Không hoàn trả</MenuItem>
                <MenuItem value="cancel">Đã hủy</MenuItem>
              </Select>
            </FormControl>
          </Box>
        </Grid>
        <Grid item xs={12} md={3}>
          <Box sx={{ py: 3, textAlign: 'left' }}>
            <Typography variant="subtitle2" color="textSecondary" gutterBottom>
              Hạn trả
            </Typography>
            <FormControl
              disabled={
                iStatus === 'returned' || iStatus === 'cancel' ? true : false
              }
              variant="outlined"
              className={classes.margin}
              required
            >
              <Select
                labelId="expire"
                id="expire"
                value={expire}
                onChange={e => setExpire(e.target.value)}
                size="small"
                required
                className={classes.margin}
              >
                <MenuItem value="all">Tất cả</MenuItem>
                <MenuItem value="belowOne">Dưới một ngày</MenuItem>
                <MenuItem value="oneToTwo">Từ 1 đến 2 ngày</MenuItem>
                <MenuItem value="aboveTwo">Trên hai ngày</MenuItem>
                <MenuItem value="expired">Bị trễ hạn</MenuItem>
              </Select>
            </FormControl>
          </Box>
        </Grid>
      </Grid>
      <Scrollbars>
        <TableContainer component={Box} sx={{ minWidth: 720, mt: 3 }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Mã đơn hàng</TableCell>
                <TableCell>Người thuê</TableCell>
                <TableCell>Tổng đơn</TableCell>
                <TableCell>Trạng thái</TableCell>
                <TableCell align="center">Hạn trả</TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {filteredOrders
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map(element => (
                  <TableRow
                    key={element._id}
                    className={
                      element.status !== 'returned' &&
                      element.status !== 'cancel' &&
                      element.status !== 'notReturned' &&
                      element.status !== 'late' &&
                      distanceDate(new Date(), element?.product.endTime) < 1
                        ? classes.warning
                        : ''
                    }
                    onClick={() => handleClickDetail(element._id)}
                  >
                    <TableCell className={classes.detailInvoice}>
                      {element._id}
                    </TableCell>
                    <TableCell>{element.fullName}</TableCell>
                    <TableCell>{fCurrency(element.totalPrice)}</TableCell>
                    <TableCell>
                      <MLabel
                        variant={
                          theme.palette.mode === 'light' ? 'ghost' : 'filled'
                        }
                        color={
                          ((element.status === 'waitingToConfirm' ||
                            element.status === 'processing' ||
                            element.status === 'transporting') &&
                            'warning') ||
                          ((element.status === 'cancel' ||
                            element.status === 'notReturned') &&
                            'error') ||
                          'success'
                        }
                      >
                        <span
                          className={`makeStyles-root-79 makeStyles-ghost-93 ${renderStatus(
                            element.status
                          )} css-1a8w37c`}
                        >
                          {statusOfOrder(element.status)}
                        </span>
                      </MLabel>
                    </TableCell>
                    <TableCell>
                      <AboutTime
                        endTime={element.product.endTime}
                        status={element.status}
                        returned={element.history.returned}
                      />
                    </TableCell>
                    <TableCell align="right">
                      <MoreButton />
                    </TableCell>
                  </TableRow>
                ))}
              {emptyRows > 0 && (
                <TableRow style={{ height: 53 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
            {isOrderNotFound && (
              <TableBody>
                <TableRow>
                  <TableCell align="center" colSpan={6}>
                    <Box sx={{ py: 3 }}>
                      <SearchNotFound searchQuery={filterName} />
                    </Box>
                  </TableCell>
                </TableRow>
              </TableBody>
            )}
          </Table>
        </TableContainer>
      </Scrollbars>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={list.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Card>
  );
}

export default NewInvoice;
