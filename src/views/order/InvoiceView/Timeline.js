import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Timeline from '@material-ui/lab/Timeline';
import TimelineItem from '@material-ui/lab/TimelineItem';
import TimelineSeparator from '@material-ui/lab/TimelineSeparator';
import TimelineConnector from '@material-ui/lab/TimelineConnector';
import TimelineContent from '@material-ui/lab/TimelineContent';
import TimelineOppositeContent from '@material-ui/lab/TimelineOppositeContent';
import TimelineDot from '@material-ui/lab/TimelineDot';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ReceiptIcon from '@material-ui/icons/Receipt';
import PlaylistAddCheckIcon from '@material-ui/icons/PlaylistAddCheck';
import LocalShippingIcon from '@material-ui/icons/LocalShipping';
import SaveAltIcon from '@material-ui/icons/SaveAlt';
import AssignmentReturnIcon from '@material-ui/icons/AssignmentReturn';
import CancelIcon from '@material-ui/icons/Cancel';
import moment from 'moment';

const useStyles = makeStyles(theme => ({
  paper: {
    padding: '6px 16px'
  },
  secondaryTail: {
    backgroundColor: theme.palette.secondary.main
  }
}));

export default function CustomizedTimeline({ historyOrder, onTime }) {
  const classes = useStyles();
  return (
    <Timeline align="alternate">
      <TimelineItem></TimelineItem>
      <TimelineItem>
        <TimelineOppositeContent>
          <Typography variant="body2" color="textSecondary">
            {moment(historyOrder?.waitingToConfirm).format('HH:mm DD/MM/YYYY')}
          </Typography>
        </TimelineOppositeContent>
        <TimelineSeparator>
          <TimelineDot>
            <ReceiptIcon />
          </TimelineDot>
          <TimelineConnector />
        </TimelineSeparator>
        <TimelineContent>
          <Paper elevation={3} className={classes.paper}>
            <Typography variant="h6" component="h1">
              Đơn hàng đã đặt
            </Typography>
          </Paper>
        </TimelineContent>
      </TimelineItem>
      {historyOrder?.processing && (
        <TimelineItem>
          <TimelineOppositeContent>
            <Typography variant="body2" color="textSecondary">
              {moment(historyOrder?.processing).format('HH:mm DD/MM/YYYY')}
            </Typography>
          </TimelineOppositeContent>
          <TimelineSeparator>
            <TimelineDot color="primary">
              <PlaylistAddCheckIcon />
            </TimelineDot>
            <TimelineConnector />
          </TimelineSeparator>
          <TimelineContent>
            <Paper elevation={3} className={classes.paper}>
              <Typography variant="h6" component="h1">
                Đơn hàng được xác nhận
              </Typography>
            </Paper>
          </TimelineContent>
        </TimelineItem>
      )}
      {historyOrder?.transporting && (
        <TimelineItem>
          <TimelineOppositeContent>
            <Typography variant="body2" color="textSecondary">
              {moment(historyOrder?.transporting).format('HH:mm DD/MM/YYYY')}
            </Typography>
          </TimelineOppositeContent>
          <TimelineSeparator>
            <TimelineDot color="primary" variant="outlined">
              <LocalShippingIcon />
            </TimelineDot>
            <TimelineConnector className={classes.secondaryTail} />
          </TimelineSeparator>
          <TimelineContent>
            <Paper elevation={3} className={classes.paper}>
              <Typography variant="h6" component="h1">
                Đơn hàng đang vận chuyển
              </Typography>
            </Paper>
          </TimelineContent>
        </TimelineItem>
      )}
      {historyOrder?.rented && (
        <TimelineItem>
          <TimelineOppositeContent>
            <Typography variant="body2" color="textSecondary">
              {moment(historyOrder?.rented).format('HH:mm DD/MM/YYYY')}
            </Typography>
          </TimelineOppositeContent>
          <TimelineSeparator>
            <TimelineDot color="primary">
              <SaveAltIcon />
            </TimelineDot>
          </TimelineSeparator>
          <TimelineContent>
            <Paper elevation={3} className={classes.paper}>
              <Typography variant="h6" component="h1">
                Người thuê đã nhận được hàng
              </Typography>
            </Paper>
          </TimelineContent>
        </TimelineItem>
      )}
      {historyOrder?.returned && (
        <TimelineItem>
          <TimelineOppositeContent>
            <Typography variant="body2" color="textSecondary">
              {`${moment(historyOrder?.returned).format('HH:mm DD/MM/YYYY')} `}
              <span style={{ color: 'red' }}>{onTime < 0 ? '(Trễ)' : ''}</span>
            </Typography>
          </TimelineOppositeContent>
          <TimelineSeparator>
            <TimelineDot color="primary">
              <AssignmentReturnIcon />
            </TimelineDot>
          </TimelineSeparator>
          <TimelineContent>
            <Paper elevation={3} className={classes.paper}>
              <Typography variant="h6" component="h1">
                Hoàn trả thành công
              </Typography>
            </Paper>
          </TimelineContent>
        </TimelineItem>
      )}
      {historyOrder?.cancel && (
        <TimelineItem>
          <TimelineOppositeContent>
            <Typography variant="body2" color="textSecondary">
              {moment(historyOrder?.cancel).format('HH:mm DD/MM/YYYY')}
            </Typography>
          </TimelineOppositeContent>
          <TimelineSeparator>
            <TimelineDot style={{ backgroundColor: 'red' }}>
              <CancelIcon />
            </TimelineDot>
          </TimelineSeparator>
          <TimelineContent>
            <Paper elevation={3} className={classes.paper}>
              <Typography variant="h6" component="h1">
                Đơn hàng bị hủy
              </Typography>
            </Paper>
          </TimelineContent>
        </TimelineItem>
      )}
    </Timeline>
  );
}
