import clsx from 'clsx';
import PropTypes from 'prop-types';
import { Icon } from '@iconify/react';
import InvoicePDF from './InvoicePDF';
import React, { useState } from 'react';
import eyeFill from '@iconify-icons/eva/eye-fill';
import closeFill from '@iconify-icons/eva/close-fill';
import { DialogAnimate } from '../../../components/Animate';
import downloadFill from '@iconify-icons/eva/download-fill';
import { PDFDownloadLink, PDFViewer } from '@react-pdf/renderer';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Tooltip,
  IconButton,
  DialogActions,
  FormControl,
  Select,
  MenuItem,
  Typography
} from '@material-ui/core';
import { LoadingButton } from '@material-ui/lab';
import { MButton } from '../../../@material-extend';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import orderService from '../../../apis/order.api';
// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginBottom: theme.spacing(5),
    '& > *': { marginLeft: theme.spacing(1) }
  },
  date: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginBottom: theme.spacing(5),
    '& > *': { marginLeft: theme.spacing(1) }
  },
  element: {
    width: '30%',
    marginTop: theme.spacing(2)
  },
  dialogActions: {
    zIndex: 9,
    boxShadow: theme.shadows[25].z8
  }
}));

// ----------------------------------------------------------------------

Toolbar.propTypes = {
  orderDetail: PropTypes.object,
  invoice: PropTypes.object.isRequired,
  className: PropTypes.string
};

function Toolbar({ orderDetail, className, ...other }) {
  const classes = useStyles();
  const [openPDF, setOpenPDF] = useState(false);
  const { enqueueSnackbar } = useSnackbar();
  const [noti, setNoti] = useState(24);
  const handleOpenPreview = () => {
    setOpenPDF(true);
  };

  const handleClosePreview = () => {
    setOpenPDF(false);
  };

  const handleExpireNoti = async () => {
    const myTime = new Date(orderDetail?.product.endTime);
    myTime.setHours(myTime.getHours() - noti);
    try {
      await orderService.updateOrder(
        {
          dateNotif: myTime + '',
          status: orderDetail?.status
        },
        orderDetail?._id
      );
      window.location.reload();
    } catch (err) {
      enqueueSnackbar('Không thể cập nhật', { variant: 'error' });
    }
  };

  return (
    <>
      <div>
        <Typography variant="subtitle2" color="textSecondary" gutterBottom>
          Xét thời gian thông báo hết hạn cho người thuê:{' '}
          {orderDetail?.dateNotif
            ? moment(orderDetail?.dateNotif).format('HH:mm DD/MM/YYYY')
            : ''}
        </Typography>
        <>
          <FormControl className={classes.element} variant="standard">
            <Select
              displayEmpty
              inputProps={{ 'aria-label': 'Without label' }}
              value={noti}
              onChange={e => setNoti(e.target.value)}
            >
              <em>
                <MenuItem value={24}>Trước 1 ngày</MenuItem>
              </em>
              <MenuItem value={1}>Trước 1 giờ</MenuItem>
              <MenuItem value={6}>Trước 6 giờ</MenuItem>
              <MenuItem value={12}>Trước 12 giờ</MenuItem>
              <MenuItem value={24}>Trước 1 ngày</MenuItem>
              <MenuItem value={36}>Trước 1 ngày rưỡi</MenuItem>
              <MenuItem value={48}>Trước 2 ngày</MenuItem>
            </Select>
          </FormControl>
          <LoadingButton
            variant="outlined"
            style={{ margin: '0.8rem' }}
            onClick={handleExpireNoti}
          >
            {' '}
            Cập nhật
          </LoadingButton>
        </>
      </div>
      <div className={clsx(classes.root, className)} {...other}>
        {/* <MButton
          color="error"
          size="small"
          variant="contained"
          endIcon={<Icon icon={shareFill} />}
        >
          Share
        </MButton> */}

        <MButton
          color="info"
          size="small"
          variant="contained"
          onClick={handleOpenPreview}
          endIcon={<Icon icon={eyeFill} />}
        >
          Xem trước
        </MButton>

        <PDFDownloadLink
          document={<InvoicePDF orderDetail={orderDetail} />}
          fileName={`INVOICE-${orderDetail?._id}`}
          style={{ textDecoration: 'none' }}
        >
          {({ loading }) => (
            <LoadingButton
              size="small"
              pending={loading}
              variant="contained"
              pendingPosition="end"
              endIcon={<Icon icon={downloadFill} />}
            >
              Tải hợp đồng thuê
            </LoadingButton>
          )}
        </PDFDownloadLink>

        <DialogAnimate fullScreen open={openPDF}>
          <Box
            sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}
          >
            <DialogActions className={classes.dialogActions}>
              <Tooltip title="Close">
                <IconButton color="inherit" onClick={handleClosePreview}>
                  <Icon icon={closeFill} />
                </IconButton>
              </Tooltip>
            </DialogActions>
            <Box sx={{ flexGrow: 1, height: '100%', overflow: 'hidden' }}>
              <PDFViewer width="100%" height="100%" style={{ border: 'none' }}>
                <InvoicePDF orderDetail={orderDetail} />
              </PDFViewer>
            </Box>
          </Box>
        </DialogAnimate>
      </div>
    </>
  );
}

export default Toolbar;
