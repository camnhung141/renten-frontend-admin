import React from 'react';
import PropTypes from 'prop-types';
import { fCurrency } from '../../../utils/formatNumber';
import {
  Page,
  View,
  Text,
  Font,
  Image,
  Document,
  StyleSheet
} from '@react-pdf/renderer';
import moment from 'moment';
// ----------------------------------------------------------------------

Font.register({
  family: 'Roboto',
  fonts: [
    { src: '/fonts/Roboto-Regular.ttf' },
    { src: '/fonts/Roboto-Bold.ttf' }
  ]
});

const styles = StyleSheet.create({
  col4: { width: '25%' },
  col8: { width: '75%' },
  col6: { width: '50%' },
  mb8: { marginBottom: 8 },
  mb40: { marginBottom: 40 },
  overline: {
    fontSize: 8,
    marginBottom: 8,
    fontWeight: 700,
    letterSpacing: 1.2,
    textTransform: 'uppercase'
  },
  title: {
    fontSize: 15,
    marginBottom: 8,
    textAlign: 'center',
    fontWeight: 700,
    letterSpacing: 1.2,
    textTransform: 'uppercase'
  },
  h3: { fontSize: 16, fontWeight: 700 },
  h4: { fontSize: 13, fontWeight: 700 },
  body1: { fontSize: 10 },
  subtitle2: { fontSize: 9, fontWeight: 700 },
  alignRight: { textAlign: 'right' },
  page: {
    padding: '40px 24px 0 24px',
    fontSize: 9,
    lineHeight: 1.6,
    fontFamily: 'Roboto',
    backgroundColor: '#fff',
    textTransform: 'capitalize'
  },
  footer: {
    left: 0,
    right: 0,
    bottom: 0,
    padding: 24,
    margin: 'auto',
    borderTopWidth: 1,
    borderStyle: 'solid',
    position: 'absolute',
    borderColor: '#DFE3E8'
  },
  gridContainer: { flexDirection: 'row', justifyContent: 'space-between' },
  gridSignature: { marginTop: '2rem' },
  table: { display: 'flex', width: 'auto' },
  tableHeader: {
    width: '100%'
  },
  tableBody: {},
  tableRow: {
    padding: '8px 0',
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderStyle: 'solid',
    borderColor: '#DFE3E8'
  },
  noBorder: { paddingTop: 8, paddingBottom: 0, borderBottomWidth: 0 },
  tableCell_1: { width: '5%' },
  tableCell_2: { width: '50%', paddingRight: 14 },
  tableCell_3: { width: '15%' }
});

// ----------------------------------------------------------------------

InvoicePDF.propTypes = {
  invoice: PropTypes.object.isRequired
};

function InvoicePDF({ orderDetail }) {
  const invoiceFrom = `${orderDetail?.agency?.address?.streetAndNumber}, ${orderDetail?.agency?.address?.wardPrefix} ${orderDetail?.agency?.address?.wardName}, ${orderDetail?.agency?.address?.districtPrefix} ${orderDetail?.agency?.address?.districtName}, ${orderDetail?.agency?.address?.cityPrefix} ${orderDetail?.agency?.address?.cityName}`;
  const invoiceTo = `${orderDetail?.deliveryAddress.streetAndNumber}, ${orderDetail?.deliveryAddress.wardPrefix} ${orderDetail?.deliveryAddress.wardName}, ${orderDetail?.deliveryAddress.districtPrefix} ${orderDetail?.deliveryAddress.districtName}, ${orderDetail?.deliveryAddress.cityPrefix} ${orderDetail?.deliveryAddress.cityName} `;

  const distanceDate = (a, b) => {
    const date1 = new Date(a);
    const ib = new Date(b);
    const date2 = new Date(
      `${ib.getMonth() + 1}/${ib.getDate() + 1}/${ib.getFullYear()}`
    );

    const diffTime = date2 - date1;
    const itime = diffTime / (1000 * 60 * 60 * 24);
    if (Math.ceil(itime * 24) < 24) {
      return `${Math.ceil(itime * 24)} giờ`;
    }
    return `${Math.ceil(itime)} ngày`;
  };
  return (
    <Document>
      <Page size="A4" style={styles.page}>
        <View style={[styles.gridContainer, styles.mb40]}>
          <Image source="/static/brand/rent-logo.png" style={{ height: 32 }} />
          <View style={{ alignItems: 'right', flexDirection: 'column' }}>
            <Text style={styles.h3}>
              Thanh toán: {orderDetail?.payment.method}
            </Text>
            <Text>Mã đơn-{orderDetail?._id}</Text>
          </View>
        </View>

        <View style={[styles.gridContainer, styles.mb40]}>
          <View style={styles.col6}>
            <Text style={[styles.overline, styles.mb1]}>Bên cho thuê</Text>
            <Text style={styles.body1}>{orderDetail?.agency.agencyName}</Text>
            <Text style={styles.body1}>{invoiceFrom}</Text>
            <Text style={styles.body1}>{orderDetail?.agency.phoneNumber}</Text>
          </View>
          <View style={styles.col6}>
            <Text style={[styles.overline, styles.mb1]}>Bên thuê</Text>
            <Text style={styles.body1}>{orderDetail?.fullName}</Text>
            <Text style={styles.body1}>{invoiceTo}</Text>
            <Text style={styles.body1}>{orderDetail?.phoneNumber}</Text>
          </View>
        </View>

        <Text style={[styles.title, styles.mb1]}>Hợp đồng thuê sản phẩm</Text>
        <Text style={[styles.overline, styles.mb1]}>Thông tin đơn hàng</Text>

        <View style={styles.table}>
          <View style={styles.tableHeader}>
            <View style={styles.tableRow}>
              <View style={styles.tableCell_1}>
                <Text style={styles.subtitle2}>#</Text>
              </View>
              <View style={styles.tableCell_2}>
                <Text style={styles.subtitle2}>Sản phẩm</Text>
              </View>
              <View style={styles.tableCell_3}>
                <Text style={styles.subtitle2}>Ngày thuê</Text>
              </View>
              <View style={styles.tableCell_3}>
                <Text style={styles.subtitle2}>Ngày trả</Text>
              </View>
              <View style={[styles.tableCell_3, styles.alignRight]}>
                <Text style={styles.subtitle2}>Giá thuê</Text>
              </View>
              <View style={[styles.tableCell_3, styles.alignRight]}>
                <Text style={styles.subtitle2}>Giá gốc</Text>
              </View>
            </View>
          </View>

          <View style={styles.tableBody}>
            <View style={styles.tableRow} key={orderDetail?._id}>
              <View style={styles.tableCell_1}>
                <Text>{1}</Text>
              </View>
              <View style={styles.tableCell_2}>
                <Text style={styles.subtitle2}>
                  Mã: {orderDetail?.product.productCode}
                </Text>
                <Text style={styles.subtitle2}>
                  {orderDetail?.product.name}
                </Text>
                <Text>Giá gốc: {orderDetail?.originalPrice}</Text>
                <Text>
                  Tình trạng hiện tại:{' '}
                  {orderDetail?.appearanceBefore?.appearance}
                </Text>
              </View>
              <View style={styles.tableCell_3}>
                <Text>
                  {moment(orderDetail?.product?.beginTime).format('DD/MM/YYYY')}
                </Text>
              </View>
              <View style={styles.tableCell_3}>
                <Text>
                  {moment(orderDetail?.product?.endTime).format('DD/MM/YYYY')}
                </Text>
              </View>
              <View style={[styles.tableCell_3, styles.alignRight]}>
                <Text>{fCurrency(orderDetail?.product.rentPrice)}</Text>
              </View>
              <View style={[styles.tableCell_3, styles.alignRight]}>
                <Text>{fCurrency(orderDetail?.product.originalPrice)}</Text>
              </View>
            </View>

            <View style={[styles.tableRow, styles.noBorder]}>
              <View style={styles.tableCell_1} />
              <View style={styles.tableCell_2} />
              <View style={styles.tableCell_3} />
              <View style={styles.tableCell_3}>
                <Text>Tạm tính</Text>
              </View>
              <View style={[styles.tableCell_3, styles.alignRight]}>
                <Text>{fCurrency(orderDetail?.product.rentPrice)}</Text>
              </View>
            </View>

            <View style={[styles.tableRow, styles.noBorder]}>
              <View style={styles.tableCell_1} />
              <View style={styles.tableCell_2} />
              <View style={styles.tableCell_3} />
              <View style={styles.tableCell_3}>
                <Text>Giảm giá</Text>
              </View>
              <View style={[styles.tableCell_3, styles.alignRight]}>
                <Text>{fCurrency(-0)}</Text>
              </View>
            </View>

            <View style={[styles.tableRow, styles.noBorder]}>
              <View style={styles.tableCell_1} />
              <View style={styles.tableCell_2} />
              <View style={styles.tableCell_3} />
              <View style={styles.tableCell_3}>
                <Text>Số ngày</Text>
              </View>
              <View style={[styles.tableCell_3, styles.alignRight]}>
                <Text>
                  {distanceDate(
                    orderDetail?.product?.beginTime,
                    orderDetail?.product?.endTime
                  )}
                </Text>
              </View>
            </View>

            <View style={[styles.tableRow, styles.noBorder, styles.mb40]}>
              <View style={styles.tableCell_1} />
              <View style={styles.tableCell_2} />
              <View style={styles.tableCell_3} />
              <View style={styles.tableCell_3}>
                <Text style={styles.h4}>Tổng</Text>
              </View>
              <View style={[styles.tableCell_3, styles.alignRight]}>
                <Text style={styles.h4}>
                  {fCurrency(
                    orderDetail?.product.rentPrice *
                      parseInt(
                        distanceDate(
                          orderDetail?.product?.beginTime,
                          orderDetail?.product?.endTime
                        )?.split(' ')[0]
                      )
                  )}
                </Text>
              </View>
            </View>
          </View>
        </View>

        <View style={[styles.gridSignature, styles.gridContainer, styles.mb40]}>
          <View style={styles.col6}>
            <Text style={[styles.overline, styles.mb1]}>
              đại diện bên cho thuê
            </Text>
            <Text style={styles.body1}></Text>
            <Text style={styles.body1}></Text>
            <Text style={styles.body1}></Text>
          </View>
          <View style={styles.col6}>
            <Text style={[styles.overline, styles.mb1]}>Đại diện bên thuê</Text>
            <Text style={styles.body1}></Text>
            <Text style={styles.body1}></Text>
            <Text style={styles.body1}></Text>
          </View>
        </View>
        <View style={[styles.gridContainer, styles.footer]}>
          <View style={styles.col8}>
            <Text style={styles.subtitle2}>Lưu ý</Text>
            <Text>
              Mọi thông tin hóa đơn đã bao gồm thuế và phí vận chuyển! Cảm ơn!
            </Text>
          </View>
          <View style={[styles.col4, styles.alignRight]}>
            <Text style={styles.subtitle2}>Thắc mắc?</Text>
            <Text>rentmart@support.com</Text>
          </View>
        </View>
      </Page>
    </Document>
  );
}

export default InvoicePDF;
