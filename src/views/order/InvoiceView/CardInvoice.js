import React, { useEffect, useState } from 'react';
import { fCurrency } from '../../../utils/formatNumber';
import Scrollbars from '../../../components/Scrollbars';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Grid,
  Card,
  Table,
  Divider,
  TableRow,
  TableBody,
  TableHead,
  TableCell,
  Typography,
  Button,
  TableContainer
} from '@material-ui/core';
import { MLabel } from '../../../@material-extend';
import moment from 'moment';
import orderService from '../../../apis/order.api';
import { useSnackbar } from 'notistack';
import Timeline from './Timeline';
import Rating from './Rating';
import AboutTime from './AboutTime';

const useStyles = makeStyles(theme => ({
  root: {},
  card: {
    padding: theme.spacing(5, 5, 0),
    marginBottom: theme.spacing(2)
  },
  gridItem: {
    marginBottom: theme.spacing(5)
  },
  tableHead: {
    borderBottom: `solid 1px ${theme.palette.divider}`,
    '& th': {
      backgroundColor: 'transparent'
    }
  },
  row: {
    borderBottom: `solid 1px ${theme.palette.divider}`
  },
  rowResult: {
    '& td': {
      paddingTop: theme.spacing(1),
      paddingBottom: theme.spacing(1)
    }
  },
  subTitle: {
    color: 'gray'
  }
}));
export default function CardInvoice({ orderDetail }) {
  const classes = useStyles();
  const [btnCancel, setBtnCancel] = useState();
  const [btnAccept, setBtnAccept] = useState();
  const { roleType } = JSON.parse(localStorage.getItem('roleAccount'));
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    if (orderDetail?.status === 'waitingToConfirm') {
      setBtnCancel('Hủy đơn hàng');
      setBtnAccept('Xác nhận đơn hàng');
    }
    if (orderDetail?.status === 'processing') {
      setBtnAccept('Bắt đầu vận chuyển');
    }
    if (orderDetail?.status === 'transporting') {
      setBtnAccept('Xác nhận người thuê đã nhận');
    }
    if (orderDetail?.status === 'rented') {
      setBtnAccept('Đã hoàn trả hàng');
      setBtnCancel('Không được hoàn trả');
    }
  }, [orderDetail]);

  const distanceDate = (a, b) => {
    let date1;
    if (a) {
      date1 = new Date(a);
    } else {
      date1 = new Date();
    }
    const ib = new Date(b);
    const date2 = new Date(
      `${ib.getMonth() + 1}/${ib.getDate() + 1}/${ib.getFullYear()}`
    );

    const diffTime = date2 - date1;
    return diffTime / (1000 * 60 * 60 * 24);
  };

  const handleCancel = async () => {
    let data = {};
    if (orderDetail?.status === 'waitingToConfirm') {
      data.status = 'cancel';
    }
    if (orderDetail?.status === 'rented') {
      data.status = 'notReturned';
    }
    try {
      await orderService.updateOrder(data, orderDetail?._id);
      enqueueSnackbar('Cập nhật thành công', { variant: 'success' });
      window.location.reload();
    } catch (error) {
      enqueueSnackbar('Không thể cập nhật', { variant: 'error' });
    }
  };

  const handleAccept = async () => {
    let data = {};
    if (orderDetail?.status === 'waitingToConfirm') {
      data.status = 'processing';
    }
    if (orderDetail?.status === 'processing') {
      data.status = 'transporting';
    }
    if (orderDetail?.status === 'transporting') {
      data.status = 'rented';
    }

    if (orderDetail?.status === 'rented') {
      if (distanceDate(new Date(), orderDetail?.product.endTime) < 0) {
        data.status = 'late';
      } else {
        data.status = 'returned';
      }
    }
    try {
      await orderService.updateOrder(data, orderDetail?._id);
      enqueueSnackbar('Cập nhật thành công', { variant: 'success' });
      window.location.reload();
    } catch (error) {
      enqueueSnackbar('Không thể cập nhật', { variant: 'error' });
    }
  };

  return (
    <>
      <Card className={classes.card}>
        <Grid container>
          <Grid item xs={12} sm={6} className={classes.gridItem}>
            <Box
              component="img"
              alt="logo"
              src="/static/brand/rent-logo.png"
              sx={{ height: 48 }}
            />
          </Grid>

          <Grid item xs={12} sm={6} className={classes.gridItem}>
            <Box sx={{ textAlign: { sm: 'right' } }}>
              <MLabel color="success" sx={{ textTransform: 'uppercase' }}>
                {orderDetail?.payment.method}
              </MLabel>
              <Box sx={{ typography: 'h6', mt: 1 }}>
                Mã đơn hàng-{orderDetail?._id}
              </Box>
            </Box>
          </Grid>

          <Grid item xs={12} sm={6} className={classes.gridItem}>
            <Box sx={{ mb: 2, typography: 'overline', color: 'text.disabled' }}>
              Bên cho thuê
            </Box>
            <Typography variant="body2">
              {orderDetail?.agency?.agencyName}
            </Typography>
            <Typography variant="body2">
              {`${orderDetail?.agency?.address?.streetAndNumber}, ${orderDetail?.agency?.address?.wardPrefix} ${orderDetail?.agency?.address?.wardName}, ${orderDetail?.agency?.address?.districtPrefix} ${orderDetail?.agency?.address?.districtName}, ${orderDetail?.agency?.address?.cityPrefix} ${orderDetail?.agency?.address?.cityName}`}
            </Typography>
            <Typography variant="body2">
              Số điện thoại: {orderDetail?.agency?.phoneNumber}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={6} className={classes.gridItem}>
            <Box sx={{ mb: 2, typography: 'overline', color: 'text.disabled' }}>
              Bên thuê
            </Box>
            <Typography variant="body2">{orderDetail?.fullName}</Typography>
            <Typography variant="body2">
              {`${orderDetail?.deliveryAddress.streetAndNumber}, ${orderDetail?.deliveryAddress.wardPrefix} ${orderDetail?.deliveryAddress.wardName}, ${orderDetail?.deliveryAddress.districtPrefix} ${orderDetail?.deliveryAddress.districtName}, ${orderDetail?.deliveryAddress.cityPrefix} ${orderDetail?.deliveryAddress.cityName} `}
            </Typography>
            <Typography variant="body2">
              Số điện thoại: {orderDetail?.phoneNumber}
            </Typography>
          </Grid>
        </Grid>

        <Scrollbars>
          <TableContainer component={Box} sx={{ minWidth: 960 }}>
            <Table className={classes.table}>
              <TableHead className={classes.tableHead}>
                <TableRow>
                  <TableCell width={40}>#</TableCell>
                  <TableCell align="left">Sản phẩm thuê</TableCell>
                  <TableCell align="center">Bắt đầu</TableCell>
                  <TableCell align="center">Kết thúc</TableCell>
                  <TableCell align="center">Mã sản phẩm</TableCell>
                  <TableCell align="center">Giá thuê (đ/ngày)</TableCell>
                  <TableCell align="center">Giá gốc</TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                <TableRow
                  key={orderDetail?.product?._id}
                  className={classes.row}
                >
                  <TableCell>{1}</TableCell>
                  <TableCell component="th" scope="row" padding="none">
                    <Box
                      sx={{
                        py: 2,
                        display: 'flex',
                        alignItems: 'center'
                      }}
                    >
                      <Box
                        component="img"
                        alt={orderDetail?.product?._id}
                        src="/static/images/placeholder.svg"
                        data-src={
                          orderDetail?.product?.thumbnail ||
                          'https://lh3.googleusercontent.com/proxy/lwawa6p0OG6aZRzjI-FA5UQlGHNXjJzJPDtnG617UinaQGMnSrZtMJen3n9GC7gkf0Z9ySt3m5uPnqezZnb46D65_ZBI0AjKXYBVBJ1h4RUeSvDHvScRrWsuq7ZTPXi8bi83'
                        }
                        className="lazyload blur-up"
                        sx={{
                          mx: 2,
                          width: 64,
                          height: 64,
                          borderRadius: 1.5
                        }}
                      />
                      <Typography variant="subtitle2" noWrap>
                        {orderDetail?.product?.name}
                        <Typography
                          variant="subtitle2"
                          className={classes.subTitle}
                        >
                          Tình trạng: {orderDetail?.product.appearance}
                        </Typography>
                      </Typography>
                    </Box>
                  </TableCell>
                  <TableCell align="center">
                    {moment(orderDetail?.product?.beginTime).format(
                      'DD/MM/YYYY'
                    )}
                  </TableCell>
                  <TableCell align="center">
                    {moment(orderDetail?.product?.endTime).format('DD/MM/YYYY')}
                  </TableCell>
                  <TableCell align="center">
                    {orderDetail?.product?.productCode}
                  </TableCell>
                  <TableCell align="center">
                    {fCurrency(orderDetail?.product?.rentPrice)}
                  </TableCell>
                  <TableCell align="center">
                    {fCurrency(orderDetail?.originalPrice)}
                  </TableCell>
                </TableRow>
                <TableRow className={classes.rowResult}>
                  <TableCell colSpan={5} />
                  <TableCell align="right">
                    <Box sx={{ mt: 2 }} />
                    <Typography variant="body1">Tạm tính</Typography>
                  </TableCell>
                  <TableCell align="right" width={120}>
                    <Box sx={{ mt: 2 }} />
                    <Typography variant="body1">
                      {fCurrency(orderDetail?.product?.rentPrice)}đ
                    </Typography>
                  </TableCell>
                </TableRow>
                <TableRow className={classes.rowResult}>
                  <TableCell colSpan={5} />
                  <TableCell align="right">
                    <Typography variant="body1">Số ngày thuê</Typography>
                  </TableCell>
                  <TableCell align="right" width={120}>
                    <Typography variant="body1">
                      {fCurrency(
                        Math.ceil(
                          distanceDate(
                            orderDetail?.product?.beginTime,
                            orderDetail?.product?.endTime
                          )
                        )
                      )}
                    </Typography>
                  </TableCell>
                </TableRow>
                <TableRow className={classes.rowResult}>
                  <TableCell colSpan={5} />
                  <TableCell align="right">
                    <Typography variant="body1">Giảm giá</Typography>
                  </TableCell>
                  <TableCell align="right" width={120}>
                    <Typography color="error">{fCurrency(0)}%</Typography>
                  </TableCell>
                </TableRow>

                <TableRow className={classes.rowResult}>
                  <TableCell colSpan={5} />
                  <TableCell align="right">
                    <Typography variant="h6">Tổng cộng</Typography>
                  </TableCell>
                  <TableCell align="right" width={140}>
                    <Typography variant="h6">
                      {fCurrency(
                        orderDetail?.product?.rentPrice *
                          Math.ceil(
                            distanceDate(
                              orderDetail?.product?.beginTime,
                              orderDetail?.product?.endTime
                            )
                          )
                      )}{' '}
                      đ
                    </Typography>
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
        </Scrollbars>

        <Box sx={{ mt: 5 }} />
        <Divider />

        <Timeline
          historyOrder={orderDetail?.history}
          onTime={distanceDate(
            orderDetail?.history?.returned,
            orderDetail?.product?.endTime
          )}
        />

        <Box sx={{ mt: 5 }} />
        <Divider />

        {roleType === 'agency' && (
          <>
            <Grid container>
              <Grid item xs={12} md={6}></Grid>
              <Grid item xs={12} md={3}>
                <Box sx={{ py: 4, textAlign: 'right' }}>
                  {btnCancel && (
                    <Button
                      variant="outlined"
                      color="primary"
                      style={{ width: '80%' }}
                      onClick={handleCancel}
                    >
                      {btnCancel}
                    </Button>
                  )}
                </Box>
              </Grid>
              <Grid item xs={12} md={3}>
                <Box sx={{ py: 4, textAlign: 'right' }}>
                  {btnAccept && (
                    <Button
                      variant="contained"
                      color="primary"
                      style={{ width: '80%' }}
                      onClick={handleAccept}
                    >
                      {btnAccept}
                    </Button>
                  )}
                </Box>
              </Grid>
            </Grid>

            <AboutTime
              endTime={orderDetail?.product.endTime}
              status={orderDetail?.status}
              returned={orderDetail?.history.returned}
            />

            {orderDetail?.history.returned &&
              (orderDetail?.status === 'returned' ||
                orderDetail?.status === 'late') &&
              !orderDetail?.reviewCustomer && (
                <>
                  <Rating
                    orderId={orderDetail?._id}
                    customer={orderDetail?.fullName}
                    accountId={orderDetail?.accountId}
                    product={orderDetail?.product}
                  />

                  <Box sx={{ mt: 5 }} />
                  <Divider />
                </>
              )}
          </>
        )}
        <Grid container>
          <Grid item xs={12} md={9}>
            <Box sx={{ py: 3 }}>
              <Typography variant="subtitle2">Lưu ý</Typography>
              <Typography variant="body2">
                Mọi thông tin hóa đơn đã bao gồm thuế và phí vận chuyển! Cảm ơn!
              </Typography>
            </Box>
          </Grid>
          <Grid item xs={12} md={3}>
            <Box sx={{ py: 3, textAlign: 'right' }}>
              <Typography variant="subtitle2">Thắc mắc liên hệ?</Typography>
              <Typography variant="body2">rentmart@support.com</Typography>
            </Box>
          </Grid>
        </Grid>
      </Card>
    </>
  );
}
