import React from 'react';
import Rating from '@material-ui/lab/Rating';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import {
  Grid,
  TextField,
  Button,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Checkbox
} from '@material-ui/core';
import authService from '../../../apis/auth.api';
import { UploadMultiFile } from '../../../components/Upload';
import { useSnackbar } from 'notistack';
import { MIconButton } from '../../../@material-extend';
import { Icon } from '@iconify/react';
import closeFill from '@iconify-icons/eva/close-fill';
import productService from '../../../apis/product.api';
import imageService from '../../../apis/images.api';

export default function SimpleRating({
  customer,
  accountId,
  orderId,
  product
}) {
  const [value, setValue] = React.useState(0);
  const [review, setReview] = React.useState();
  const [appearance, setAppearance] = React.useState();
  const [error1, setError1] = React.useState('');
  const [expanded, setExpanded] = React.useState('');
  const [error2, setError2] = React.useState('');
  const [images, setImages] = React.useState([]);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const handleClick = async () => {
    if (!value) {
      setError1('* Vui lòng đánh giá sao');
      return;
    }
    if (!review) {
      setError1('* Vui lòng nhập đánh giá');
      return;
    }
    if (expanded === 'panel1' && !appearance && appearance === '') {
      setError2('* Vui lòng nhập tình trạng sản phẩm mới nhất');
      return;
    }
    if (expanded === 'panel1' && images.length === 0) {
      setError2('* Vui lòng chọn ảnh sản phẩm mới nhất');
      return;
    }
    await authService
      .reviewOrder({
        content: review,
        rating: value,
        orderId,
        accountId
      })
      .then(async () => {
        let data = {
          hasChangeAppearance: false,
          product: {},
          orderId
        };
        if (expanded === 'panel1') {
          const files = await imageService.upload(images, 6);
          data.product.appearance = appearance;
          data.product.images = files;
          data.product.note = 'Cập nhật tình trạng sản phẩm';
          data.hasChangeAppearance = true;
        }
        await productService.updateHistoryProduct(
          data,
          product.templateId,
          product.productId
        );
        enqueueSnackbar('Cập nhật thành công', {
          variant: 'success',
          action: key => (
            <MIconButton size="small" onClick={() => closeSnackbar(key)}>
              <Icon icon={closeFill} />
            </MIconButton>
          )
        });
        window.location.reload();
      })
      .catch(error => {
        enqueueSnackbar('Không thể cập nhật', {
          variant: 'error',
          action: key => (
            <MIconButton size="small" onClick={() => closeSnackbar(key)}>
              <Icon icon={closeFill} />
            </MIconButton>
          )
        });
        setError1('');
        setError2('');
      });
  };

  return (
    <>
      <Grid container>
        <Grid item xs={12} md={1}></Grid>
        <Grid item xs={12} md={2}>
          <Box sx={{ py: 3, textAlign: 'left' }}>
            <Typography variant="subtitle2">Nhận xét khách hàng:</Typography>
            <Typography variant="body2">{customer}</Typography>
          </Box>
        </Grid>
        <Grid item xs={12} md={2}>
          <Box sx={{ py: 3 }}>
            <div>
              <Box component="fieldset" mb={3} borderColor="transparent">
                <Typography component="legend">Đánh giá</Typography>
                <Rating
                  name="simple-controlled"
                  value={value}
                  onChange={(event, newValue) => {
                    setValue(newValue);
                  }}
                />
              </Box>
            </div>
          </Box>
        </Grid>
        <Grid item xs={12} md={4} style={{ alignItems: 'center' }}>
          <Box sx={{ py: 3 }}>
            <Box>
              <TextField
                id="filled-basic"
                label="Viết đánh giá khách hàng"
                value={review}
                size="small"
                onChange={e => setReview(e.target.value)}
                style={{ width: '90%' }}
              />
              <p style={{ color: 'red', fontSize: '1rem' }}>{error1}</p>
            </Box>
          </Box>
        </Grid>
        <Grid item xs={12} md={3}>
          <Box sx={{ py: 4, textAlign: 'right' }}>
            <Button
              variant="contained"
              color="primary"
              style={{ width: '80%' }}
              onClick={() => {
                handleClick();
              }}
            >
              Đánh giá
            </Button>
          </Box>
        </Grid>
      </Grid>
      <Grid container>
        <Grid xs={12} md={1}></Grid>
        <Grid xs={12} md={5}>
          <Accordion square expanded={expanded === 'panel1'}>
            <AccordionSummary
              aria-controls="panel1d-content"
              id="panel1d-header"
            >
              {/* <FormControlLabel
                aria-label="Acknowledge"
                onClick={event => {
                  if (expanded === 'panel1') {
                    setExpanded('');
                  } else {
                    setExpanded('panel1');
                  }
                }}
                control={<Checkbox />}
              /> */}
              <Checkbox
                color="primary"
                onClick={event => {
                  if (expanded === 'panel1') {
                    setExpanded('');
                  } else {
                    setExpanded('panel1');
                  }
                }}
                inputProps={{ 'aria-label': 'secondary checkbox' }}
              />
              <Typography variant="subtitle2">
                Tình trạng sản phẩm đã bị thay đổi sau khi thuê
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <TextField
                id="filled-basic"
                label="Nhập tình trạng sản phẩm mới nhất"
                value={appearance}
                size="small"
                onChange={e => setAppearance(e.target.value)}
                style={{ width: '100%' }}
              />
              <Box>
                <Typography
                  variant="subtitle2"
                  color="textSecondary"
                  gutterBottom
                >
                  Ảnh sản phẩm sau khi thuê
                </Typography>
                {/* <UploadSingleFile
            value={values.cover}
            onChange={val => setFieldValue('cover', val)}
            caption="(Only *.jpeg and *.png images will be accepted)"
            error={Boolean(touched.cover && errors.cover)}
          /> */}
                <UploadMultiFile
                  value={images}
                  onChange={setImages}
                  caption="Tải ảnh lên"
                />
                <p style={{ color: 'red', fontSize: '1rem' }}>{error2}</p>
              </Box>
            </AccordionDetails>
          </Accordion>
        </Grid>
      </Grid>
    </>
  );
}
