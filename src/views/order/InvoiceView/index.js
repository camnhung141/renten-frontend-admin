import React, { useEffect } from 'react';
import { PATH_APP } from '../../../routes/paths';
import Toolbar from './Toolbar';
import Page from '../../../components/Page';
import { useParams } from 'react-router-dom';
import CardInvoice from './CardInvoice';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import HeaderDashboard from '../../../components/HeaderDashboard';
import { Container } from '@material-ui/core';
import { getDetailOrder } from '../../../redux/slices/order';
import LoadingScreen from '../../../components/LoadingScreen';

// ----------------------------------------------------------------------

const useStyles = makeStyles(theme => ({
  root: {},
  card: {
    padding: theme.spacing(5, 5, 0)
  },
  gridItem: {
    marginBottom: theme.spacing(5)
  },
  tableHead: {
    borderBottom: `solid 1px ${theme.palette.divider}`,
    '& th': {
      backgroundColor: 'transparent'
    }
  },
  row: {
    borderBottom: `solid 1px ${theme.palette.divider}`
  },
  rowResult: {
    '& td': {
      paddingTop: theme.spacing(1),
      paddingBottom: theme.spacing(1)
    }
  }
}));

// ----------------------------------------------------------------------

function InvoiceView() {
  const classes = useStyles();
  const { id } = useParams();
  const dispatch = useDispatch();
  const orderDetail = useSelector(state => state.order.orderDetail);
  useEffect(() => {
    dispatch(getDetailOrder(id));
  }, [dispatch, id]);

  if (!orderDetail) {
    return <LoadingScreen />;
  }
  return (
    <Page title="Management | Invoice Details" className={classes.root}>
      <Container>
        <HeaderDashboard
          heading="Danh sách đơn hàng"
          links={[
            { name: 'Bảng điều khiển', href: PATH_APP.root },
            { name: 'Quản lý', href: PATH_APP.management.root },
            { name: 'Đơn hàng', href: PATH_APP.management.order.root },
            {
              name: 'Danh sách đơn hàng',
              href: PATH_APP.management.order.list
            },
            { name: 'Chi tiết' }
          ]}
        />
        <Toolbar orderDetail={orderDetail} />
        <CardInvoice orderDetail={orderDetail} />
      </Container>
    </Page>
  );
}

export default InvoiceView;
