import React from 'react';
import { Typography } from '@material-ui/core';
export default function AboutTime({ endTime, status, returned }) {
  const distanceDate = (a, b) => {
    let date1;
    if (returned) {
      date1 = new Date(returned);
    } else {
      date1 = new Date();
    }
    const ib = new Date(endTime);
    const date2 = new Date(
      `${ib.getMonth() + 1}/${ib.getDate() + 1}/${ib.getFullYear()}`
    );

    const diffTime = date2 - date1;
    return diffTime / (1000 * 60 * 60 * 24);
  };

  return (
    <>
      {distanceDate() > 0 &&
        status !== 'returned' &&
        status !== 'cancel' &&
        status !== 'late' &&
        status !== 'notReturned' && (
          <>
            <Typography
              variant="body2"
              style={{ float: 'right', color: 'red' }}
            >
              {' '}
              Còn{' '}
              {distanceDate() > 1
                ? Math.ceil(distanceDate()) + ' Ngày'
                : Math.ceil(distanceDate() * 24) + ' Giờ'}
            </Typography>
          </>
        )}

      {distanceDate() < 0 &&
        status !== 'returned' &&
        status !== 'cancel' &&
        status !== 'lated' &&
        status !== 'notReturned' && (
          <>
            <Typography
              variant="body2"
              style={{ float: 'right', color: 'red' }}
            >
              {' '}
              Trễ quá{' '}
              {distanceDate() < -1
                ? Math.ceil(Math.abs(distanceDate())) + ' Ngày'
                : Math.ceil(Math.abs(distanceDate()) * 24) + ' Giờ'}
            </Typography>
          </>
        )}
    </>
  );
}
